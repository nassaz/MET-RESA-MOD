<?php

/**
 * @file
 * Builds placeholder replacement tokens for node-related data.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\user\Entity\User;

/**
 * Implements hook_token_info().
 */
function reservation_token_info() {
  $type = [
    'name' => t('Nodes'),
    'description' => t('Tokens related to individual content items, or "nodes".'),
    'needs-data' => 'node',
  ];

  // Core tokens for nodes.
  $node['nid'] = [
    'name' => t("Content ID"),
    'description' => t('The unique ID of the content item, or "node".'),
  ];
  $node['vid'] = [
    'name' => t("Revision ID"),
    'description' => t("The unique ID of the node's latest revision."),
  ];
  $node['type'] = [
    'name' => t("Content type"),
  ];
  $node['type-name'] = [
    'name' => t("Content type name"),
    'description' => t("The human-readable name of the node type."),
  ];
  $node['title'] = [
    'name' => t("Title"),
  ];
  $node['body'] = [
    'name' => t("Body"),
    'description' => t("The main body text of the node."),
  ];
  $node['summary'] = [
    'name' => t("Summary"),
    'description' => t("The summary of the node's main body text."),
  ];
  $node['langcode'] = [
    'name' => t('Language code'),
    'description' => t('The language code of the language the node is written in.'),
  ];
  $node['url'] = [
    'name' => t("URL"),
    'description' => t("The URL of the node."),
  ];
  $node['edit-url'] = [
    'name' => t("Edit URL"),
    'description' => t("The URL of the node's edit page."),
  ];

  // Chained tokens for nodes.
  $node['created'] = [
    'name' => t("Date created"),
    'type' => 'date',
  ];
  $node['changed'] = [
    'name' => t("Date changed"),
    'description' => t("The date the node was most recently updated."),
    'type' => 'date',
  ];
  $node['author'] = [
    'name' => t("Author"),
    'type' => 'user',
  ];

  return [
    'types' => ['node' => $type],
    'tokens' => ['node' => $node],
  ];
}

/**
 * Implements hook_tokens().
 */
function reservation_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $token_service = \Drupal::token();

  $replacements = [];
  if ($type == 'reservation_demande' && !empty($data['reservation_demande'])) {
    /** @var \Drupal\node\NodeInterface $node */
    $reservationDemande = $data['reservation_demande'];
    
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // Simple key values on the node.
        case 'datecreneau':
          $replacements[$original] = $reservationDemande->getDateCreneau();
          break;
        case 'datedemande':
          $replacements[$original] = $reservationDemande->getCreatedFormat();
          break;
        case 'ressource':
          $replacements[$original] = $reservationDemande->getDate()->getReservationRessourceNode()->getNode()->getTitle();
          break;
      }
    }
  }
   
  return $replacements;
}
