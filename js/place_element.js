(function ($, Drupal) {
    
    Drupal.behaviors.reservation = {

        attach: function(context) {
            var SelectHoraireId = '#reservation-accompagnateur-select';
            
            $.ajax({
                url: '/reservation/place',
                dataType: "json",
                async: false,
                success: function(data) { 
                                        
                    $('#reservation-accompagnateur-select > option').each(function() {
                        if($(this).text() > data)
                        {
                            $("#reservation-accompagnateur-select option[value='" + $(this).val() + "']").remove();
                        }
                    });
                }
            });  

        }
    }
})(jQuery, Drupal);



