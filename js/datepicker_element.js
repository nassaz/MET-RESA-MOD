(function ($, Drupal) {
    
    Drupal.behaviors.reservation = {

        attach: function(context) {
          
            var statutDates = {},
                rdidDates = {},
                horaireDates = {},
                placeDates = {},
                jaugeDates = {},
                dates = new Date(),
                currentYear = dates.getFullYear(),
                currentMonth = dates.getMonth(),
                formId =  $('#webform-reservation-ressource-id').val(),
                CalendarId = '#reservation-calendar',
                CalendarDatepickerId = '#reservation-calendar-datepicker',
                horaireId = '#reservation-horaire-wrapper',
                horaireTitleId = '#span-reservation-horaire-title',
                horaireSelectId = '#reservation-horaire-select',
                moisEntier = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                moisCourt = ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"];
                
            $(horaireId).hide();
            
            if($(CalendarId).hasClass('showElements')){
                $('#block-reservation .form-item').css('display', 'block');
                $('.region-sidebar-right .field--type-text-with-summary').css('display', 'block');
                $('#block-reservation .webform-button--submit').css('display', 'block');
            };
                
            if(formId)
            {
                $.ajax({
                    url: '/reservation/calendar/'+formId,
                    dataType: "json",
                    async: false,
                    success: function(data) { 
                        $.each(data, function (key, val) {
                            dateData = new Date(val.date);
                            statutDates[dateData] = val.statut;
                            rdidDates[getDateFormat(dateData)] = val.rdid;
                            jaugeDates[getDateFormat(dateData)] = val.jauge;
                            placeDates[getDateFormat(dateData)] = val.place;
                            if(val.horaire)
                            {
                               horaireDates[getDateFormat(dateData)] = val.horaire;                            
                            }
                        });
                    }
                });  
            }

            $(CalendarDatepickerId).datepicker({
                dateFormat: "yy-mm-dd",
                firstDay: 1,
                minDate: new Date(currentYear, currentMonth, '01'),
                maxDate: new Date(currentYear + 1, currentMonth, ''),
                monthNames: moisEntier,
                dayNamesMin: moisCourt,
                onSelect: function (date) {
                    $("#nombreplacedate").remove();
                    $(CalendarId).val(rdidDates[date]);
                    $('#block-reservation .form-item').css('display', 'block');
                    $('.region-sidebar-right .field--type-text-with-summary').css('display', 'block');
                    $('#block-reservation .webform-button--submit').css('display', 'block');
                               
                    $('#id-nombre-place').find('option').remove().end();
                    $(horaireSelectId).find('option').remove().end();
                    if(horaireDates[date])
                    {
                        $(horaireId).show();
                        $(horaireTitleId).html('<i style="color:orange" >' + getDateDMY(date) + '</i>');
                        $.each(horaireDates[date], function (key, val) {
                            var placeHoraire = '';
                            if($(CalendarId).hasClass('showplacehoraire') && val.jauge === "1")
                            {
                                placeHoraire = ' : ' + val.place + ' place(s) restante(s)';
                            };
                            
                            $(horaireSelectId).append(
                                $('<option>', {
                                    value: val.rhid,
                                    text: val.heure + placeHoraire
                                })
                            );
                         });
                    }
                    else
                    {     
                        $(horaireId).hide();
                        if($(CalendarId).hasClass('showplacedate') && jaugeDates[date] === "1")
                        {
                            $(CalendarDatepickerId).append(
                                '<div id="nombreplacedate"><p><b>Nombre de place(s) restante(s) : </b>' + placeDates[date] + 
                                '</p></div>' 
                            );                     
                        };
                    }
                },

                beforeShowDay: function (dates) {
                    
                    if (statutDates[dates] == 'waitDates') {
                        return [false, 'waiting', 'waitDate'];
                    } else if (statutDates[dates] == 'reserveDates') {
                        return [false, 'reserved', 'reserveDate'];
                    } else if (statutDates[dates]  == 'openDates') {
                        return [true, 'opened', 'openDate'];
                    } else {
                        return [false, 'closed', ''];
                    }
                }, 
            }).find('.ui-datepicker-current-day')
                .removeClass('ui-datepicker-current-day');

         
            $('#Date').val('');

            $(CalendarDatepickerId).once().append(
                '<div id="legendBloc">' +
                '<div class="legendDiv"><svg width="30" height="30"><rect width="30" height="30" style="fill:#a4c414;stroke-width:0;" /></svg><p>Disponible</p></div>' +
                '<div class="legendDiv"><svg width="30" height="30"><rect width="30" height="30" style="fill:#c60b0b;stroke-width:0;" /></svg><p>Réservé</p></div>' +
                '<div class="legendDiv"><svg width="30" height="30"><rect width="30" height="30" style="fill:#da9800;stroke-width:0;" /></svg><p>En attente</p></div>' +
                '<div class="legendDiv"><svg width="30" height="30"><rect width="30" height="30" style="fill:#cacaca;stroke-width:0;" /></svg><p>Fermé</p></div>' +
                '</div>' 
            );   


            function getDateFormat(date) {

                day = date.getDate();
                month = date.getMonth();
                year = date.getFullYear();
                month = month + 1;

                if(month < 10)
                {
                    month = '0' + month;
                }

                if(day < 10)
                {
                    day = '0' + day;
                }

                dateFormat = year + '-' + month + '-' + day;

                return dateFormat;
            }
            
            function getDateDMY(date) {
                var d = new Date(date);

                day = d.getDate();
                month = d.getMonth();
                year = d.getFullYear();
                
                dateFormat = day + ' ' + moisEntier[month] + ' ' + year;

                return dateFormat;
            }
        }
    }
})(jQuery, Drupal);


