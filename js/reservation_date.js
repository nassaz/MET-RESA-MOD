(function ($) {
  Drupal.behaviors.reservation = {
    attach: function(context) {
  
    // Verification du changement du bouton "ALL" pour chaque mois
    $('[id$="-all"]').change(function() {

        var myRegEx = new RegExp('edit-table-date-(.*)-all'); 
        var month = myRegEx.exec($(this).attr('id'))[1];

        if( $(this).is(':checked') ){
            // Si la checbkbox ALL est activée, tous les jours de son mois sont activés
            $("[id^='edit-table-date-" + month + "'][id$='-statut']").prop("checked", true);
        } else {
            // Si la checbkbox ALL est désactivée, tous les jours de son mois sont désactivés
            $("[id^='edit-table-date-" + month + "'][id$='-statut']").prop("checked", false);
        }
    });
     

     
    }
  };
})(jQuery);