<?php

namespace Drupal\reservation\Service;

class ReservationHoraireServices
{
    public function __construct()
    {

    }
    public static function getAll($ids = null)
    {
        return \Drupal::entityTypeManager()
                ->getStorage("reservation_horaire")
                ->loadMultiple($ids);
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getByDate($rdid) {

        $query = \Drupal::entityQuery('reservation_horaire');
        $query->condition('rdid', $rdid);
        $query->sort('heure_debut', 'ASC');  

        return self::getAll($query->execute());
    }
    
    public function notExisteHoraire($rdid, $heure_debut, $heure_fin)
    {
        $query = \Drupal::entityQuery('reservation_horaire');        
        $query->condition('heure_debut', $heure_debut);   
        $query->condition('heure_fin', $heure_fin);   
        $query->condition('rdid', $rdid);   
        $query->range(0,1);
        
        return $query->count()->execute() ? False : True;
    }

    public static function getByRdid($rdid, $statut = True) {

        $query = \Drupal::entityQuery('reservation_horaire');
        $query->condition('rdid', $rdid);
        $query->condition('statut', $statut);
        $query->sort('heure_debut', 'ASC');
        
        return self::getAll($query->execute());
    }

}
