<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationDate;
use Drupal\Core\Url;


class ReservationDateServices 
{    
    const ID_RESERVATION_DATE = 'reservation_date';
    
    public function __construct()
    {

    }
        
    /**
     * blabla
     * 
     * @return date Champ Date 
     */
    public function loadDemande($rdid) 
    {        
        $reservationdate = ReservationDate::load(current($rdid));
        if($reservationdate)
        {
            return $reservationdate->getDate();
        }
    }
    
    /**
     * 
     * @return date Champ Date 
     */
    public function getDateMax() {        
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DATE);
        $query->range(0,1);
        $query->sort('date', 'DESC');
        
        return $this->loadDemande($query->execute());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getYearMax() {          
        $date = new \DateTime($this->getDateMax());
        return $date->format('Y');
    }

    /**
     * {@inheritdoc}
     */
    public function getDateMin() {        
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DATE);
        $query->range(0,1);
        $query->sort('date', 'ASC');
        
        return $this->loadDemande($query->execute());
    }
    
    /**
     * {@inheritdoc}
     */
    public function getYearMin() {        
        $date = new \DateTime($this->getDateMin());
        return $date->format('Y');
    }
      
    public function getAll($ids = null)
    {
        return \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_DATE)
                ->loadMultiple($ids);
    }
    
    public function getRdidByNid($nid)
    {        
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DATE);   
        $query->condition('nid', $nid, 'IN');        
        return $query->execute();       
    }
        
    public function getFormDatesExist($nid, $year) {

        $dates = [];
        $reservationDates = $this->getFormDates($nid,$year, '1');
        foreach($reservationDates as $reservationDate)
        {
          $row = [];
          $date = new \DateTime($reservationDate->get('date')->value);
          
          $row['rdid'] = $reservationDate->get('rdid')->value;
          $row['statut'] = $reservationDate->get('statut')->value;
          $dates[$date->format('m')][$date->format('d')] = $row;
        }
      
        return $dates;
    }
    
    public function queryDate($nid, $date, $statut = null, $publie = null, $postDate = False) 
    {
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DATE);
        if($statut)
        {
            $query->condition('statut', $statut);            
        }
        if($publie)
        {
            $query->condition('publie', $statut);            
        }
        if($postDate)
        {
            $date_now = new \DateTime();
            $query->condition('date', $date_now->format('Y-m-d 00:00:00'), '>=');            
        }
        $query->condition('date', $date.'%', 'LIKE');   
        $query->condition('nid', $nid);
        $query->sort('date', 'ASC');
        
        return $query;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPublieMonth($year, $month, $nid, $publie = True) 
    {
        $query = $this->queryDate($nid, $year . '-' . $month, True, $publie); 
        $query->range(0,1);        
        return $query->count()->execute() ? True : False;      
    }
    
    /**
     * 
     * @param type $nid
     * @param type $date
     * @param type $publie
     * @param type $statut
     * @return type
     */    
    public function getDate($nid, $date, $publie = False, $statut = False, $postDate = False) 
    {
        $query = $this->queryDate($nid, $date, $statut, $publie, $postDate);
        return $this->getAll($query->execute());
    }
        
    /**
     * 
     * @param type $nid
     * @param type $date
     * @param type $publie
     * @param type $statut
     * @return type
     */    
    public function getDatePublie($nid, $date, $publie = True, $statut = True) 
    {
        $query = $this->queryDate($nid, $date, $statut, $publie, True);
        return $this->getAll($query->execute());
    }
    
    public function getDateOpen($nid, $date, $statut) 
    {
        $query = $this->queryDate($nid, $date, $statut, null, True);   
        return $this->getAll($query->execute());
    }
    
    public function getFirstDateById($nid, $date)
    {      
        $reservationDate = $this->getDateOpen($nid, $date, True);
        return array_shift($reservationDate);
    }
        
    public function getFormDates($nid, $date, $statut) 
    {        
        $query = $this->queryDate($nid, $date, $statut);        
        return $this->getAll($query->execute());
    }

    public function getOpenMonth($nid, $month, $year, $statut) 
    {
        $dates = [];
        $rows = $this->getDateOpen($nid, $year, $statut);
        foreach ($rows as $row) {            
          $date = new \DateTime($row->get('date')->value);
          $key = $date->format('m');
          $dates[$key]['key'] = $key;
          $dates[$key]['title'] = $date->format('M');
          $dates[$key]['link'] = Url::fromRoute('reservation.disponibilite.horaire', ['nid' => $nid, 'month' => $key, 'year' => $year], ['absolute' => TRUE]);         
          if($key == $month)
          {
              $dates[$key]['active'] = True;
          }
        }
        return $dates;
    }

    public function getDateById($nid) 
    {
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DATE);
        $query->condition('nid', $nid);
        $query->sort('rdid', 'ASC');
        
        return $this->getAll($query->execute());
    }    
    
    public function setCancelDate($nid){
         
        $rows = $this->getDateOpen($nid, '', null);
        foreach ($rows as $node) {
            $node->statut->value = False;
            $node->save();
        }
    }
    
    public function countDateOpen($nid, $year) 
    {        
        $query = $this->queryDate($nid, $year, True, null, True);  
        return $query->count()->execute();
    }
    
    public function countDatePublie($nid, $year) 
    {        
        $query = $this->queryDate($nid, $year, True, True, True);  
        return $query->count()->execute();
    }
}
