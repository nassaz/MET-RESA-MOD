<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationDate;
use Drupal\reservation\Entity\ReservationHoraire;

class ReservationCalendarServices {

    const STATUT_RESERVE = 'reserveDates';
    const STATUT_WAIT = 'waitDates';
    const STATUT_OPEN = 'openDates';
    const ID_RESERVATION_DEMANDE = 'reservation_demande';

    /**
     *
     * @var demandeServices 
     */
    protected $demandeServices;

    /**
     *
     * @var horaireServices 
     */
    protected $horaireServices;

    /**
     * 
     * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
     */
    public function __construct(ReservationDemandeServices $demandeServices, ReservationHoraireServices $horaireServices) {
        $this->demandeServices = $demandeServices;
        $this->horaireServices = $horaireServices;
    }

    /**
     * 
     * @param type $rdid
     * @param type $rhid
     * @param type $destroy
     * @return type
     */
    public function verificationDisponibilite($rdid = null, $rhid = null) {
        $statut = False;
        $reservationDate = ReservationDate::load($rdid);
        if ($reservationDate->getHoraire()) {
            $reservationHoraire = ReservationHoraire::load($rhid);
            $count = $this->getDemandeHoraireDetail($reservationHoraire);
            $dates = $this->getDemandeLabelStatut(1, $count['jauge'], $count['reserve'], $count['wait']);
            if ($count['horaire']['place'] > 0) {
                $statut = True;
            }
        } else {
            $dates = $this->getDemandeDate($reservationDate);
            $statut = ($dates['statut'] === self::STATUT_OPEN) ? True : False;
        }

        return $statut;
    }

    public function getDemandeDate($reservationDate) {
        $reservationDemandes = $this->demandeServices->getDemandeByRdid($reservationDate->Id());
        $count = $this->getDemandeCountStatut($reservationDemandes);
        $statut = $this->getDemandeLabelStatut($reservationDate->getJaugeStatut(), $reservationDate->getJaugeNombre(), $count['reserve'], $count['wait']);
        $place = $reservationDate->getJaugeNombre() - $count['reserve'] - $count['wait'];
        return $this->getFormatDate($reservationDate, $statut, False, $place);
    }

    public function getDemandeHoraire($reservationDate) {
        $count_reserve = 0;
        $count_wait = 0;
        $count_jauge = 0;
        $horaires = [];

        $horaireDisponibles = $this->horaireServices->getByRdid($reservationDate->Id());
        foreach ($horaireDisponibles as $horaireDisponible) {
            $count = $this->getDemandeHoraireDetail($horaireDisponible);

            $count_wait += $count['wait'];
            $count_reserve += $count['reserve'];
            $count_jauge += $count['jauge'];

            if (isset($count['horaire']) && $count['horaire']['place'] > 0) {
                $horaires[] = $count['horaire'];
            }
        }

        $statut = $this->getDemandeLabelStatut(1, $count_jauge, $count_reserve, $count_wait);

        return $this->getFormatDate($reservationDate, $statut, $horaires);
    }

    public function getDemandeHoraireDetail($reservationHoraire) {
        $reservationDemandes = $this->demandeServices->getDemandeByRhid($reservationHoraire->Id());
        $count = $this->getDemandeCountStatut($reservationDemandes);
        $count['jauge'] = $reservationHoraire->getJaugeNombre();

        $place = $count['jauge'] - $count['reserve'] - $count['wait'];
        if ($place > 0) {
            $count['horaire'] = [
                'rhid' => $reservationHoraire->Id(),
                'heure' => $reservationHoraire->getHeureDebutFormat('H:i'),
                'place' => $count['jauge'] - $count['reserve'] - $count['wait'],
                'jauge' => $reservationHoraire->getJaugeStatut(),
            ];
        }

        return $count;
    }

    public function getFormatDate($reservationDate, $statut, $horaire = False, $place = 0) {
        return [
            'rdid' => $reservationDate->Id(),
            'date' => $reservationDate->getDateFormat('m/d/Y'),
            'statut' => $statut,
            'place' => $place,
            'jauge' => $reservationDate->getJaugeStatut(),
            'horaire' => $horaire,
        ];
    }

    public function getDemandeCountStatut($reservationDemandes) {
        $statut = null;
        $count_wait = 0;
        $count_reserve = 0;

        foreach ($reservationDemandes as $reservationDemande) {
            $statut = $reservationDemande->getStatut();
            if ($statut == 'caution' || $statut == 'formulaire' || $statut == 'attente') {
                $count_wait += $reservationDemande->getJauge();
            } elseif ($statut == 'confirme' || $statut == 'archive') {
                $count_reserve += $reservationDemande->getJauge();
            }
        }

        return ['wait' => $count_wait, 'reserve' => $count_reserve];
    }

    /**
     * 
     * @param type $jaugeStatut
     * @param type $jaugeNombre
     * @param type $count_reserve
     * @param type $count_wait
     * @return type
     */
    public function getDemandeLabelStatut($jaugeStatut = 1, $jaugeNombre = 0, $count_reserve = 0, $count_wait = 0) {
        $statut = null;

        if ($jaugeStatut && ($jaugeNombre <= $count_reserve)) {
            $statut = self::STATUT_RESERVE;
        } elseif ($jaugeStatut && ($jaugeNombre <= $count_reserve + $count_wait)) {
            $statut = self::STATUT_WAIT;
        } else {
            $statut = self::STATUT_OPEN;
        }

        return $statut;
    }

}
