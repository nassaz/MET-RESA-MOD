<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationDemande;


class ReservationDemandeServices 
{
    const STATUT_RESERVE = 'reserveDates';
    
    const STATUT_WAIT = 'waitDates';
    
    const STATUT_OPEN = 'openDates';
        
    const ID_RESERVATION_DEMANDE = 'reservation_demande';
    
    public function __construct()
    {
        
    }
   
    /**
     * 
     * @param type $id
     * @return type
     */
    public function load($id = null)
    {
        return \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_DEMANDE)
                ->load($id);
    }
        
    /**
     * 
     * @param type $ids
     * @return type
     */
    public function getAll($ids = null)
    {
        return \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_DEMANDE)
                ->loadMultiple($ids);
    }
    
    /**
     * 
     * @param string $sid
     * @return type
     */
    public function getByWebform(string $sid = null)
    {
        $query = \Drupal::entityQuery('reservation_demande');
	$query->condition('sid', $sid, '=');
        return current($query->execute());
    }
    
    /**
     * 
     * @return type
     */
    public function getDateMax() {        
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DEMANDE);
        $query->range(0,1);
        $query->sort('created', 'DESC');
        $retour = $query->execute();
        $reservationdemande = ReservationDemande::load(current($retour));
        return $reservationdemande->getCreatedTime();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getYearMax() {          
        $date = new \DateTime();
        $date->setTimestamp($this->getDateMax());
        return $date->format('Y');
    }

    /**
     * {@inheritdoc}
     */
    public function getDateMin() {        
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DEMANDE);
        $query->range(0,1);
        $query->sort('created', 'ASC');
        $retour = $query->execute();
        $reservationdemande = ReservationDemande::load(current($retour));
        return $reservationdemande->getCreatedTime();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getYearMin() {        
        $date = new \DateTime();
        $date->setTimestamp($this->getDateMin());
        return $date->format('Y');
    }
        
    public function getDemandeByYear($rdid, $years = 1) {

        $date_old = new \DateTime();
        $date_old->sub(new \DateInterval('P'.$years.'Y'));
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DEMANDE);
        $query->condition('rdid', $rdid, 'IN');
        $query->condition('created', $date_old->getTimestamp(), '<'); 
        
        return $this->getAll($query->execute());
    }
    
    public function getDemandeByRdid($rdid) {

        $query = \Drupal::entityQuery(self::ID_RESERVATION_DEMANDE);
        $query->condition('rdid', $rdid);
        $condition_or = $query->orConditionGroup();
        $condition_or->condition('rhid', '0');
        $condition_or->notExists('rhid');
        $query->condition($condition_or);

        return $this->getAll($query->execute());
    }
    
    public function getDemandeByRhid($rhid) {

        $query = \Drupal::entityQuery(self::ID_RESERVATION_DEMANDE);
        $query->condition('rhid', $rhid);
        
        return $this->getAll($query->execute());
    }
    
    public function getDdemandeEmail(string $statut = null) 
    {
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DEMANDE);
        
        if($statut)
        {
            $query->condition('statut', $statut, '=');                       
        }
        
        return $this->getAll($query->execute());
    }    
    
    public function getDemandeByFilter($header = null, $pager = 15, $nid = null, $statut = null, $choix_date_demande = True,
            $date_demande_debut = null, $date_demande_fin = null, $choix_date_creneau  = True, $date_creneau_debut = null, $date_creneau_fin  = null) 
    {
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DEMANDE);
        
        if($nid)
        {
            $query->condition('rdid.entity.nid', $nid, 'IN');                       
        }
        
        if($statut)
        {
            $query->condition('statut', $statut, 'IN');
        }   
        
        if($choix_date_demande && $date_demande_debut)
        {
            $date = new \DateTime($date_demande_debut);
            $query->condition('created', $date->getTimestamp(), '>');
        }   
        
        if($choix_date_demande && $date_demande_fin)
        {
            $date = new \DateTime($date_demande_fin);
            $query->condition('created', $date->getTimestamp(), '<');
        }   
        
        if($choix_date_creneau && $date_creneau_debut)
        {
            $date = new \DateTime($date_creneau_debut);
            $query->condition('rdid.entity.date', $date->format('Y-m-d\T00:00:00'), '>');
        }   
        
        if($choix_date_creneau && $date_creneau_fin)
        {
            $date = new \DateTime($date_creneau_fin);
            $query->condition('rdid.entity.date', $date->format('Y-m-d\T00:00:00'), '<');
        }   
        
        if($header)
        {
            $query->tableSort($header);
        }        
        
        if($pager)
        {
            $query->pager($pager);           
        }
        
        return $this->getAll($query->execute());
    }
    
    public function getDemandeMultipleByFilter($header = null, $pager = 15, $year = null, $month = null) 
    {
        $emails = [];
        $date_debut = new \Datetime($year.'-'.$month);
        $date_fin = new \Datetime($year.'-'.$month);
        $date_fin->add(new \DateInterval('P1M'));        
        
        $queryEmail = \Drupal::entityQueryAggregate(self::ID_RESERVATION_DEMANDE);
                           
        $queryEmail->condition('created', $date_debut->getTimestamp(), '>');
        $queryEmail->condition('created', $date_fin->getTimestamp(), '<');
        
        $queryEmail->conditionAggregate('rdid', 'COUNT', 1, '>');
        $queryEmail->groupby('email');
  
        $rows = $queryEmail->execute();
        foreach($rows as $row)
        {
            $emails[] = $row['email'];
        }
        
        $query = \Drupal::entityQuery(self::ID_RESERVATION_DEMANDE);
                                        
        $query->condition('created', $date_debut->getTimestamp(), '>');
        $query->condition('created', $date_fin->getTimestamp(), '<');

        $query->condition('statut', 'formulaire', '<>');
        
        if($emails)
        {
            $query->condition('email', $emails, 'IN');            
        }
        else {
            $query->condition('email', ' ', '=');
        }
   
        $query->tableSort($header);
        
        $query->pager($pager);
  
        return $this->getAll($query->execute());
    }
    
    public function getDemandeByNidType($nid, $type_email = 'demande') {
       
        $nodes = \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_DEMANDE)
                ->loadByProperties(['nid' => $nid, 'type_email' => $type_email]);
                
        return array_shift($nodes);
    }
    
    public function createDemande($rdid = null, $rhid = '0', $sid = null, $statut = null, $demandeur = null, $jauge = null, $email = null, $telephone = null) 
    {        
        $demande = ReservationDemande::create([
            'rdid' => $rdid,
            'rhid' => $rhid,
            'sid' => $sid,
            'statut' => $statut,
            'demandeur' => $demandeur,
            'jauge' => $jauge,
            'email' => $email,
            'telephone' => $telephone
        ]);
        $demande->save();
        
        return $demande;
    }
    
    public function editDemande($rdmid = null, $rdid = null, $rhid = null, $sid = null, $statut = null, $demandeur = null, $jauge = null, $email = null, $telephone = null) 
    {        
        $demande = $this->load($rdmid);
        if($rdid) 
        { 
            $demande->setRdid($rdid);
        }
        if($rhid) 
        { 
            $demande->setRhid($rhid);
        }
        if($statut) 
        { 
            $demande->setStatut($statut);
        }
        if($demandeur) 
        { 
            $demande->setDemandeur($demandeur);
        }
        if($jauge) 
        { 
            $demande->setJauge($jauge);
        }
        if($email) 
        { 
            $demande->setEmail($email);
        }
        if($telephone) 
        { 
            $demande->setTelephone($telephone);
        }
        $demande->save();
        
        return $demande;
    }
        
    /**
     * 
     * @param string $rdmid
     * @param string $action
     * @return type
     */
    public function setStatut(string $rdmid, string $action)
    {
        $reservationDemande = $this->load($rdmid);
        $reservationDemande->setStatut($action);
        return $reservationDemande->save(); 
    }

    /**
     * 
     * @param string $rdmid
     * @return type
     */
    public function deleteDemande(string $rdmid) 
    {
        $reservationDemande = $this->load($rdmid);
        $webformSubmission = $reservationDemande->getWebform();
        if($webformSubmission)
        {
            $webformSubmission->delete();            
        }
        return $reservationDemande->delete();
    }
    
    /**
     * 
     * @param array $rdmids
     * @return int
     */
    public function deleteMultipleDemande(array $rdmids) 
    {
        $count = 0;
        foreach($rdmids as $rdmid)
        {
            $this->deleteDemande($rdmid);
            $count++;
        }
        return $count;
    }
}
