<?php

/**
 * @file
 * Contains \Drupal\reservation\Service\ReservationMailServices.
 */

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationMail;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\Component\Render\FormattableMarkup;

class ReservationMailServices
{
    /**
     *
     * @var demandeServices 
     */
    protected $demandeServices;
    
    /**
     *
     * @var dateServices 
     */
    protected $dateServices;
    
    /**
     *
     * @var notificationServices 
     */
    protected $notificationServices;
    
    /**
     * 
     * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
     * @param \Drupal\reservation\Service\ReservationDateServices $dateServices
     * @param \Drupal\reservation\Service\ReservationNotificationServices $notificationServices
     */
    public function __construct(ReservationDemandeServices $demandeServices, ReservationDateServices $dateServices, ReservationNotificationServices $notificationServices) 
    {
       $this->demandeServices = $demandeServices;
       $this->dateServices = $dateServices;
       $this->notificationServices = $notificationServices;
    }
    
    /**
     * 
     * @param ReservationDemande $demande
     * @param string $type_email
     * @param bool $check
     */
    public function generateEmail(ReservationDemande $demande, string $type_email, bool $check = False) 
    {        
        if($check || $this->checkEmail($demande, $type_email))
        {
            return $this->createEmail($demande, $type_email);
        }       
    }
    
    /**
     * 
     * @param string $rdmid
     * @param string $type_email
     * @param bool $check
     */
    public function generateEmailById(string $rdmid, string $type_email, bool $check = False) 
    {        
        $demande = $this->demandeServices->load($rdmid);
        return $this->generateEmail($demande, $type_email, $check);     
    }
    
    /**
     * 
     * @param type $type_email
     * @param bool $check
     */
    public function generateMultipleEmailByType(string $type_email, $statut  = null, bool $check = False) 
    {        
        $count = 0;
        $demandes = $this->demandeServices->getAll(); 
        foreach($demandes as $demande)
        {
            if($this->generateEmail($demande, $type_email, $check))
            {
                $count++;
            }
        }
        
        \Drupal::logger('reservation')->info('Nombre d\'email ' . $type_email . ' envoyé : ' . $count);
    }
    
    /**
     * 
     * @param array $ids
     * @param type $type_email
     * @param type $check
     */
    public function generateMultipleEmailById(array $ids, string $type_email, bool $check = False) 
    {
        $count = 0;
        $demandes = $this->demandeServices->getAll($ids); 
        foreach($demandes as $demande)
        {
            if($this->generateEmail($demande, $type_email, $check))
            {
                $count++;
            }
        }
        
        \Drupal::logger('reservation')->info('Nombre d\'email ' . $type_email . ' envoyé : ' . $count);
    }
    
    /**
     * 
     * @param ReservationDemande $demande
     * @param type $type_email
     * @return boolean
     */
    public function checkEmail(ReservationDemande $demande, string $type_email)
    {
        $date_now = new \DateTime();
        $date = $demande->getDate();
        $date_condition = new \DateTime($date->getDate());
        
        $notification = $this->notificationServices->getNotificationByNidType($demande->getDate()->getReservationRessourceNode()->id(), $type_email);
        if($this->checkAlraidySend($notification->id(),$demande->id()))
        {
            return False;
        }
        
        if(!$notification->getStatut())
        {
            return False;
        }
        
        if($type_email == 'rappel' && $date->getRappel())
        {
            $date_condition->sub(new \DateInterval('P'.$date->getRappelJour().'D'));
            if($date_now->format('Y-m-d') != $date_condition->format('Y-m-d'))
            {
                return False;   
            }
        }
        
        if($type_email == 'enquete' && $date->getEnquete())
        {
            $date_condition->add(new \DateInterval('P'.$date->getEnqueteJour().'D'));
            if($date_now->format('Y-m-d') != $date_condition->format('Y-m-d'))
            {
                return False;   
            }
        }
        
        return True;    
    }
       
    /**
     * 
     * @param type $rnid
     * @param type $rdmid
     * @return type
     */
    public function checkAlraidySend($rnid,$rdmid)
    {
        $query = \Drupal::entityQuery('reservation_mail'); 
        $query->condition('rnid', $rnid);   
        $query->condition('rdmid', $rdmid);   
         
        return $query->count()->execute();   
    }
    
    /**
     * 
     * @param ReservationDemande $demande
     * @param type $type_email
     * @return boolean
     */
    public function createEmail(ReservationDemande $demande, $type_email)
    {
        $notification = $this->notificationServices->getNotificationByNidType($demande->getDate()->getReservationRessourceNode()->id(), $type_email);
        
        $langcode = \Drupal::config('system.site')->get('langcode');
        $module = 'reservation';
        $key = 'reservation_demande_mail';
        $to = $this->replaceElement($demande, $notification->getReservationRessourceNode()->getEmailTo());
        $reply = NULL;
        $send = TRUE;

        $params['message'] = $this->replaceElement($demande, $notification->getEmailCorps());
        $params['subject'] = $this->replaceElement($demande, $notification->getEmailObjet());
        $params['options']['title'] = 'Your wonderful title';
        $params['options']['footer'] = 'Your wonderful footer';
        $params['from'] = $notification->getEmailFrom();

        $mailManager = \Drupal::service('plugin.manager.mail');
        $result = $mailManager->mail($module, $key, $to, $langcode, $params, $reply, $send);
      
        if($result)
        {
            $mail = ReservationMail::create([
                    'rdmid' => $demande->id(),
                    'rnid' => $notification->id()                    
                ]);
            $mail->save();
            
            \Drupal::logger('reservation')
                    ->info('Email : ' . $type_email . ' de la demande n°' . $demande->id() . ' du ' . $demande->getCreatedFormat());

            return True;
        }
        else
        {
            return False;
        }
    }
    
    /**
     * 
     * @param ReservationDemande $demande
     * @param type $element
     * @return type
     */
    public function replaceElement(ReservationDemande $demande, $element)
    {
        $token_service = \Drupal::token();
        
        return $token_service->replace($element, 
            [
                'reservation_demande' => $demande,  
                'webform_submission' => $demande->getWebform()
            ]);
    }
}
