<?php

namespace Drupal\reservation\Service;


class ReservationRessourceServices
{
    const ID_RESERVATION_RESSOURCE = 'reservation_ressource';
        
    /**
     * 
     */
    public function __construct()
    {

    }
    
    /**
     * 
     * @param type $ids
     * @return type
     */
    public function getAll($ids = null)
    {
        return \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_RESSOURCE)
                ->loadMultiple($ids);
    }
    
    public function getRessources($statut = null) {
       
        $query = \Drupal::entityQuery(self::ID_RESERVATION_RESSOURCE);
        if($statut)
        {
            $query->condition('statut',1);
        }       
        
        return $this->getAll($query->execute());
    }

    public function getTypeStatut($statut = null) {

        $types = [];    
        $node_types = $this->getRessources($statut);
        foreach($node_types as $node_type)
        {
          $types[] = $node_type->get('type')->value;          
        }

        return $types;
    }
    
    public function getTypeExist($type, $statut = null) 
    {
        $query = \Drupal::entityQuery(self::ID_RESERVATION_RESSOURCE);
        $query->condition('type', $type);
        if($statut)
        {
            $query->condition('statut', $statut);            
        }
        $rows = $query->execute();

        return $rows ? True : False;
    }
    

    public function getRessourceByType($type) {

      $ressource = \Drupal::entityTypeManager()->getStorage(self::ID_RESERVATION_RESSOURCE)->loadByProperties(['type' => $type]);

      return isset($ressource) ? array_shift($ressource) : null;
    }  

    public function getNodeTypes() 
    {
        $entites = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();

        $contentTypesList = [];
        foreach ($entites as $entite) {
            $contentTypesList[$entite->id()] = $entite->label();
        }

        return $contentTypesList;    
    }

    public function getRessourceEnable() 
    {
        $rows =[];
        $types = $this->getTypeStatut(1);
        if($types == null)
        {
            return False;
        }

        $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => $types]);
        foreach($nodes as $node)
        {
          $rows[$node->Id()] = $node->getTitle();
        }

        return $rows;    
    }
        
    public function getRessourceDemandeEnable() 
    {
      $rows =[];
      $types = $this->getTypeStatut(1);
      $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => $types]);
      foreach($nodes as $node)
      {            
        $dates = \Drupal::entityTypeManager()->getStorage('reservation_date')->loadByProperties(['nid' => $node->Id()]);
        foreach($dates as $date)
        {        
            $demandes = \Drupal::entityTypeManager()->getStorage('reservation_demande')->loadByProperties(['rdid' => $date->Id()]);
            if($demandes)
            {
                $rows[$node->Id()] = $node->getTitle();                
            }
        }        
      }

      return $rows;    
    }
}
