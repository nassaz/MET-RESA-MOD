<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationDemandeToken;
use Drupal\reservation\Entity\ReservationDemande;

class ReservationDemandeTokenServices 
{   
    const ID_RESERVATION_DEMANDE_TOKEN = 'reservation_demande_token';
    
    /**
     *
     * @var demandeServices 
     */
    protected $demandeServices;
    
    /**
     *
     * @var demandeServices 
     */
    protected $timeLimit;

    /**
     * 
     * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
     */    
    public function __construct(ReservationDemandeServices $demandeServices) 
    {
       $this->demandeServices = $demandeServices;
       $reservationSettings = \Drupal::config('reservation.settings');
       $tokenSettings = $reservationSettings->get('token');
       $this->timeLimit = $tokenSettings['time_limit'];
    }
        
    /**
     * 
     * @param type $token
     * @return type
     */
    public function loadToken($token = null)
    {
        return \Drupal::entityTypeManager()
            ->getStorage(self::ID_RESERVATION_DEMANDE_TOKEN)
            ->load($token);
    }
        
    /**
     * 
     * @param type $ids
     * @return type
     */
    public function getAll($ids = null)
    {
        return \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_DEMANDE)
                ->loadMultiple($ids);
    }
    
    /**
     * 
     * @param type $token
     * @return type
     */
    public function loadDemandeByToken($token = null)
    {
        $demandeToken = ReservationDemandeToken::load($token);   
        if($demandeToken)
        {            
            return $demandeToken->getDemande();
        }
        else
        {
            return null;
        }
    }
    
    /**
     * 
     * @param type $token
     * @return type
     */
    public static function destroyToken($token = null)
    {
        $demandeToken = ReservationDemandeToken::load($token);
        
        return $demandeToken->delete();
    }
       
    /**
     * 
     * @param type $time
     * @return type
     */
    public function getTokenByTime($time = 0)
    {
        $date_limit = new \Datetime();
        $date_limit->sub(new \DateInterval('PT' . $time .'M'));

        $query = \Drupal::entityQuery('reservation_demande_token');
        $query->condition('created', $date_limit->getTimestamp(), '<');
        
        return ReservationDemandeToken::loadMultiple($query->execute());
    }
    
    /**
     *  Vérfication des tokens Actifs
     */
    public function checkToken($token) 
    {
        $this->destroyTokenObsolete();
        
        $date_now = new \Datetime();
        
        $demandeToken = ReservationDemandeToken::load($token);
        if($demandeToken)
        {
            $date_limit = $demandeToken->getCreatedFormat();
            $date_limit->add(new \DateInterval('PT' . $this->timeLimit .'M'));
            if($date_limit->getTimestamp() > $date_now->getTimestamp())
            {
                return True;
            }
            else
            {
                return False;
            }
        }
        else            
        {
            return False;
        }
    }
    
    public function messageTime($demandeToken) 
    {
        $date_limit = $demandeToken->getCreatedFormat();
        $date_limit->add(new \DateInterval('PT' . $this->timeLimit .'M'));
        drupal_set_message('Vous avez jusqu\'à : ' . $date_limit->format('H:i') . ' pour procéder à la réservation.' , 'error');
    }
    
    /**
     * 
     * @param ReservationDemande $demande
     * @param type $token
     * @return type
     */
    public function createDemandeToken(ReservationDemande $demande = null, $token = null) 
    {
        $reservationDemandeToken = ReservationDemandeToken::create([
            'token' => $token,
            'rdmid' => $demande->id()
        ]);
        $reservationDemandeToken->save();
        $this->setCookie($token);
        
        return $reservationDemandeToken;
    }
    
    /**
     *  Vérfication des tokens Actifs et suppression des demandes
     */
    public function destroyTokenObsolete() 
    {
        /**
         *  Suppression des Tokens dépassant le temps limite de Purge
         */
        foreach($this->getTokenByTime($this->timeLimit) as $token)
        {         
            $demande = $token->getDemande();
            if($demande)
            {
                $this->demandeServices->deleteDemande($demande->id());
            }
            $token->delete();
        }
        
    }
    
    /**
     * 
     * @param type $formBuildId
     * @param type $seconds
     */
    public function setCookie($token) // Création d'un Cookie avec un temps d'activation de 10 min par défaut
    {        
        $seconds = $this->timeLimit * 60;
        setCookie('formulaire_reservation', $token, time() + $seconds, '/' , NULL, FALSE);
    }

    /**
     * 
     * @return type
     */
    public function getCookie() // Récupération du Cookie
    {        
        return \Drupal::request()->cookies->get('formulaire_reservation');
    }
}
