<?php

namespace Drupal\reservation\Service;


class ReservationRessourceUserServices
{
    const ID_RESERVATION_RESSOURCE_USER = 'reservation_ressource_user';
    
    /**
     *
     * @var demandeServices 
     */
    protected $ressourceServices;

    /**
     * 
     * @param \Drupal\reservation\Service\ReservationRessourceServices $ressourceServices
     */
    public function __construct(ReservationRessourceServices $ressourceServices) 
    {
       $this->ressourceServices = $ressourceServices;
    }
    
    public function load($id = null)
    {
        return \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_RESSOURCE_USER)
                ->load($id);
    }
    
    /**
     * 
     * @param type $ids
     * @return type
     */
    public function getAll($ids = null)
    {
        return \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_RESSOURCE_USER)
                ->loadMultiple($ids);
    }
    
    /**
     * 
     * @param type $uid
     * @return type
     */
    public function queryRessourceUserByUid($uid = null, $nid = null) {
       
        $query = \Drupal::entityQuery(self::ID_RESERVATION_RESSOURCE_USER);
        if($uid)
        {
            $query->condition('user_id',$uid);
        }       
        if($nid)
        {
            $query->condition('nid',$nid);
        }       
        
        return $query->execute();
    }
    
    /**
     * 
     * @param type $uid
     * @return type
     */
    public function getRessourceUserByUid($uid = null) 
    {   
        return $this->getAll($this->queryRessourceUserByUid($uid));
    }
       
    /**
     * 
     * @param type $uid
     * @return type
     */
    public function getRessourceUserNidByUid($uid = null) 
    {   
        $ids = [];
        foreach($this->getRessourceUserByUid($uid) as $ressource_user)
        {
            if($ressource_user->getNode())
            {
                $ids[] = $ressource_user->getNode()->id();                
            }
        }
        return $ids;
    }
       
    
    /**
     * 
     * @param type $uid
     * @return type
     */
    public function getRessourceUserByUidRessource($uid = null, $nid = null) 
    {   
        return $this->getAll($this->queryRessourceUserByUid($uid, $nid));
    }
    
    /**
     * 
     * @param type $uid
     * @return type
     */
    public function getRessourceUserByNid($nid = null) 
    {   
        return $this->getAll($this->queryRessourceUserByUid(null, $nid));
    }
    
    /**
     * 
     * @param type $uid
     * @return type
     */
    public function getNidByUser($uid = null) {
       
        $tab = [];
        foreach($this->getRessourceUserByUid($uid) as $user)
        {
            $tab[] = $user->getNode()->id();
        }
        
        return $tab;
    }
        /**
     * 
     * @param type $uid
     * @return type
     */
    public function getUser($uid = null, $nid = null) 
    {              
        return current($this->getRessourceUserByUidRessource($uid, $nid));
    }
    
    public function getUsers($role = null, $nid = null) 
    {
        $users = \Drupal::entityTypeManager()->getStorage('user')->loadMultiple();
        
        $array = [];
        foreach ($users as $user) 
        {
            $query = \Drupal::entityQuery(self::ID_RESERVATION_RESSOURCE_USER);
            $query->condition('user_id',$user->id());
            $query->condition('nid',$nid);
            $exist_user = $query->count()->execute();
            
            if($user->id() && array_search ($role, $user->getRoles()) && !$exist_user)
            {
                $array[$user->id()] = $user->get('name')->value;
            }                
        }

        return $array;    
    }
    
    public function getRoles() 
    {
        $roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();

        $array = [];
        foreach ($roles as $role) {
            $array[$role->id()] = $role->label();
        }

        return $array;    
    }
    
    
    

        /**
     * {@inheritdoc}
     */
    public function getPublieMonth($year, $month, $nid, $publie = True) 
    {
        $query = $this->queryDate($nid, $year . '-' . $month, True, $publie); 
        $query->range(0,1);        
        return $query->count()->execute() ? True : False;      
    }
    
}
