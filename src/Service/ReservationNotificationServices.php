<?php

namespace Drupal\reservation\Service;


class ReservationNotificationServices
{
    const ID_RESERVATION_NOTIFICATION = 'reservation_notification';
        
    protected $reservationDemande;
    
    public function __construct() {
       
    }

    /**
     * 
     * @param type $ids
     * @return type
     */
    public function getAll($ids = null, $storage = self::ID_RESERVATION_NOTIFICATION)
    {
        return \Drupal::entityTypeManager()
                ->getStorage($storage)
                ->loadMultiple($ids);
    }  
    
    public function getNotificationById($nid, $storage = self::ID_RESERVATION_NOTIFICATION) {

        return \Drupal::entityTypeManager()
                ->getStorage($storage)
                ->load($nid);   
    }
    
    public function getNotificationByNidType($nid, $type_email = 'demande') {
       
        $nodes = \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_NOTIFICATION)
                ->loadByProperties(['nid' => $nid, 'type_email' => $type_email]);
                
        return array_shift($nodes);
    }
    
    public function getNotificationByType($type_email = 'rappel') {
       
        return \Drupal::entityTypeManager()
                ->getStorage(self::ID_RESERVATION_NOTIFICATION)
                ->loadByProperties(['type_email' => $type_email]);
    }
    
    public function generateStatutsTabs($nid = null, $type_email = null) 
    {
        
        $tabs = [];        
        $reservationSettings = \Drupal::config('reservation.settings');
        $rows = $reservationSettings->get('notification');
        foreach($rows as $key => $row)
        {
            $reservation = $this->getNotificationByNidType($nid, $key);
            $tabs[$key]['key'] = $key;
            $tabs[$key]['title'] = $row['details']['title'];
            $tabs[$key]['title_statut'] = $row['details']['statut'];
            $tabs[$key]['statut'] = $reservation->statut->value;
            $tabs[$key]['active'] = ($key === $type_email) ? True : False;
        }
        
        return $tabs;
    }
        
}
    
