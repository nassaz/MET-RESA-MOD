<?php

namespace Drupal\reservation\Service;

use Drupal\reservation\Entity\ReservationDemande;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class ReservationDemandeFormServices 
{
    /**
     *
     * @var demandeServices 
     */
    protected $demandeServices;
    
    /**
     *
     * @var mailServices 
     */
    protected $mailServices;
    
    /**
     * 
     * @param \Drupal\reservation\Service\ReservationDemandeServices $demandeServices
     * @param \Drupal\reservation\Service\ReservationDateServices $dateServices
     * @param \Drupal\reservation\Service\ReservationNotificationServices $notificationServices
     */
    public function __construct(ReservationDemandeServices $demandeServices, ReservationMailServices $mailServices)
    {
       $this->demandeServices = $demandeServices;
       $this->mailServices = $mailServices;
    }
   
        
    /**
     * 
     * @param type $form
     * @return string
     */
    public static function tableActionForm($form)
    {
        $form['table_action'] = [
            '#type' => 'table',
            '#tableselect' => False,
            '#weight' => 0,
        ];
        
        $form['table_action'][0]['confirme'] = [
          '#type' => 'submit',
          '#name' => 'confirme',
          '#value' => 'Confirmer',
        ];
        
        $form['table_action'][0]['attente'] = [
          '#type' => 'submit',
          '#name' => 'attente',
          '#value' => 'En attente',
        ];
        
        $form['table_action'][0]['refuse'] = [
          '#type' => 'submit',
          '#name' => 'refuse',
          '#value' => 'Refuser',
        ];
        
        $form['table_action'][0]['rappel'] = [
          '#type' => 'submit',
          '#name' => 'rappel',
          '#value' => 'Mail Rappel',
        ];
        
        $form['table_action'][0]['archive'] = [
          '#type' => 'submit',
          '#name' => 'archive',
          '#value' => 'Archive',
        ];
        
        $form['table_action'][0]['delete'] = [
          '#type' => 'submit',
          '#name' => 'delete',
          '#value' => 'Supprimer',
        ];
        
        return $form;
    }
      
    /**
     * 
     * @param FormStateInterface $form_state
     * @param type $field
     */
    public function setStatut(FormStateInterface $form_state, $field)
    {
        foreach($form_state->getValue('table_demande') as $rdmid)
        {
            if($rdmid)
            {
                $reservationDemande = ReservationDemande::load($rdmid);
                $reservationDemande->set('statut', $field);
                $reservationDemande->save();
            }
        }
    }
    
    
    /**
     * 
     * @param FormStateInterface $form_state
     */
    public function filteDemandeSimple(FormStateInterface $form_state) 
    {
        $ressource = $form_state->getValue(['table_filter', '1', 'ressource']);
        $statuts = [];
        foreach ($form_state->getValue(['table_filter', '1', 'statut']) as $statut)
        {
            if($statut)
            {
                $statuts[] = $statut;                
            }
        }
        
        $choix_date_demande = $form_state->getValue(['table_filter', '1', 'table_date_demande', '1', 'choix_date_demande']);
        $date_demande_debut = $form_state->getValue(['table_filter', '1', 'table_date_demande', '2', 'date_demande_debut']);
        $date_demande_fin = $form_state->getValue(['table_filter', '1', 'table_date_demande', '3', 'date_demande_fin']);
        
        $choix_date_creneau = $form_state->getValue(['table_filter', '1', 'table_date_creneau', '1', 'choix_date_creneau']);
        $date_creneau_debut = $form_state->getValue(['table_filter', '1', 'table_date_creneau', '2', 'date_creneau_debut']);
        $date_creneau_fin = $form_state->getValue(['table_filter', '1', 'table_date_creneau', '3', 'date_creneau_fin']);
        
        
        $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.simple', 
        [
            'ressource' => $ressource, 
            'statut' => $statuts,
            'choix_date_demande' => $choix_date_demande,
            'date_demande_debut' => $date_demande_debut,
            'date_demande_fin' => $date_demande_fin,
            'choix_date_creneau' => $choix_date_creneau,
            'date_creneau_debut' => $date_creneau_debut,
            'date_creneau_fin' => $date_creneau_fin
        ]));   
    }
        
    /**
     * 
     * @param type $form_state
     */
    public function filteDemandeMultiple($form_state) 
    {
        $year = $form_state->getValue(['table_filter', '1', 'year']);
        $month = $form_state->getValue(['table_filter', '1', 'month']);
        
        $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.multiple', 
            [
                'year' => $year,
                'month' => $month
            ]));
    }
        
    /**
     * 
     * @param type $form_state
     */
    public function filteDemande($form_state, $type_demande) 
    {
        if($type_demande == 'simple')
        {
            $this->filteDemandeSimple($form_state);
        }
        else 
        {
            $this->filteDemandeMultiple($form_state);
        }
    }
    
    /**
     * 
     * @param array $valeurs
     * @return array
     */
    public function selectedId(array $valeurs) 
    {
        $ids = [];
        foreach($valeurs as $valeur)
        {
            if($valeur)
            {
                $ids[] = $valeur;
            }
        }
        
        return $ids;
    }
    
    /**
     * 
     * @param type $form
     * @return string
     */
    public function submitActionForm(FormStateInterface $form_state, $type_demande)
    {
        $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
        $trigger = $form_state->getTriggeringElement();
        switch ($trigger['#name']) {
            case 'confirme':
                $this->setStatut($form_state, 'confirme');
                break;
            case 'attente':
                 $this->setStatut($form_state, 'attente');
                break;
            case 'refuse':
                 $this->setStatut($form_state, 'refuse');
                break;
            case 'archive':
                 $this->setStatut($form_state, 'archive');
                break;
            case 'rappel':
                $this->mailServices->generateMultipleEmailById($form_state->getValue('table_demande'), 'rappel', True);        
                break;
            case 'delete':
                $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.delete', 
                        ['rdmid' => $this->selectedId($form_state->getValue('table_demande')), 'type' => $type_demande, $request]));               
                break;
            case 'reset':
                $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.'.$type_demande));                
                break;
            case 'filtre':
                $this->filteDemande($form_state, $type_demande);                
                break;
        }
    }
        
    public function getStatutTab() {
                
        return [
            'confirme' => 'Confirmée',
            'attente' => 'En attente',
            'caution' => 'En attente Caution',
            'archive' => 'Archivée',
            'refuse' => 'Refusée',
            'formulaire' => 'Formulaire en traitement',
        ];
    }
        
    public function getHeaderTableSimple() 
    {
        return [
            'ressource'      => [
                'data'      => 'Ressource',
                'field'     => 'rdid.entity.nid.entity.title',
                'specifier' => 'rdid.entity.nid.entity.title',
            ],
            'datedemande'       => [
                'data'      => 'Date demande',
                'field'     => 'created',
                'specifier' => 'created',
                'sort' => 'desc',
            ],
            'demandeur'       => [
                'data'      => 'Demandeur',
                'field'     => 'demandeur',
                'specifier' => 'demandeur',
            ],
            'datecreneau'       => [
                'data'      => 'Date/Créneau',
                'field'     => 'rdid.entity.date',
                'specifier' => 'rdid.entity.date',
            ],
            'jauge'       => [
                'data'      => 'NB Inscrits',
            ],
            'statut'       => [
                'data'      => 'Statut',
                'field'     => 'statut',
                'specifier' => 'statut',
            ],
            'actions'       => [
                'data'      => 'Actions',
            ],
        ];
    }
    
    public function getHeaderTableMultiple() 
    {
        return [
            'email'      => [
                    'data'      => 'email',
                    'field'     => 'email',
                    'specifier' => 'email',
                    'sort' => 'asc',
            ],
            'telephone'      => [
                    'data'      => 'Téléphone',
                    'field'     => 'telephone',
                    'specifier' => 'telephone',
            ],
            'ressource'      => [
                    'data'      => 'Ressource',
                    'field'     => 'rdid.entity.nid.entity.title',
                    'specifier' => 'rdid.entity.nid.entity.title',
            ],
            'datedemande'       => [
                    'data'      => 'Date demande',
                    'field'     => 'created',
                    'specifier' => 'created',
                    'sort' => 'desc',
            ],
            'demandeur'       => [
                    'data'      => 'Demandeur',
                    'field'     => 'demandeur',
                    'specifier' => 'demandeur',
            ],
            'datecreneau'       => [
                    'data'      => 'Date/Créneau',
                    'field'     => 'rdid.entity.date',
                    'specifier' => 'rdid.entity.date',
            ],
            'jauge'       => [
                'data'      => 'Nb Inscrits',
            ],
            'statut'       => [
                    'data'      => 'Statut',
                    'field'     => 'statut',
                    'specifier' => 'statut',
            ],
            'actions'       => [
                    'data'      => 'Actions'
            ],
        ];
    }
        
    public function getStatutFormat($statut) {

        $tab = $this->getStatutTab();
        
        return $tab[$statut];
    }
}
