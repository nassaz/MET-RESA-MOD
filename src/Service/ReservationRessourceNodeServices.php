<?php

namespace Drupal\reservation\Service;


use Drupal\webform\Entity\Webform;



class ReservationRessourceNodeServices
{
    public function __construct()
    {

    }
    
    public static function getNodeById($nid) {

      $node = ReservationRessourceNode::load($nid);

      return $node;    
    }
    
    public static function queryNodeByRrid($rrid) {

        $query = \Drupal::entityQuery('reservation_ressource_node'); 
        $query->condition('rrid', $rrid);   
         
      return $query->execute();   
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCautionStatut() {
      return ($this->get('caution_statut')->value) ? True : False;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCautionStatut($caution_statut) {
      $this->set('caution_statut', $caution_statut);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCautionMontant() {
      return $this->get('caution_montant')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCautionMontant($caution_montant) {
      $this->set('caution_montant', $caution_montant);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAutomatique() {
      return $this->get('automatique')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setAutomatique($automatique) {
      $this->set('automatique', $automatique);
      return $this;
    }

    public static function setStatusById($nid, $statut) {

      $rows = \Drupal::entityTypeManager()->getStorage('reservation_ressource_node')->loadByProperties(['nid' => $nid]);
      foreach($rows as $row)
      {
        $row->statut->value = $statut;
        $row->save();
      } 
    }
    
    /**
     * 
     * @param type $nid
     * @return type
     */
    public function getWebform($nid = null)
    {
        $webforms = Webform::loadMultiple();
        foreach($webforms as $webform)
        {
            $elements = $webform->getElementsDecoded() ?: [];
            foreach($elements as $element)
            {
                if(isset($element["#type"]) && $element["#type"] === "webform_reservation" && isset($element["#value"]) && $element["#value"] === $nid)
                {
                    return $webform;   
                }        
            }
        } 
    }
    
    
    public function recurse_array($array, $form = []) 
    {
        foreach ($array as $key => $value)
        {
            if(isset($value["#title"]))
            {
                $form[] = ['title' => $value["#title"], 'token' => '[webform_submission:values:' . $key . ']'];
            }	

            if(is_array($value))
            {
                $form = $this->recurse_array($array[$key], $form) ;
            }
        }
        return $form;
    }

    public function getTokenTable($nid) 
    {
        $form = [];
        
        $form[] = ['title' => 'n° Demande', 'token' => '[reservation_demande:rdid:entity:rdid]'];
        $form[] = ['title' => 'Date Créneau', 'token' => '[reservation_demande:datecreneau]'];
        $form[] = ['title' => 'Date de la demande', 'token' => '[reservation_demande:datedemande]'];
        $form[] = ['title' => 'Titre de la ressource', 'token' => '[reservation_demande:ressource]'];
        
        if($this->getWebform($nid))
        {
            $webform = $this->getWebform($nid)->getElementsDecoded();
            $form = $this->recurse_array($webform, $form);
        }
        
        return $form;
    }

    
}
