<?php
/**
 * @file
 * Contains \Drupal\reservation\Plugin\QueueWorker\TaskWorkerReservationDemandeRemove.
 */
namespace Drupal\reservation\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;


/**
 * Processes Tasks for My Module.
 *
 * @QueueWorker(
 *   id = "reservation_mail",
 *   title = @Translation("Suppression des demandes dont la date est suérieu au param^tre en base"),
 *   cron = {"time" = 1}
 * )
 */
class TaskWorkerReservationMail extends QueueWorkerBase {
  
    /**
    * {@inheritdoc}
    */
    public function processItem($data) 
    {
        $mailServices  = \Drupal::service('reservation.mail');
        $mailServices->generateMultipleEmailByType('rappel', 'confirme');
        $mailServices->generateMultipleEmailByType('enquete', 'confirme');
    }
  
}