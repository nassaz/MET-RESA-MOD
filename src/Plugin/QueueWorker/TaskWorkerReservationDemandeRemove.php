<?php
/**
 * @file
 * Contains \Drupal\reservation\Plugin\QueueWorker\TaskWorkerReservationDemandeRemove.
 */
namespace Drupal\reservation\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\webform\Entity\WebformSubmission;

/**
 * Processes Tasks for My Module.
 *
 * @QueueWorker(
 *   id = "reservation_demande_remove",
 *   title = @Translation("Suppression des demandes dont la date est suérieu au param^tre en base"),
 *   cron = {"time" = 1}
 * )
 */
class TaskWorkerReservationDemandeRemove extends QueueWorkerBase {
  
    /**
    * {@inheritdoc}
    */
    public function processItem($data) {

        $reservationRessourceNode = \Drupal::service('reservation.ressource.node');
        $reservationRessource = \Drupal::service('reservation.ressource');
        $reservationDemande = \Drupal::service('reservation.demande');
        $reservationDate = \Drupal::service('reservation.date');
        $count = 0;
        
        $ressources = $reservationRessource->getRessources();
        foreach($ressources as $ressource)
        {
            $rdid = null;
            $demandes = null;

            $nodes = $reservationRessourceNode->queryNodeByRrid($ressource->id());

            if($nodes)
            {
                $rdid = $reservationDate->getRdidByNid($nodes);           
            }
            if($rdid)
            {
                $demandes = $reservationDemande->getDemandeByYear($rdid, $ressource->getRgpd());          
            }
            if($demandes)
            {
                $count += $this->removeDemandeWebform($demandes);
            }
        }

        \Drupal::logger('reservation')
                ->info('Nombre de demandes supprimées : ' . $count);
    }
  
    private function removeDemandeWebform($demandes) {

        $count = 0;
        foreach($demandes as $demande)
        {
            $demandeServices  = \Drupal::service('reservation.demande');
            $demandeServices->deleteDemande($demande->id());
            $count++;
        }
        return $count;
    }
}