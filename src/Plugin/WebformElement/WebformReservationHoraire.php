<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;

/**
 *
 * @WebformElement(
 *   id = "webform_reservation_horaire",
 *   label = @Translation("Réservation Horaire"),
 *   description = @Translation("Element permettant la sélection des horaires dispos pour une date."),
 *   category = @Translation("Reservation"),
 *   composite = FALSE,
 *   states_wrapper = FALSE,
 * )
 */
class WebformReservationHoraire extends WebformCompositeBase {

    
  public function getDefaultProperties() {
    $properties = parent::getDefaultProperties();

    $properties['title'] = 'Webform Réservation Horaire';
    
    return $properties;
  }

    
  
  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
          
      return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);

    $lines = [];
    

    
    return $lines;
  }


  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
            
//var_dump($form['validation']['required_container']);
//var_dump($form);
    $form['element']['title']['#disabled'] = True;
        
    $form['composite']['element'] = [];
    $form['composite']['flexbox'] = [];
    $form['access'] = [];
    $form['element_attributes'] = [];
          
    unset($form['form']);
    unset($form['element_description']);
    unset($form['element']['multiple']);
    unset($form['element']['multiple_error']);
    unset($form['element']['multiple__header_container']);
    unset($form['conditional_logic']);
    
    return $form;
  }

}

