<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'webform_calendar_element' element.
 *
 * @WebformElement(
 *   id = "webform_reservation_calendar",
 *   label = @Translation("Réservation Calendar"),
 *   description = @Translation("Element permettant la sélection des dates dispos."),
 *   category = @Translation("Reservation"),
 * )
 *
 * @see \Drupal\webform_datepicker_element\Element\WebformExampleElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformReservationCalendar extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
    $properties = parent::getDefaultProperties();

    $properties['showElements'] = '';
    $properties['showplacedate'] = '';
    $properties['showplacehoraire'] = '';
    $properties['title'] = 'Webform Réservation Calendar';

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    // Here you can customize the webform element's properties.
    // You can also customize the form/render element's properties via the
    // FormElement.
    //
    // @see \Drupal\webform_datepicker_element\Element\WebformExampleElement::processWebformElementExample
  }


  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form['element'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Element settings'),
      '#access' => TRUE,
      '#weight' => -50,
    ];
    
    $form['element']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => NULL,
      '#description' => $this->t('This is used as a descriptive label when displaying this webform element.'),
      '#required' => TRUE,
      '#disabled' => TRUE,
      '#attributes' => ['autofocus' => 'autofocus'],
    ];
    
    $form['element']['value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Value'),
      '#description' => $this->t('The value of the webform element.'),
    ];
    
    $form['element']['showElements'] = [
      '#title' => 'Afficher les autres élements du formulaire en permanence (si non coché, les élements seront affichés seulement si une date du calendrier est choisie)',
      '#type' => 'checkbox',  
    ];

    $form['element']['showplacedate'] = [
      '#title' => 'Afficher les places disponibles pour les dates.',
      '#type' => 'checkbox',  
    ];

    $form['element']['showplacehoraire'] = [
      '#title' => 'Afficher les places disponibles pour les horaires.',
      '#type' => 'checkbox',  
    ];

    $form['options'] = [];
    
    $form['options_other'] = [];

    $form['validation'] = [
      '#type' => 'details',
      '#title' => $this->t('Form validation'),
    ];
    
    $form['validation']['required_container'] = [
      '#type' => 'container',
      '#access' => TRUE,
    ];
    
    $form['validation']['required_container']['required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Required'),
      '#description' => $this->t('Check this option if the user must enter a value.'),
      '#return_value' => TRUE,
    ];
    
    $form['validation']['required_container']['required_error'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Required message'),
      '#description' => $this->t('If set, this message will be used when a required webform element is empty, instead of the default "Field x is required." message.'),
      '#states' => [
        'visible' => [
          ':input[name="properties[required]"]' => ['checked' => TRUE],
        ],
      ],
    ];
   
    return $form;
  }

}


