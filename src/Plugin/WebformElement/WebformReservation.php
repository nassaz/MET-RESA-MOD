<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\reservation\Entity\ReservationRessource;

/**
 * Provides a 'webform_reservation' element.
 *
 * @WebformElement(
 *   id = "webform_reservation",
 *   label = @Translation("Réservation"),
 *   description = @Translation("Element permettant le lien entre une ressource et un node."),
 *   category = @Translation("Reservation"),
 * )
 *
 * @see \Drupal\reservation\Element\WebformReservationTest
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformReservation extends WebformElementBase {

  /**
   * {@inheritdoc}
   */
  public function getDefaultProperties() {
      
    return  [
      'title' => 'Webform Réservation',
      'key' => 'webform_reservation_ressource_id',
      'machine-name' => 'webform_reservation_ressource_id',
      'value' => '',
    ];
  }
  
  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

  }


  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['title']['#disabled'] = True;
    $form['element']['key']['#disabled'] = True;
    
        
    $ressourceServices  = \Drupal::service('reservation.ressource');
    $ressourceEnable = $ressourceServices->getRessourceEnable();
    $form['element']['value'] = array(
      '#type' => 'select',
      '#title' => 'Ressource : ',
      '#options' => $ressourceEnable,
    );
    
    $form['options'] = [];
    
    $form['options_other'] = [];
    
    return $form;
  }

}

