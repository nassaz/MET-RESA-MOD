<?php

namespace Drupal\reservation\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;

/**
 *
 * @WebformElement(
 *   id = "webform_reservation_accompagnateur",
 *   label = @Translation("Réservation Accompagnateur"),
 *   description = @Translation("Element proposant un nombre d'accompagnateurs à une réservation."),
 *   category = @Translation("Reservation"),
 *   composite = FALSE,
 *   states_wrapper = FALSE,
 * )
 */
class WebformReservationAccompagnateur extends WebformCompositeBase {

    
  public function getDefaultProperties() {
    $properties = parent::getDefaultProperties();

    $properties['title'] = 'Webform Réservation Accompagnateur';
    $properties['jauge'] = 7;

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatHtmlItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
          
      return $this->formatTextItemValue($element, $webform_submission, $options);
  }

  /**
   * {@inheritdoc}
   */
  protected function formatTextItemValue(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    $value = $this->getValue($element, $webform_submission, $options);

    $lines = [];
    

    
    return $lines;
  }


  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
  
    $form['element']['title']['#disabled'] = True;
        
    $form['element']['jauge'] = [
          '#type' => 'number',
          '#title' => 'Jauge',
          '#default_value' => 5,
          '#validated' => TRUE
    ];

    
    $form['element']['multiple'] = [];
    $form['composite']['element'] = [];
    $form['composite']['flexbox'] = [];
    $form['access'] = [];
    $form['element_attributes'] = [];
            
    return $form;
  }

}

