<?php

namespace Drupal\reservation\Plugin\WebformHandler;

use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\WebformInterface;

/**
 * Webform submission action handler.
 *
 * @WebformHandler(
 *   id = "reservation_demande",
 *   label = @Translation("Réservation Demande"),
 *   category = @Translation("Réservation Demande"),
 *   description = @Translation("Création d'une demande de réservation suite à la soumission d'un webform"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class ReservationDemandeWebformHandler extends WebformHandlerBase 
{
    /**
     *
     * @var type 
     */
    protected $caution = False;
    
    /**
     *
     * @var type 
     */
    protected $token = False;
    
    /**
     * 
     * @param WebformSubmissionInterface $webform_submission
     * @param type $update
     */
    public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) 
    {
        if(!$webform_submission->getOriginalData())
        {
            $this->creationDemande($webform_submission);
        }
        else
        {
            $this->modificationDemande($webform_submission);
        }
    }  
    
    /**
     * 
     * @param WebformSubmissionInterface $webform_submission
     */
    public function creationDemande(WebformSubmissionInterface $webform_submission) 
    {
        $demandeTokenServices = \Drupal::service('reservation.demande.token');
        $submission = $webform_submission->getData();

        $token = $webform_submission->getToken();
        if($demandeTokenServices->checkToken($token) === True)
        {
            $this->token = $token;
            $demande = $demandeTokenServices->loadDemandeByToken($token);
            $ressourcenode = ReservationRessourceNode::load($submission["webform_reservation"]);
            if($ressourcenode->getCautionStatut())
            {
                $demande->setStatut('caution');
                $this->caution = True;
            }
            else
            {
                if($ressourcenode->getAutomatique())
                {
                    $demande->setStatut('confirme');
                }
                else
                {
                    $demande->setStatut('attente');
                }
            }

            $rdid = $submission["webform_reservation_calendar"];
            $rhid = $submission["webform_reservation_horaire"]['reservation-horaire-select'];
            $accompagnateur = $submission["webform_reservation_accompagnateur"]['reservation-accompagnateur-select'];

            $demande->setRdid($rdid);
            $demande->setRhid($rhid);
            $demande->setDemandeur($submission["nom"], $submission["prenom"]);
            $demande->setJauge(isset($accompagnateur) ? $accompagnateur + 1 : 1);
            $demande->setTelephone(isset($submission["telephone"]) ? $submission["telephone"] : '');
            $demande->setEmail($submission["email"]);
            $demande->setSid($webform_submission->Id());
            $demande->save();   
        }
    }

    /**
     * 
     * @param WebformSubmissionInterface $webform_submission
     */
    public function modificationDemande(WebformSubmissionInterface $webform_submission) 
    {
        $reservationDemande = \Drupal::service('reservation.demande');
        $submission = $webform_submission->getData();

        $rdmid = $reservationDemande->getByWebform($webform_submission->id());
        $demande = $reservationDemande->load($rdmid);
        if($demande === True)
        {
            $rdid = $submission["webform_reservation_calendar"];
            $rhid = $submission["webform_reservation_horaire"]['reservation-horaire-select'];
            $accompagnateur = $submission["webform_reservation_accompagnateur"]['reservation-accompagnateur-select'];
            
            $demande->setRdid($rdid);
            $demande->setRhid($rhid);
            $demande->setDemandeur($submission["nom"], $submission["prenom"]);
            $demande->setJauge(isset($accompagnateur) ? $accompagnateur + 1 : 1);
            $demande->setTelephone(isset($submission["telephone"]) ? $submission["telephone"] : '');
            $demande->setEmail($submission["email"]);
            $demande->setSid($webform_submission->Id());
            $demande->save();
            
            $mailServices  = \Drupal::service('reservation.mail');
            $mailServices->generateEmailById($demande->id(), 'modification');
        }
    }
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     * @param WebformSubmissionInterface $webform_submission
     * @return type
     */
    public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission)
    {
        parent::validateForm($form, $form_state, $webform_submission);
        if ($form_state->hasAnyErrors()) {
            return;
        }
        $storage = $form_state->getStorage();
        
        if($storage['current_page'] == 'page_1' && $storage['form_display']->getMode() == 'add')
        {
            $demandeServices = \Drupal::service('reservation.demande');
            $demandeTokenServices = \Drupal::service('reservation.demande.token');
            $caldendarServices = \Drupal::service('reservation.calendar');

            $submission = $webform_submission->getData();          
            $rdid = $submission["webform_reservation_calendar"];
            $rhid = $submission["webform_reservation_horaire"]['reservation-horaire-select'] ? $submission["webform_reservation_horaire"]['reservation-horaire-select'] : '0';
            $verification = $caldendarServices->verificationDisponibilite($rdid, $rhid);
            if($verification)
            {
                // Création d'une demande en début de formulaire, pour bloquer la dispo pour la durée du panier
                $token = $webform_submission->getToken();
                if($demandeTokenServices->checkToken($token))
                {
                    $demande = $demandeTokenServices->loadDemandeByToken($token);
                    $demandeToken = $demandeTokenServices->loadToken($token);
                    $demande->setRdid($rdid);
                    $demande->setRhid($rhid);
                    $demande->save();                    
                    $demandeToken->setCreatedTime($demande->getCreatedTime());
                    $demandeToken->save();                    
                }
                else
                {
                    $demande = $demandeServices->createDemande($rdid, $rhid, $webform_submission->Id(), 'formulaire', '', 1, '', '');
                    $demandeToken = $demandeTokenServices->createDemandeToken($demande, $token);
                    $demandeTokenServices->messageTime($demandeToken);                    
                }
            }
            else
            {
                $form_state->setErrorByName('reservation-calendar-datepicker', 'Jour en cours de réservation par un autre utilisateur.');  
            }
        } 
    }

    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     * @param WebformSubmissionInterface $webform_submission
     */
    public function confirmForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) 
    {
        $storage = $form_state->getStorage();
        if(\Drupal::currentUser()->hasPermission('administer reservation demande') && $storage['form_display']->getMode() == 'edit')
        {
            $url = \Drupal::request()->query->get('url');        
            $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
            
            $form_state->setRedirectUrl(Url::fromRoute($url, $request));  

        }
        else
        {
            $token = $webform_submission->getToken();
            if($this->caution)
            {
                $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.caution', ['token' => $token]));  
            }
            else
            {
                $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.affichage', ['token' => $token]));  
            }            
        }
    }
    
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     * @param WebformSubmissionInterface $webform_submission
     */
    public function alterForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) 
    { 
        $storage = $form_state->getStorage();
        if(\Drupal::currentUser()->hasPermission('administer reservation demande') && $storage['form_display']->getMode() == 'edit')
        {
           unset($form['information']);
           unset($form['navigation']);
           $demandeActionServices  = \Drupal::service('reservation.demande.form');
           $form = $demandeActionServices->tableActionForm($form);
        }
    }
    
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     * @param WebformSubmissionInterface $webform_submission
     */
    public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) 
    {
        $url = \Drupal::request()->query->get('url');        
        $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
        $storage = $form_state->getStorage();
        
        if(\Drupal::currentUser()->hasPermission('administer reservation demande') && $storage['form_display']->getMode() == 'edit')
        {
            $trigger = $form_state->getTriggeringElement();
            $demandeServices  = \Drupal::service('reservation.demande');
            $mailServices  = \Drupal::service('reservation.mail');
            
            $rdmid = $demandeServices->getByWebform($webform_submission->id());
            
            $form_state->setRedirectUrl(Url::fromRoute($url, $request));  
            
            switch ($trigger['#name']) 
            {
                case 'confirme':
                    $demandeServices->setStatut($rdmid, 'confirme');
                    $mailServices->generateEmailById($rdmid, 'confirme');
                    break;
                case 'attente':
                    $demandeServices->setStatut($rdmid, 'attente');
                    $mailServices->generateEmailById($rdmid, 'attente');
                    break;
                case 'refuse':
                    $demandeServices->setStatut($rdmid, 'refuse');
                    $mailServices->generateEmailById($rdmid, 'refuse');
                    break;
                case 'archive':
                    $demandeServices->setStatut($rdmid, 'archive');
                    break;
                case 'rappel':
                    $mailServices->generateEmailById($rdmid, 'rappel');       
                    break;
                case 'delete':
                    $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.delete', 
                        [
                            'rdmid' => [$rdmid], 
                            'type' => 'simple',
                            $request
                        ]));
                    break;
            }
        }        
    }
    
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     * @param WebformSubmissionInterface $webform_submission
     * @return string
     */
    public function actionDemandeForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) 
    { 
        $form['table_action'] = array(
            '#type' => 'table',
            '#tableselect' => False,
            '#weight' => 0,
        );

        $form['table_action'][0]['confirme'] = [
          '#type' => 'submit',
          '#name' => 'confirme',
          '#value' => 'Confirmer',
        ];

        $form['table_action'][0]['attente'] = [
          '#type' => 'submit',
          '#name' => 'attente',
          '#value' => 'En attente',
        ];

        $form['table_action'][0]['refuse'] = [
          '#type' => 'submit',
          '#name' => 'refuse',
          '#value' => 'Refuser',
        ];

        $form['table_action'][0]['rappel'] = [
          '#type' => 'submit',
          '#name' => 'rappel',
          '#value' => 'Mail Rappel',
        ];

        $form['table_action'][0]['archive'] = [
          '#type' => 'submit',
          '#name' => 'archive',
          '#value' => 'Archive',
        ];

        $form['table_action'][0]['delete'] = [
          '#type' => 'submit',
          '#name' => 'delete',
          '#value' => 'Supprimer',
        ];

        return $form;
    }
}
