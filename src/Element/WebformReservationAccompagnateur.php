<?php

namespace Drupal\reservation\Element;

use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a webform element for an address element.
 *
 * @FormElement("webform_reservation_accompagnateur")
 */
class WebformReservationAccompagnateur extends WebformCompositeBase {

    /**
     * 
     * @return string
     */
    public function getInfo() {
        $info = parent::getInfo();
        $info['#id'] = 'reservation-accompagnateur';
        
        return $info;
    }

    /**
     * 
     * @param array $element
     * @return string
     */
    public static function getCompositeElements(array $element) {
        $elements = [];
        if(isset($element["#jauge"]))
        {
            $jauge = $element["#jauge"];
        }
        else
        {
            $jauge = 5;
        }
        $elements['reservation-accompagnateur-select'] = [
          '#id' => 'reservation-accompagnateur-select',
          '#type' => 'select',
          '#title' => 'Nombre d\'accompagnants',
          '#options' => range(1, $jauge),
          '#validated' => TRUE
        ];

        return $elements;
    }
    
    /**
     * 
     * @param array $element
     * @return string
     */
    public static function preRenderCompositeFormElement($element) {
        
        $element['#attached']['library'][] = 'reservation/reservation.place';
        
        return $element;
    }
}
