<?php

namespace Drupal\reservation\Element;

use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a webform element for an address element.
 *
 * @FormElement("webform_reservation_horaire")
 */
class WebformReservationHoraire extends WebformCompositeBase {

    /**
     * 
     * @return string
     */
    public function getInfo() {
        $info = parent::getInfo();
        $info['#id'] = 'reservation-horaire';
        $info['#prefix'] = '<div id="reservation-horaire-wrapper">';
        $info['#suffix'] = '</div>';
        
        return $info;
    }

    /**
     * 
     * @param array $element
     * @return string
     */
    public static function getCompositeElements(array $element) {
        $elements = [];

        $elements['reservation-horaire-select'] = [
          '#id' => 'reservation-horaire-select',
          '#type' => 'select',
          '#title' => 'Choix de l\'horaire pour le <span id="span-reservation-horaire-title"></span></div>',
          '#validated' => TRUE,
          '#options' => ''
        ];

        return $elements;
    }
}
