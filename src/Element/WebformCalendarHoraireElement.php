<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform_calendar_horaire_element'.
 *
 * Webform elements are just wrappers around form elements, therefore every
 * webform element must have correspond FormElement.
 *
 * Below is the definition for a custom 'webform_calendar_horaire_element' which just
 * renders a simple text field.
 *
 * @FormElement("webform_calendar_horaire_element")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\webform_datepicker_element\Element\WebformExampleElement
 */
class WebformCalendarHoraireElement extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this); /*renvoi: 'Drupal\reservation\Element\WebformCalendarElement'*/
    return [
      '#input' => False,
      '#process' => [
        [$class, 'processCalendarHoraire'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateCalendarHoraire'],
      ],
      '#pre_render' => [
        [$class, 'preRenderCalendarHoraire'],
      ],
      '#theme' => 'input__webform_calendar_horaire_element',
      '#theme_wrappers' => ['form_element'],
      '#prefix' => '<select id="edit-calendarhoraire">',
      '#suffix' => '</select>',
    ];
  }

  /**
   * Processes a 'webform_datepicker_element' element.
   */
  public static function processCalendarHoraire(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add and manipulate your element's properties and callbacks.
    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_datepicker_element'.
   */
  public static function validateCalendarHoraire(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add custom validation logic.
  }

  /**
   * Prepares a #type 'email_multiple' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderCalendarHoraire(array $element) {
    
    $element['#title'] = 'Choix Horaire';
    Element::setAttributes($element, ['id', 'name', 'value', 'size', 'maxlength', 'placeholder', ]); 
    return $element;
  }

}
