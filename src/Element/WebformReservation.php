<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform_reservation'.
 *
 * Webform elements are just wrappers around form elements, therefore every
 * webform element must have correspond FormElement.
 *
 * Below is the definition for a custom 'webform_reservation' which just
 * renders a simple text field.
 *
 * @FormElement("webform_reservation")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\webform_reservation\Element\WebformReservation
 */
class WebformReservation extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#id' => 'webform-reservation-ressource-id',
      '#name' => 'webform_reservation_ressource_id',
      '#input' => TRUE,
      '#size' => 60,
      '#process' => [
        [$class, 'processWebformElementReservation'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateWebformReservation'],
      ],
      '#pre_render' => [
        [$class, 'preRenderWebformReservation'],
      ],
      '#theme' => 'input__webform_reservation',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes a 'webform_reservation' element.
   */
  public static function processWebformElementReservation(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add and manipulate your element's properties and callbacks.
    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_reservation'.
   */
  public static function validateWebformReservation(&$element, FormStateInterface $form_state, &$complete_form) {
    // Here you can add custom validation logic.
  }

  /**
   * Prepares a #type 'email_multiple' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderWebformReservation(array $element) {
    $element['#attributes']['type'] = 'hidden';
    Element::setAttributes($element, ['id', 'name', 'value']);
    static::setAttributes($element, ['form-text', 'webform-reservation-element']);
    return $element;
  }

}
