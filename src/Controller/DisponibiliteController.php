<?php
/**
 * @file
 * Contains \Drupal\reservation\Controller\DisponibiliteController.
 */
namespace Drupal\reservation\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\reservation\Service\ReservationDateServices;
use Drupal\reservation\Service\ReservationRessourceServices;
use Drupal\reservation\Service\ReservationHoraireServices;
use Drupal\reservation\Service\ReservationDemandeServices;
use Drupal\reservation\Service\ReservationRessourceNodeServices;
use Drupal\reservation\Service\ReservationRessourceUserServices;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\reservation\Form\DisponibiliteHoraireForm;
use Drupal\reservation\Form\DisponibiliteHoraireTableAddForm;
use Drupal\reservation\Form\DisponibiliteHoraireTableEditForm;
use Drupal\reservation\Form\DisponibiliteModeForm;
use Drupal\reservation\Form\DisponibiliteCautionForm;


class DisponibiliteController extends ControllerBase implements ContainerInjectionInterface {

    protected $ressource;

    protected $element = 0;

    protected $limit = 10;
    
    protected $reservationDate;
    
    protected $reservationHoraire;
    
    protected $reservationRessource;
    
    protected $reservationRessourceNode;
    
    protected $ressourceUserServices;
    
    protected $reservationDemande;

    
    public function __construct(ReservationDateServices $reservationDate, ReservationHoraireServices $reservationHoraire, 
            ReservationRessourceServices $reservationRessource, ReservationRessourceNodeServices $reservationRessourceNode,
             ReservationRessourceUserServices $ressourceUserServices, ReservationDemandeServices $reservationDemande) {
       
        $this->dateServices = $reservationDate;
        $this->horaireServices = $reservationHoraire;
        $this->ressourceServices = $reservationRessource;
        $this->ressourceNodeServices = $reservationRessourceNode;
        $this->ressourceUserServices = $ressourceUserServices;
        $this->demandeServices = $reservationDemande;
    }

    public static function create(ContainerInterface  $container) {

        return new static(
            $container->get('reservation.date'),
            $container->get('reservation.horaire'),
            $container->get('reservation.ressource'),
            $container->get('reservation.ressource.node'),
            $container->get('reservation.ressource.user'),
            $container->get('reservation.demande')
        );
    }
    
    public function liste() 
    {        
        $choix = \Drupal::request()->query->get('choix');
        
        if($choix == 'all')
        {
            $rows = $this->listeLine();  
            $choix = 'all';        
        }
        
        if($choix == 'user')
        {
            $user_id = \Drupal::currentUser();
            $rows = $this->listeLine($user_id->id());   
            $choix = 'user';                
        }    
        
        if($choix == null)
        {
            if(\Drupal::currentUser()->hasPermission('visualisation propre demande'))
            {
                $user_id = \Drupal::currentUser();
                $rows = $this->listeLine($user_id->id());   
                $choix = 'user';     
            }
            else 
            {
                $rows = $this->listeLine();  
                $choix = 'all';       
            }
        }
        
        return [
          '#theme' => 'template_disponibilite_liste',
          '#form' => \Drupal::formBuilder()->getForm('Drupal\reservation\Form\DisponibiliteUserForm', $choix),
          '#rows' => $rows,
          '#year' => date("Y")
        ];
    }
    

    public function listeLine(string $uid = null) 
    {
        $date_now = new \DateTime();
        $year = $date_now->format('Y');
        
        $ids = [];     
        $dates = [];  
        $types = $this->ressourceServices->getTypeStatut(1);
        $nodes = null;

        if($types)
        {
            $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => $types]);
        }

        if($uid)
        {
            $ids = $this->ressourceUserServices->getRessourceUserNidByUid($uid);
        }    

        foreach($nodes as $node)
        {       
            $nid = $node->id();        
            if($uid == null || in_array($nid, $ids))
            {   
                $ressourcenode = ReservationRessourceNode::load($nid);
                $dates[$nid]["nid"] = $nid;
                $dates[$nid]["title"] = $node->getTitle();
                $dates[$nid]["count"]["disponible"] = $this->dateServices->countDateOpen($nid, $year, '1');  
                $dates[$nid]["count"]["publie"] = $this->dateServices->countDatePublie($nid, $year, '1');  
                $form = new DisponibiliteModeForm($nid);
                $dates[$nid]["mode"] = \Drupal::formBuilder()->getForm($form, $ressourcenode); 
                $form_caution = new DisponibiliteCautionForm($nid);
                $dates[$nid]["caution"] = \Drupal::formBuilder()->getForm($form_caution, $ressourcenode); 
                
                $webform = $this->ressourceNodeServices->getWebform($nid);
                $dates[$nid]["webform"] = $webform ? $webform->id() : null; 
            }
        }

        return $dates; 
    }
        
    public function date() {

        $nid = \Drupal::request()->query->get('nid');
        
        return [
          '#theme' => 'template_disponibilite_date',
          '#tab_diponinilite' => $this->getTabs($nid, "date"),
          '#form' => \Drupal::formBuilder()->getForm('Drupal\reservation\Form\DisponibiliteDateForm'),
        ];
    }

    public function horaire() {

        $rows = [];
        $ressourceEnable = $this->ressourceServices->getRessourceEnable();

        $nid = \Drupal::request()->query->get('nid');
        $month = \Drupal::request()->query->get('month');
        $year = \Drupal::request()->query->get('year');
        
        if($nid == null)
        {
            $nid = current(array_keys($ressourceEnable));
        }        
        
        if($month == null || $year == null)
        {
            $date_now = new \DateTime();
            $reservationDate = $this->dateServices->getFirstDateById($nid, $date_now->format('Y'), True); 
            $node_date_month = $reservationDate ? $reservationDate->getDate() : null;
            $date_month = new \DateTime($node_date_month);
            $month = $date_month->format('m');
            $year = $date_month->format('Y');
        }
      
        $date = $year . '-' . $month;
        $tabs = $this->dateServices->getOpenMonth($nid, $month, $year, True);
        $ressourceDates = $this->dateServices->getDateOpen($nid, $date, True);
        foreach($ressourceDates as $ressourceDate)
        {
            $rdid = $ressourceDate->Id();
            $horaire = $ressourceDate->getHoraire();
            $date = new \DateTime($ressourceDate->getDate());
            
            $rows[$rdid]["date"] = $date->format('d/m/Y');
            
            $form = new DisponibiliteHoraireForm($rdid, $nid, $month, $year);
            $rows[$rdid]["form_date"] = \Drupal::formBuilder()->getForm($form, $ressourceDate);

            if($horaire == True)
            {
                $reservationHoraires = $this->horaireServices->getByDate($rdid);
                
                $form = new DisponibiliteHoraireTableAddForm($rdid, $nid, $month, $year);
                $rows[$rdid]["form_horaire_add"] = \Drupal::formBuilder()->getForm($form);    
                $creaneau = 1;
                foreach($reservationHoraires as $reservationHoraire)
                {
                    $rhid = $reservationHoraire->id();
                    $form = new DisponibiliteHoraireTableEditForm($rdid, $rhid, $nid, $month, $year);
                    $rows[$rdid]["form_horaire_line"][$rhid] = \Drupal::formBuilder()->getForm($form, $reservationHoraire, $creaneau);
                    $creaneau++;
                }
            }            
        }         
        
        return [
          '#theme' => 'template_disponibilite_horaire',
          '#tab_diponinilite' => $this->getTabs($nid, "horaire"),
          '#form_select' => \Drupal::formBuilder()->getForm('Drupal\reservation\Form\DisponibiliteHoraireSelectForm', $ressourceEnable, $nid, $month, $year),
          '#tabs' => $tabs,
          '#rows' => $rows,
          '#nid' => $nid,
          '#year' => $year,
        ];
    }

    public function getTabs($nid, $tab) 
    {                
        $tabs = [];
        $tabs["disponibilite"]['key'] = "disponibilite";
        $tabs["disponibilite"]['title'] = "Disponibilité";
        $tabs["disponibilite"]['link'] = Url::fromRoute('reservation.disponibilite.index', ['absolute' => TRUE]);                 
        
        $tabs["date"]['key'] = "date";
        $tabs["date"]['title'] = "Saise des Dates";
        $tabs["date"]['link'] = Url::fromRoute('reservation.disponibilite.date', ['nid' => $nid], ['absolute' => TRUE]);                 

        $tabs["horaire"]['key'] = "horaire";
        $tabs["horaire"]['title'] = "Saise des Horaires";
        $tabs["horaire"]['link'] = Url::fromRoute('reservation.disponibilite.horaire', ['nid' => $nid], ['absolute' => TRUE]);
        
        $tabs[$tab]['active'] = True;
            
        return $tabs;
    }
}


