<?php
/**
 * @file
 * Contains \Drupal\reservation\Controller\DemandeController.
 */
namespace Drupal\reservation\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Unicode;
use Drupal\file\Entity\File;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Drupal\reservation\Service\ReservationDemandeServices;
use Drupal\reservation\Service\ReservationDemandeFormServices;
use Drupal\reservation\Service\ReservationDemandeTokenServices;
use Drupal\reservation\Service\ReservationMailServices;
use Drupal\Core\Url;

class DemandeController extends ControllerBase implements ContainerInjectionInterface {
    
    /**
     *
     * @var type 
     */
    protected $demandeServices;
    
    /**
     *
     * @var type 
     */
    protected $demandeTokenServices;
    
    /**
     *
     * @var type 
     */
    protected $reservationMail;

    /**
     *
     * @var type 
     */
    protected $demandeFormServices;

    /**
     * 
     * @param ReservationDemandeServices $demandeServices
     * @param ReservationMailServices $reservationMail
     */
    public function __construct(ReservationDemandeServices $demandeServices, ReservationDemandeTokenServices $demandeTokenServices, ReservationMailServices $reservationMail, ReservationDemandeFormServices $demandeFormServices) 
    {       
        $this->demandeServices = $demandeServices;
        $this->demandeTokenServices = $demandeTokenServices;
        $this->mailServices = $reservationMail;
        $this->demandeFormServices = $demandeFormServices;
    }

    /**
     * 
     * @param ContainerInterface $container
     * @return \static
     */
    public static function create(ContainerInterface $container) {
          
        return new static(
            $container->get('reservation.demande'),
            $container->get('reservation.demande.token'),
            $container->get('reservation.mail'),
            $container->get('reservation.demande.form')
        );
    }
    
    /**
     * 
     * @return type
     */
    public function detail() {
        
        $year = \Drupal::request()->query->get('year');
        $month = \Drupal::request()->query->get('month');
        
        $form = \Drupal::formBuilder()->getForm('Drupal\reservation\Form\DemandeMultipleForm', $year, $month);

        return [
            '#type' => 'markup',
            '#markup' => $form
        ];      
    }
    
    /**
     * 
     * @param string $rdmid
     * @param string $action
     * @param string $url
     * @return type
     */
    public function action(string $rdmid = null, string $action = null, string $url = null) 
    {
        $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
        switch ($action) 
        {
            case 'confirme':
                $this->demandeServices->setStatut($rdmid, 'confirme');
                $this->mailServices->generateEmailById($rdmid, 'confirme');
                break;
            case 'attente':
                $this->demandeServices->setStatut($rdmid, 'attente');
                $this->mailServices->generateEmailById($rdmid, 'attente');
                break;
            case 'refuse':
                $this->demandeServices->setStatut($rdmid, 'refuse');
                $this->mailServices->generateEmailById($rdmid, 'refuse');
                break;
            case 'archive':
                $this->demandeServices->setStatut($rdmid, 'archive');
                break;
            case 'rappel':
                $this->mailServices->generateEmailById($rdmid, 'rappel', true);       
                break;
            case 'delete':
                return $this->redirect(Url::fromRoute('reservation.demande.delete', [
                    'rdmid' => [$rdmid],
                    $request]));
                
        }   
        
        return $this->redirect($url, [$request]);
    }
    
    /**
     * 
     * @return type
    */
    public function exportDemandeSimple()
    {
        $nid = \Drupal::request()->query->get('ressource');
        $statut = \Drupal::request()->query->get('statut');
        $date_debut = \Drupal::request()->query->get('date_debut');
        $date_fin = \Drupal::request()->query->get('date_fin');
        
        if($date_debut == null)
        {
            $date_debut_format = new \Datetime();
            $date_debut_format->sub(new \DateInterval('P1M'));
            $date_debut = $date_debut_format->format('Y-m-d');
        }
        else
        {
            $date_debut_format = new \Datetime($date_debut);                
        }
            
        if($date_fin == null)
        {
            $date_fin_format = new \Datetime();
            $date_fin_format->add(new \DateInterval('P1D'));
            $date_fin = $date_fin_format->format('Y-m-d');  
        }
        else
        {
            $date_fin_format = new \Datetime($date_fin);            
        }
        
        $escape = "\r\n";
        $delimiter = ";";
        $datas = "Ressource; Date Demande; Demandeur; Date Créneau; Email; Téléphone; Participants; Statut";
        
        $header = $this->demandeFormServices->getHeaderTableSimple();  
        $demandes = $this->demandeServices->getDemandeByFilter($header, 0, $nid, $statut, $date_debut, $date_fin);
        foreach($demandes as $demande)
        {
            $datas .= $escape;
            $datas .= $demande->getDate()->getReservationRessourceNode()->getNode()->getTitle();
            $datas .= $delimiter;
            $datas .= $demande->getCreatedFormat();
            $datas .= $delimiter;
            $datas .= $demande->getDemandeur();
            $datas .= $delimiter;
            $datas .= $demande->getDateCreneau();
            $datas .= $delimiter;
            $datas .= $demande->getEmail();
            $datas .= $delimiter;
            $datas .= $demande->getTelephone();
            $datas .= $delimiter;
            $datas .= $demande->getJauge();
            $datas .= $delimiter;
            $datas .= $this->demandeFormServices->getStatutFormat($demande->getStatut());
            $datas .= $delimiter;
        }
        
        return $this->exportCSV($datas, 'simple');            
    }
       
    /**
     * 
     * @return type
     */
    public function exportDemandeMultiple()
    {
        $year = \Drupal::request()->query->get('year');
        $month = \Drupal::request()->query->get('month');
                
        $date_now = new \Datetime();
        
        if($year == null)
        {
            $year = $date_now->format('Y');
        }

        if($month == null)
        {
            $month = $date_now->format('m');  
        }
        
        $escape = "\r\n";
        $delimiter = ";";
        $datas = "Email; Téléphone; Ressource; Date Demande; Demandeur; Date Créneau; Statut";
        $header = $this->demandeFormServices->getHeaderTableMultiple();  
        $demandes = $this->demandeServices->getDemandeMultipleByFilter($header, 0, $year, $month);  
        foreach($demandes as $demande)
        {
            $datas .= $escape;
            $datas .= $demande->getEmail();
            $datas .= $delimiter;
            $datas .= $demande->getTelephone();
            $datas .= $delimiter;
            $datas .= $demande->getDate()->getReservationRessourceNode()->getNode()->getTitle();
            $datas .= $delimiter;
            $datas .= $demande->getDemandeur();
            $datas .= $delimiter;
            $datas .= $demande->getCreatedFormat();
            $datas .= $delimiter;
            $datas .= $demande->getDateCreneau();
            $datas .= $delimiter;
            $datas .= $demande->getJauge();
            $datas .= $delimiter;
            $datas .= $this->demandeFormServices->getStatutFormat($demande->getStatut());
            $datas .= $delimiter;
        }
        
        return $this->exportCSV($datas, 'multiple');            
    }

    public function exportCSV($datas = [], $mode = null)
    {
      
        $scheme = 'temporary';
        $filename = 'export_demande_' . $mode . '_' . date('YmdHis') . '.csv';
        
        $file = File::create([
          'uid' => 1,
          'filename' => $filename,
          'uri' => $scheme . '://'. $filename,
          'status' => 1,
        ]);
        $file->save();
        
        file_put_contents($file->getFileUri(), utf8_decode($datas));
        
        $mimetype = Unicode::mimeHeaderEncode($file->getMimeType());
        $headers = [
          'Content-Type'              => $mimetype,
          'Content-Disposition'       => 'attachment; filename="' . $file->getFilename() . '"',
          'Content-Length'            => $file->getSize(),
          'Content-Transfer-Encoding' => 'binary',
          'Pragma'                    => 'no-cache',
          'Cache-Control'             => 'must-revalidate, post-check=0, pre-check=0',
          'Expires'                   => '0',
          'Accept-Ranges'             => 'bytes',
        ];
        
        return new BinaryFileResponse($file->getFileUri(), 200, $headers, $scheme !== 'private');
    }

    /**
     * Traitement de la demande suite au retour depuis OGONE
     * 
     * @param string $etat
     * @param string $token
     * @return type
     */
    public function cautionRedirection(string $token = null)
    {
        if($this->demandeTokenServices->checkToken($token))
        {
            $demande = $this->demandeTokenServices->loadDemandeByToken($token);
            $form = \Drupal::formBuilder()->getForm('Drupal\reservation\Form\DemandeCautionForm', $token, $demande);
            
            return [
              '#theme' => 'template_demande_caution',
              '#form' => $form,
            ];     
        }
        else
        {
            return $this->affichage($token);
        }
     
    }
    
    /**
     * Traitement de la demande suite au retour depuis OGONE
     * 
     * @param string $etat
     * @param string $token
     * @return type
     */
    public function caution(string $etat = null, string $token = null)
    {
        $reservationSettings = \Drupal::config('reservation.settings');
        // Test si le token utilisé est toujours actif, sinon suppression du token + demande (dans la fonction checkToken)
        if($this->demandeTokenServices->checkToken($token) === True)
        {
            $demande = $this->demandeTokenServices->loadDemandeByToken($token);
            $this->demandeTokenServices->destroyToken($token);
            if($etat === 'accept')
            { 
                if($demande->getDate()->getReservationRessourceNode()->getAutomatique())
                {
                    $demande->setStatut('confirme');
                    $message = $reservationSettings->get('message_validation')['message_confirmation_acceptee'];
                }
                else
                {
                    $demande->setStatut('attente');
                $message = $reservationSettings->get('message_validation')['message_confirmation_pre_reservation'];
                }
                $demande->save();
                
                // Envoi Mail confirmation enregistrement Réservation
                $mailServices  = \Drupal::service('reservation.mail');
                $mailServices->generateEmailById($demande->id(), $demande->getStatut());
            }
            elseif($etat === 'cancel')
            {
                $this->demandeServices->deleteDemande($demande->id());
                $message = $reservationSettings->get('message_validation')['message_annulee'];
            }
            else
            {
                $this->demandeServices->deleteDemande($demande->id());
                $message = $reservationSettings->get('message_validation')['message_incident'];
            }
        }
        else
        {
            $message = $reservationSettings->get('message_validation')['message_temps_depasse'];
        }
        
        return [
            '#type' => 'markup',
            '#markup' => '<h2>' . $message . '</h2>'
        ];      
    }
    
    /**
     * Traitement de la demande suite au retour depuis OGONE
     * 
     * @param string $etat
     * @param string $token
     * @return type
     */
    public function affichage(string $token = null)
    {
        $reservationSettings = \Drupal::config('reservation.settings');
        // Test si le token utilisé est toujours actif, sinon suppression du token + demande (dans la fonction checkToken)
        if($this->demandeTokenServices->checkToken($token) === True)
        {
            $demande = $this->demandeTokenServices->loadDemandeByToken($token);
            $this->demandeTokenServices->destroyToken($token);

            if($demande->getStatut() == 'confirme')
            {
                $message = $reservationSettings->get('message_validation')['message_confirmation_acceptee'];
            }
            else
            {
                $message = $reservationSettings->get('message_validation')['message_confirmation_pre_reservation'];
            }
            $demande->save();

            // Envoi Mail confirmation enregistrement Réservation
            $mailServices  = \Drupal::service('reservation.mail');
            $mailServices->generateEmailById($demande->id(), $demande->getStatut());
        }
        else
        {           
            $message = $reservationSettings->get('message_validation')['message_temps_depasse'];     
        }
        
        return [
            '#type' => 'markup',
            '#markup' => '<h2>' . $message . '</h2>'
        ];      
    }
    
}


