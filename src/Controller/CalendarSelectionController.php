<?php
/**
 * @file
 * Contains \Drupal\reservation\Controller\DatePickerSelectionController.
 */
namespace Drupal\reservation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\reservation\Service\ReservationDateServices;
use Drupal\reservation\Service\ReservationDemandeServices;
use Drupal\reservation\Service\ReservationDemandeTokenServices;
use Drupal\reservation\Service\ReservationCalendarServices;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class CalendarSelectionController extends ControllerBase implements ContainerInjectionInterface {

    const STATUT_RESERVE = 'reserveDates';
    
    const STATUT_WAIT = 'waitDates';
    
    const STATUT_OPEN = 'openDates';
    
    protected $reservationDate;
    
    protected $demandeTokenServices;
    
    protected $reservationDemande;
    
    protected $calendarServices;
    
    public function __construct(ReservationDateServices $dateServices, ReservationDemandeTokenServices $tokenServices, 
            ReservationDemandeServices $demandeServices, ReservationCalendarServices $calendarServices) 
    {       
        $this->dateServices = $dateServices;
        $this->tokenServices = $tokenServices;
        $this->demandeServices = $demandeServices;
        $this->calendarServices = $calendarServices;
    }

    public static function create(ContainerInterface  $container) {

        return new static(
            $container->get('reservation.date'),
            $container->get('reservation.demande.token'),
            $container->get('reservation.demande'),
            $container->get('reservation.calendar')
        );
    }
    
    
    /**
     * Tableau de données regroupant les dates et les horaires disponibles avec le statut
     * exemple :      
     * [
     *       {   // date sans horaire
     *           "rdid":"10",
     *           "date":"06\/28\/2018",
     *           "statut":"waitDates",
     *           "horaire":false
     *       },
     *       {   // date avec horaire
     *           "rdid":"44",
     *           "date":"08\/01\/2018",
     *           "statut":"openDates",
     *           "horaire":
     *               [
     *                   {
     *                       "rhid":"458",
     *                       "heure":"10:10",
     *                       "place":10
     *                   },
     *                   {
     *                       "rhid":"488",
     *                       "heure":"12:20",
     *                       "place":1
     *                   }
     *              ]
     *      },
     *  ]
     * 
     * 
     * @param type $nid
     * @return JsonResponse
     */
    public function getDate($nid){
        
        // Purge des demandes obsolètes
        $this->tokenServices->destroyTokenObsolete();
        
        $date_now = new \DateTime();
        $year = $date_now->format('Y');

        // Extraction des demandes publiées et leurs statuts
        $dates = [];
        if(\Drupal::currentUser()->hasPermission('creation manuelle demande'))
        {
            $datePublies = $this->dateServices->getDatePublie($nid, $year, null);
        }
        else
        {
            $datePublies = $this->dateServices->getDatePublie($nid, $year);
        }        
        foreach($datePublies as $datePublie)
        {
            if($datePublie->getHoraire())                
            {
                // Récupération des dates ayant un horaire
                $dates[] = $this->calendarServices->getDemandeHoraire($datePublie);
            }
            else
            {
                // Récupération des dates seules
                $dates[] = $this->calendarServices->getDemandeDate($datePublie);
            }
        }
        
        return new JsonResponse($dates);
    } 
    
    public function getPlace()
    {
        $token = $this->tokenServices->getCookie();
        $demande = $this->tokenServices->loadDemandeByToken($token);
        if($demande == null)
        {
            $place = 0;
        }
        else
        {
            if($demande->getHoraire())                
            {
                $date = $this->calendarServices->getDemandeHoraireDetail($demande->gethoraire());
            }
            else
            {
                $date = $this->calendarServices->getDemandeDate($demande->getDate());
            }
            $place = $date["place"] + 0;
        }

        return new JsonResponse($place);
    } 
}
