<?php
/**
 * @file
 * Contains \Drupal\reservation\Controller\RessourceController.
 */
namespace Drupal\reservation\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\reservation\Service\ReservationRessourceServices;
use Drupal\reservation\Service\ReservationNotificationServices;
use Drupal\reservation\Form\ParametreNotificationForm;
use Drupal\reservation\Form\ParametreNotificationFilterForm;


class ParametreController extends ControllerBase implements ContainerInjectionInterface {

    
    protected $reservationRessource;
    
    protected $reservationNotification;
    
    public function __construct(ReservationRessourceServices $reservationRessource, ReservationNotificationServices $reservationNotification) {
       
        $this->reservationRessource = $reservationRessource;
        $this->reservationNotification = $reservationNotification;
    }

    public static function create(ContainerInterface  $container) {

        return new static(
            $container->get('reservation.ressource'),
            $container->get('reservation.notification')
        );
    }
    
    public function notification($nid = 'nid', $type_email = 'type_email') {
        
        $ressources = $this->reservationRessource->getRessourceEnable();  
        
        if($ressources == null)
        {
            drupal_set_message('!! Pas de ressource paramétré !!', 'error');
            return $this->redirect('reservation.parametre.ressource');
        }
        
        if($nid === 'nid' || $type_email === 'type_email')
        {
            $nid = key($ressources);     
            $statutstabs = $this->reservationNotification->generateStatutsTabs($nid);
            $type_email = key($statutstabs);
            
            return $this->redirect('reservation.parametre.notification', ['nid' => $nid, 'type_email' => $type_email]);
        }
        
        $statutstabs = $this->reservationNotification->generateStatutsTabs($nid, $type_email);  
        $notification = $this->reservationNotification->getNotificationByNidType($nid, $type_email);  

        $form_filter = new ParametreNotificationFilterForm($notification, $ressources, $statutstabs, $type_email, $nid);
        $form_email = new ParametreNotificationForm($notification->id());
        
        return [
          '#theme' => 'template_parametre_notification',
          '#form_email' => \Drupal::formBuilder()->getForm($form_email, $notification),
          '#form_filter' => \Drupal::formBuilder()->getForm($form_filter),
          '#tabs' => $statutstabs,
          '#$type_email' => $type_email,
          '#nid' => $nid
        ];
    }
      
}


