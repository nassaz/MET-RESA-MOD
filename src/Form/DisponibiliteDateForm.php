<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteDateForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDate;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteDateForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'disponibilite_date_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        /*
         *  Initialisation des variables et librairies
         * 
        */
        $reservationRessource = \Drupal::service('reservation.ressource');
        $reservationDate = \Drupal::service('reservation.date');
        
        $jour_fr = [
          1 => 'Lu',
          2 => 'Ma',
          3 => 'Me',
          4 => 'Je',
          5 => 'Ve',
          6 => 'Sa',
          7 => 'Di'
        ];
        $form['#attached']['library'][] = 'reservation/reservation_date';
        
        $ressourceEnable = $reservationRessource->getRessourceEnable();
        
        $nid = \Drupal::request()->query->get('nid');
        $year = \Drupal::request()->query->get('year');

        if($nid == null)
        {
            $nid = current(array_keys($ressourceEnable));
        }        

        if($year == null)
        {
            $date_now = new \DateTime();
            $year = $date_now->format('Y');
        }

        /*
         *  Génération des filtres Ressource et année
         * 
        */
                
        $form['table_filtre'] = array(
            '#type' => 'table',
            '#tableselect' => FALSE,
        );    
        
        $form['table_filtre'][0]['select_ressource'] = array(
            '#type' => 'select',
            '#title' => 'Ressource : ',
            '#options' => $ressourceEnable,
            '#default_value' => $nid,
            '#ajax'         => [
                'callback'  => '::redirectPage',
                'wrapper'   => 'mode',
            ],
        );   
        
        $range_year = range($reservationDate->getYearMin(), $reservationDate->getYearMax() + 1);
        $form['table_filtre'][0]['select_year'] = array(
            '#type' => 'select',
            '#title' => 'Année : ',
            '#options' => array_combine($range_year, $range_year),
            '#default_value' => $year,
            '#ajax'         => [
                'callback'  => '::redirectPage',
                'wrapper'   => 'mode',
            ],
        );   
        
        
        /*
         *  Génération du tableau de dates
         * 
        */
        
        $datesExists = $reservationDate->getFormDatesExist($nid,$year);
        
        $start = new \DateTime(date("Y-01-01"));
        $date_now = new \DateTime();

        $interval_month = new \DateInterval("P1M");
        $period_month = new \DatePeriod($start,$interval_month,11);

        $hearders = array("Publier", "mois", "Tout");

        $form['table_date'] = array(
            '#type' => 'table',
            '#header' => array_merge($hearders, range(1,31)),
            '#tableselect' => FALSE,
        );    

        foreach($period_month as $month) {
            $month_word[] = $month->format("M");
            $month_num[] = $month->format("m");
        }

        foreach ($month_num as $key => $month) {

            $start = \DateTime::createFromFormat("Ymd",$year.$month.'01');
            $interval_day = new \DateInterval("P1D");

            $period_day = new \DatePeriod($start,$interval_day,$start->format("t")-1);
            $disable_month = $start->format('Y-m') < $date_now->format('Y-m') ? True : False;

            $form['table_date'][$month]['publie'] = array(
                '#type' => 'checkbox',
                '#default_value' => $reservationDate->getPublieMonth($year, $month, $nid),  
            );
                        
            $form['table_date'][$month]['publie'] = [
              '#type' => 'submit',
              '#value' => 'Publier',
              '#name' => 'submit-publie-'.$month,
            ];

            $form['table_date'][$month]['MONTH'] = array(
                '#type' => 'label',
                '#title' => $month_word[$key]
            );

            $form['table_date'][$month]['ALL'] = array(
                '#type' => 'checkbox',
                '#default_value' => False,  
                '#disabled' => $disable_month,
            );

            foreach($period_day as $day){

                $disable_day = $day->format('Y-m-d') < $date_now->format('Y-m-d') ? True : False;

                $d = $day->format("d");
                $statut = $disable_day ? 'no' : 'statut';                
                $form['table_date'][$month][$d][$statut] = array(
                    '#type' => 'checkbox',
                    '#default_value' => isset($datesExists[$month][$d]['statut']) ? True : False,
                    '#title' => $jour_fr[$day->format("N")],  
                    '#disabled' => $disable_day,
                );
                if(isset($datesExists[$month][$d]['rdid']) && $datesExists[$month][$d]['rdid'])
                {
                    $form['table_date'][$month][$d]['rdid'] = array(
                        '#type' => 'hidden',
                        '#value' => $datesExists[$month][$d]['rdid'],
                    );
                }            
            }

            $form['nid'] = array(
                '#type' => 'hidden',
                '#value' => $nid,
            );

            $form['year'] = array(
                '#type' => 'hidden',
                '#value' => $year,
              );

            $form['submit'] = [
              '#type' => 'submit',
              '#value' => 'Sauvegarder',
              '#name' => 'submit-date',
            ];

        }

      return $form;
    }

    public function redirectPage(array $form, FormStateInterface $form_state){

         $nid = $form_state->getValue(['table_filtre', '0', 'select_ressource']);
         $year = $form_state->getValue(['table_filtre', '0', 'select_year']);

         $response = new AjaxResponse();
         $url = Url::fromRoute('reservation.disponibilite.date', 
                 ['nid' => $nid, 'year' => $year], 
                 array("absolute" => TRUE))->toString();

         $response->addCommand(new RedirectCommand($url));

         return $response;
     }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {


      $form_state->setRedirect(Url::fromRoute('reservation.disponibilite.index', ['choix' => 'user']));
    }

    
    
    /**
     * {@inheritdoc}
     */
    public function publieForm(array &$form, FormStateInterface $form_state, string $month) 
    {
        $nid = $form_state->getValue('nid');
        $year = $form_state->getValue('year');
        
        $dateServices  = \Drupal::service('reservation.date');
        $dates = $dateServices->getFormDates($nid,$year.'-'.$month,'1');
        foreach($dates as $date)
        {
            $date->setPublie(True);
            $date->save();
        }        
    }

    /**
     * {@inheritdoc}
     */
    public function monthForm(array &$form, FormStateInterface $form_state) 
    {
        $nid = $form_state->getValue('nid');
        $year = $form_state->getValue('year');

        // Boucle mois
        foreach($form["table_date"]["#value"] as $month => $form_month)
        { 
            // Boucle jour
            foreach($form_month as $day => $form_day)
            { 
                if($day !== "ALL")  
                {
                    // cas d'une date déjà présente rdid non null
                    if(isset($form_day['rdid']))
                    {              
                      $entite = ReservationDate::load($form_day['rdid']);
                      $entite->setStatut($form_day['statut']);
                      $entite->save();
                    }
                    else
                    {        
                        if($form_day['statut'])
                        {
                          $date = \DateTime::createFromFormat("Y-m-d", $year."-".$month."-".$day);

                          $entite = ReservationDate::create([
                              'date' => $date->format('Y-m-d\Th:i:s'),
                              'nid' => $nid,
                              'statut' => isset($form_day["statut"]) ? '1' : '0',
                          ]);
                          $entite->save();
                        }
                    }
                }
            }
        }
    }
    
    
    
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $trigger = $form_state->getTriggeringElement();
        
        $nid = $form_state->getValue('nid');
        $year = $form_state->getValue('year');
        
        preg_match('/submit-(.*)-(.*)/', $trigger['#name'], $choix);
        
        if($choix[1] === 'publie')
        {    
            $this->publieForm($form, $form_state, $choix[2]);
        }
        else
        {
            $this->monthForm($form, $form_state);
        }
                
             

      $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.date', ['nid' => $nid, 'year' => $year]));
    }
}
