<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DemandeMultipleForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DemandeMultipleForm extends FormBase {
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'demande_multiple_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {     
        $form['#attached']['library'][] = 'reservation/reservation.demande';
        
        $reservationDemande = \Drupal::service('reservation.demande');
        $demandeFormServices = \Drupal::service('reservation.demande.form');
        $tokenServices = \Drupal::service('reservation.demande.token');
        $tokenServices->destroyTokenObsolete();
        
        $mois_fr = [
          1 => 'Janvier',
          2 => 'Février',
          3 => 'Mars',
          4 => 'Avril',
          5 => 'Mai',
          6 => 'Juin',
          7 => 'Juillet',
          8 => 'Août',
          9 => 'Septembre',
          10 => 'Octobre',
          11 => 'Novembre',
          12 => 'Décembre',
        ];
        
        $url_action = 'reservation.demande.action';
        $url_retour = 'reservation.demande.multiple';
        
        $year = \Drupal::request()->query->get('year');
        $month = \Drupal::request()->query->get('month');
        
        $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
              
        $date_now = new \Datetime();
        
        if($year == null)
        {
            $year = $date_now->format('Y');
        }

        if($month == null)
        {
            $month = $date_now->format('m');  
        }
        
        $form['table_filter'] = [
            '#type' => 'table',
            '#tableselect' => FALSE,
        ];             
   
        $period_year = [];
        $year_max = $reservationDemande->getYearMin();
        $year_min = $reservationDemande->getYearMax();
        
        foreach(range($year_min, $year_max) as $period) {
            
            if($date_now->format('Y') == $period)
            {
                $en_cours = " - En cours - ";
            }
            else
            {
                $en_cours = null;                
            }
            $period_year[$period] = $period . $en_cours;
        }
        
        $form['table_filter'][1]['year'] = [
            '#type' => 'select',
            '#options' => $period_year,
            '#default_value' => $year,
        ];   
           
        $mois = [];
        $start = new \DateTime(date("Y-01-01"));
        $interval_month = new \DateInterval("P1M");
        $periods = new \DatePeriod($start,$interval_month,11);
                
        foreach($periods as $period) {
            if($date_now->format('m') == $period->format("m"))
            {
                $en_cours = " - En cours - ";
            }
            else
            {
                $en_cours = null;                
            }
            $mois[$period->format("m")] = $mois_fr[$period->format("n")] . $en_cours;
        }
        
        $form['table_filter'][1]['month'] = [
            '#type' => 'select',
            '#options' => $mois,
            '#default_value' => $month,
        ];  
        
        $form['table_filter'][1]['filtre'] = [
            '#type' => 'submit',
            '#name' => 'filtre',
            '#value' => 'Filtrer',
        ];             
               
        $form['table_filter'][1]['reset'] = array(
            '#type' => 'submit',
            '#name' => 'reset',
            '#value' => 'Reset',
        );      
                
        $demandeActionServices  = \Drupal::service('reservation.demande.form');
        $form = $demandeActionServices->tableActionForm($form);
        
        $form['table_action'][0]['print'] = [
          $this->createButtonAction('reservation.demande.export.multiple', 'print', [$request]),
        ];
        
        $output = [];    
        $header = $demandeFormServices->getHeaderTableMultiple();
        $demandes = $reservationDemande->getDemandeMultipleByFilter($header, 15, $year, $month);  
        foreach($demandes as $demande)
        {
            $webform_submission = $demande->getWebform();
            if($webform_submission)
            {
                $route = 'entity.webform_submission.edit_form';
                $button_parameter = [
                    'webform' => 'reservation_le_hamac',
                    'webform_submission' => $webform_submission->id(),
                    'rdmid' => $demande->Id(), 
                    'url' => $url_retour, 
                    $request];
            }
            else
            {
                $route = 'reservation.demande.edit';
                $button_parameter = [
                    'rdmid' => $demande->Id(), 
                    'url' => $url_retour, 
                    $request];
            }
            $button_edition = $this->createButtonAction($route, 'pencil', $button_parameter);
            

            $output[$demande->Id()] = 
            [ 
                '#attributes' => ['class' => ['row-statut-'.$demande->getStatut()]],
                'email' => $demande->getEmail(),  
                'telephone' => $demande->getTelephone(),  
                'ressource' => $demande->getDate()->getReservationRessourceNode()->getNode()->getTitle(), 
                'demandeur' => $demande->getDemandeur(),   
                'datedemande' => $demande->getCreatedFormat(),   
                'datecreneau' => $demande->getDateCreneau(), 
                'jauge' => $demande->getJauge(),
                'statut' => $demandeFormServices->getStatutFormat($demande->getStatut()), 
                'actions' => [
                    'data' =>
                        [
                            $button_edition,
                            $this->createButtonAction($url_action, 'check-circle', ['rdmid' => $demande->Id(),
                                    'action' => 'confirme', 'url' => $url_retour, $request]),
                            $this->createButtonAction($url_action, 'hourglass-half', ['rdmid' => $demande->Id(),
                                    'action' => 'attente', 'url' => $url_retour, $request]),
                            $this->createButtonAction($url_action, 'times-circle', ['rdmid' => $demande->Id(),
                                    'action' => 'refuse', 'url' => $url_retour, $request]),
                            $this->createButtonAction($url_action, 'envelope', ['rdmid' => $demande->Id(),
                                    'action' => 'rappel', 'url' => $url_retour, $request]),
                            $this->createButtonAction($url_action, 'save', ['rdmid' => $demande->Id(),
                                    'action' => 'archive', 'url' => $url_retour, $request]),
                            $this->createButtonAction('reservation.demande.delete', 'trash-alt', ['rdmid' => [$demande->Id()],
                                    'type' => 'multiple', 'url' => $url_retour, $request]),
                        ]
                    ],
            ];
        }
        
        $form['table_demande'] = [
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $output,
            '#empty' => 'Aucunes demandes liées à cette recherche',
        ];
             
        $form['pager'] = [
           '#type' => 'pager'
        ];

        return $form;
    }
    
    /**
     * 
     * @param type $url_action
     * @param type $icon
     * @param type $parameter
     * @return type
     */
    public function createButtonAction($url_action, $icon, $parameter)
    {
        return
            [
                '#type' => 'link',
                '#title' => Markup::create('<i class="fa fa-'.$icon.' fa-2x fa-fw"></i>'),
                '#url' => Url::fromRoute($url_action, $parameter),
            ];
    }
    
    /**
     * 
     * @param type $form_state
     */
    public function filteDemandeMultiple($form_state) 
    {
        $year = $form_state->getValue(['table_filter', '1', 'year']);
        $month = $form_state->getValue(['table_filter', '1', 'month']);
        
        $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.multiple', 
            [
                'year' => $year,
                'month' => $month
            ]));
    }
    
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {        
        // Appel de la méthode commune (formulaire demande simple et multiple) pour agir sur les demandes
        $demandeFormServices  = \Drupal::service('reservation.demande.form');
        $demandeFormServices->submitActionForm($form_state, 'multiple');
  
    }
}
 