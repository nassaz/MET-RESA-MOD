<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteHoraireTableAddForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationDate;
use Drupal\reservation\Entity\ReservationHoraire;
/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireTableAddForm extends FormBase {


    protected $rdid;
    
    protected $nid;
    
    protected $month;
    
    protected $year;
    
    public function __construct($rdid, $nid, $month, $year)
    {
      $this->rdid = $rdid;
      $this->nid = $nid;
      $this->month = $month;
      $this->year = $year;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'disponibilite_horaire_table_add_form_' . $this->rdid;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['table_horaire'] = array(
            '#type' => 'table',
            '#header' => ['', 'Heure début', 'Heure fin', 'Jauge', '', 'Actions'],
            '#tableselect' => FALSE,
        );       

        $form['table_horaire'][$this->rdid]['creneau'] = array(
            '#type' => 'item',
            '#markup' => '*'
        );

        $form['table_horaire'][$this->rdid]['heure_debut'] = array(
            '#type' => 'time',
            '#required' => TRUE
        );

        $form['table_horaire'][$this->rdid]['heure_fin'] = array(
            '#type' => 'time',
            '#required' => TRUE
        );

        $form['table_horaire'][$this->rdid]['jauge_statut'] = array(
            '#type' => 'checkbox',
            '#default_value' => True,
        );
        
        $form['table_horaire'][$this->rdid]['jauge_nombre'] = array(
            '#type' => 'number',
            '#default_value' => 1,
        );

        $form['table_horaire'][$this->rdid]['actions'] = array(
            '#type' => 'submit',
            '#value' => 'Add',
        );

        return $form;
    }


    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) 
    {       
        $heure_debut = $form_state->getValue(['table_horaire', $this->rdid, 'heure_debut']);
        $heure_fin = $form_state->getValue(['table_horaire', $this->rdid, 'heure_fin']);
        if($heure_debut > $heure_fin)
        {
            $form_state->setErrorByName('table_horaire]['.$this->rdid.'][heure_debut','L\'heure de fin doit être supérieure à l\'heure de début.');
            $form_state->setErrorByName('table_horaire]['.$this->rdid.'][heure_fin');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $jauge_statut = $form_state->getValue(['table_horaire', $this->rdid, 'jauge_statut']);
        $jauge_nombre = $form_state->getValue(['table_horaire', $this->rdid, 'jauge_nombre']);
        $heure_debut = $this->formatJourHeure($form_state->getValue(['table_horaire', $this->rdid, 'heure_debut']));
        $heure_fin = $this->formatJourHeure($form_state->getValue(['table_horaire', $this->rdid, 'heure_fin']));
        
        $entite = ReservationHoraire::create([
                'rdid' => $this->rdid,
                'statut' => '1',
                'heure_debut' => $heure_debut,
                'heure_fin' => $heure_fin,
                'jauge_statut' => $jauge_statut,
                'jauge_nombre' => $jauge_nombre,
            ]
        );
        $entite->save();
    }

    private function formatJourHeure($heure) {
        
        $reservationDate = ReservationDate::load($this->rdid);
        $date = new \DateTime($reservationDate->get('date')->value);
        $date_heure = \DateTime::createFromFormat("Y-m-d H:i", $date->format('Y-m-d ') . $heure);
        
        return $date_heure->format('Y-m-d\TH:i:s');
    }


}
