<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DemandeForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DemandeDeleteForm extends FormBase {

    protected $rdmid;

    protected $type;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'demande_delete_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {          
        $this->rdmid = \Drupal::request()->query->get('rdmid');
        $this->type = \Drupal::request()->query->get('type'); 
      
        $form['validation'] = [
          '#type' => 'item',
          '#markup' => 'Etes vous sûr de vouloir supprimer ?',
        ];
        
        $form['supprimer'] = [
          '#type' => 'submit',
          '#name' => 'supprimer',
          '#value' => 'Supprimer',
        ];
                
        $form['annuler'] = [
          '#type' => 'submit',
          '#name' => 'annuler',
          '#value' => 'Annuler',
        ];
                
        return $form;
    }

    
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {        
        
        
        $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
        $trigger = $form_state->getTriggeringElement();

        if($trigger['#name'] == 'supprimer')
        {
            $demandeServices  = \Drupal::service('reservation.demande');
            $count = $demandeServices->deleteMultipleDemande($this->rdmid);

            drupal_set_message($count . ' demande(s) supprimée(s)', 'info');
            
            $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.' . $this->type));
        }
        else
        {
            $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.' . $this->type, [$request]));
        }
                 
    }
}