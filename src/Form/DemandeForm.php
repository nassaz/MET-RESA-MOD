<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DemandeForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DemandeForm extends FormBase {
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'demande_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {                   
        $form['#attached']['library'][] = 'reservation/reservation.demande';
        
        $reservationRessource = \Drupal::service('reservation.ressource');
        $reservationDemande = \Drupal::service('reservation.demande');
        $demandeFormServices = \Drupal::service('reservation.demande.form');
        $tokenServices = \Drupal::service('reservation.demande.token');
        $tokenServices->destroyTokenObsolete();
        
        $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
        $url_action = 'reservation.demande.action';
        $url_retour = 'reservation.demande.simple';
        
        $ressource = \Drupal::request()->query->get('ressource');
        $statut = \Drupal::request()->query->get('statut');
        $choix_date_demande = \Drupal::request()->query->get('choix_date_demande');
        $date_demande_debut = \Drupal::request()->query->get('date_deamnde_debut');
        $date_demande_fin = \Drupal::request()->query->get('date_demande_fin');
        $choix_date_creneau = \Drupal::request()->query->get('choix_date_creneau');
        $date_creneau_debut = \Drupal::request()->query->get('date_creneau_debut');
        $date_creneau_fin = \Drupal::request()->query->get('date_creneau_fin');
        
        if($ressource == null && \Drupal::currentUser()->hasPermission('visualisation propre demande'))
        {
            $ressourceUserServices  = \Drupal::service('reservation.ressource.user');
            $ressource = $ressourceUserServices->getRessourceUserNidByUid(\Drupal::currentUser()->id());
        }
        
        if($statut == null)
        {
            $statut = ['confirme', 'attente', 'caution'];
        }
        
        if($choix_date_demande === null)
        {
            $choix_date_demande = True;     
        }
        
        if($date_demande_debut == null)
        {
            $date_demande_debut_format = new \Datetime();
            $date_demande_debut_format->sub(new \DateInterval('P1M'));
            $date_demande_debut = $date_demande_debut_format->format('Y-m-d');
        }
        else
        {
            $date_demande_debut_format = new \Datetime($date_demande_debut);                
        }
            
        if($date_demande_fin == null)
        {
            $date_demande_fin_format = new \Datetime();
            $date_demande_fin_format->add(new \DateInterval('P1D'));
            $date_demande_fin = $date_demande_fin_format->format('Y-m-d');  
        }
        else
        {
            $date_demande_fin_format = new \Datetime($date_demande_fin);            
        }
          
        if($choix_date_creneau === null)
        {
            $choix_date_creneau = False;     
        }
         
        if($date_creneau_debut == null)
        {
            $date_creneau_debut_format = new \Datetime();
            $date_creneau_debut_format->sub(new \DateInterval('P1M'));
            $date_creneau_debut = $date_creneau_debut_format->format('Y-m-d');
        }
        else
        {
            $date_creneau_debut_format = new \Datetime($date_creneau_debut);                
        }
            
        if($date_creneau_fin == null)
        {
            $date_creneau_fin_format = new \Datetime();
            $date_creneau_fin_format->add(new \DateInterval('P1D'));
            $date_creneau_fin = $date_creneau_fin_format->format('Y-m-d');  
        }
        else
        {
            $date_creneau_fin_format = new \Datetime($date_creneau_fin);            
        }
        
        $ressourceEnable = $reservationRessource->getRessourceDemandeEnable();
                 
        $form['table_filter'] = [
            '#type' => 'table',
            '#tableselect' => FALSE,
        ];             
   
        $form['table_filter'][1]['ressource'] = [
            '#type' => 'select',
            '#multiple' => 'true',
            '#title' => 'Ressource : ',
            '#options' => $ressourceEnable,
            '#empty_value' => '',
            '#default_value' => $ressource,
        ];   

        $form['table_filter'][1]['statut'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Statut',
            '#options' => $demandeFormServices->getStatutTab(),
            '#default_value' => $statut,
          );
         
       
        $form['table_filter'][1]['table_date_demande'] = [
            '#type' => 'table',
            '#tableselect' => FALSE,
        ];            
        
        $form['table_filter'][1]['table_date_demande'][1]['choix_date_demande'] = array(
            '#type' => 'checkbox',
            '#title' => 'Date Demande',
            '#default_value' => $choix_date_demande,
        );
        
        $form['table_filter'][1]['table_date_demande'][2]['date_demande_debut'] = array(
            '#type' => 'date',
            '#title' => 'Début',
            '#value' => $date_demande_debut_format->format("Y-m-d"),
        );
        
        $form['table_filter'][1]['table_date_demande'][3]['date_demande_fin'] = array(
            '#type' => 'date',
            '#title' => 'Fin',
            '#value' => $date_demande_fin_format->format("Y-m-d"),
        );
        
        $form['table_filter'][1]['table_date_creneau'] = [
            '#type' => 'table',
            '#tableselect' => FALSE,
        ];            
        
        $form['table_filter'][1]['table_date_creneau'][1]['choix_date_creneau'] = array(
            '#type' => 'checkbox',
            '#title' => 'Date Créneau',
            '#default_value' => $choix_date_creneau,
        );
        
        $form['table_filter'][1]['table_date_creneau'][2]['date_creneau_debut'] = array(
            '#type' => 'date',
            '#title' => 'Début',
            '#value' => $date_creneau_debut_format->format("Y-m-d"),
        );
        
        $form['table_filter'][1]['table_date_creneau'][3]['date_creneau_fin'] = array(
            '#type' => 'date',
            '#title' => 'Fin',
            '#value' => $date_creneau_fin_format->format("Y-m-d"),
        );
        
        $form['table_filter'][1]['filtre'] = array(
            '#type' => 'submit',
            '#name' => 'filtre',
            '#value' => 'Filtrer',
        );             
        
        $form['table_filter'][1]['reset'] = array(
            '#type' => 'submit',
            '#name' => 'reset',
            '#value' => 'Reset',
        );        
        
        $demandeActionServices  = \Drupal::service('reservation.demande.form');
        $form = $demandeActionServices->tableActionForm($form);
                
        $form['table_action'][0]['print'] = [
          $this->createButtonAction('reservation.demande.export.simple', 'print', [$request]),
        ];        
        
        $output = [];        
        $header = $demandeFormServices->getHeaderTableSimple();  
        $demandes = $reservationDemande->getDemandeByFilter($header, 15, $ressource, $statut, $choix_date_demande, $date_demande_debut, $date_demande_fin, $choix_date_creneau, $date_creneau_debut, $date_creneau_fin);  
        foreach($demandes as $demande)
        {
            $webform_submission = $demande->getWebform();
            if($webform_submission)
            {
                $route = 'entity.webform_submission.edit_form';
                $button_parameter = [
                    'webform' => 'reservation_le_hamac',
                    'webform_submission' => $webform_submission->id(),
                    'rdmid' => $demande->Id(), 
                    'url' => $url_retour, 
                    $request];
            }
            else
            {
                $route = 'reservation.demande.edit';
                $button_parameter = [
                    'rdmid' => $demande->Id(), 
                    'url' => $url_retour, 
                    $request];
            }
            $button_edition = $this->createButtonAction($route, 'pencil', $button_parameter);
            
            $output[$demande->Id()] = 
            [
                '#attributes' => ['class' => ['row-statut-'.$demande->getStatut()]],
                'ressource' => $demande->getDate()->getReservationRessourceNode()->getNode()->getTitle(),  
                'datedemande' => $demande->getCreatedFormat(),  
                'demandeur' => $demande->getDemandeur(),  
                'datecreneau' => $demande->getDateCreneau(),  
                'jauge' => $demande->getJauge(),
                'statut' => $demandeFormServices->getStatutFormat($demande->getStatut()), 
                'actions' => [
                    'data' =>
                        [
                            $button_edition,
                            $this->createButtonAction($url_action, 'check-circle', ['rdmid' => $demande->Id(),
                                'action' => 'confirme', 'url' => $url_retour, $request]),
                            $this->createButtonAction($url_action, 'hourglass-half', ['rdmid' => $demande->Id(),
                                'action' => 'attente', 'url' => $url_retour, $request]),
                            $this->createButtonAction($url_action, 'times-circle', ['rdmid' => $demande->Id(),
                                'action' => 'refuse', 'url' => $url_retour, $request]),
                            $this->createButtonAction($url_action, 'envelope', ['rdmid' => $demande->Id(),
                                'action' => 'rappel', 'url' => $url_retour, $request]),
                            $this->createButtonAction($url_action, 'save', ['rdmid' => $demande->Id(),
                                'action' => 'archive', 'url' => $url_retour, $request]),
                            $this->createButtonAction('reservation.demande.delete', 'trash-alt', 
                                [
                                    'rdmid' => [$demande->Id()],
                                    'type' => 'simple', 
                                    'url' => $url_retour, 
                                    $request
                                ]),
                        ]
                    ],
            ];
        }
        
        $form['table_demande'] = [
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $output,
            '#empty' => 'Aucunes demandes liées à cette recherche',
        ];
        
         $form['pager'] = array(
            '#type' => 'pager'
          );
                
        return $form;
    }

    /**
     * 
     * @param type $url_action
     * @param type $icon
     * @param type $parameter
     * @return type
     */
    public function createButtonAction($url_action, $icon, $parameter)
    {
        return
            [
                '#type' => 'link',
                '#title' => Markup::create('<i class="fa fa-'.$icon.' fa-2x fa-fw"></i>'),
                '#url' => Url::fromRoute($url_action, $parameter),
            ];
    }
    
    
    
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {        
        // Appel de la méthode commune (formulaire demande simple et multiple) pour agir sur les demandes
        $demandeFormServices  = \Drupal::service('reservation.demande.form');        
        $demandeFormServices->submitActionForm($form_state, 'simple');
        
        
    }
}