<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\ParametreNotificationFilterForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationRessourceNode;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class ParametreNotificationFilterForm extends FormBase {
    
    protected $notification;
        
    protected $statutstabs;
        
    protected $type_email;
        
    protected $ressources;
    
    protected $nid;
        
    public function __construct($notification, $ressources, $statutstabs, $type_email, $nid)
    {
        $this->notification = $notification;
        $this->statutstabs = $statutstabs;
        $this->type_email = $type_email;
        $this->ressources = $ressources;
        $this->nid = $nid;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'reservation_notification_filter_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {  
        $form['select']['select_ressource'] = array(
              '#type' => 'select',
              '#title' => 'Ressource : ',
              '#options' => $this->ressources,
              '#default_value' => $this->nid,            
              '#ajax'         => [
                  'callback'  => '::redirectPage',
                  'wrapper'   => 'mode',
               ],
        );   

        $form['email_to'] = array(
            '#type' => 'textfield',
            '#title' => 'Email To :',
            '#default_value' => $this->notification->getReservationRessourceNode()->getEmailTo(),
            '#ajax'         => [
                'callback'  => [$this, 'setEmailTo'],
                'event'  => 'change',
            ],
        );

        $form['statuts'] = array(
            '#type' => 'table',
            '#title' => 'Sample Table',
        );

        foreach($this->statutstabs as $key => $row)
        {
            $form['statuts'][1][$key] = array(
                '#type' => 'checkbox',
                '#title' => $row['title_statut'],
                '#default_value' => $row['statut'],
                '#ajax'         => [
                    'callback'  => '::setStatut',
                    'wrapper'   => 'mode',
                 ],
            );
        }

        return $form;
    }
  
    public function setEmailTo(array $form, FormStateInterface $form_state)
    {
        $emailTo = $form_state->getValue('email_to');
        $ressourceNode = $this->notification->getReservationRessourceNode();
        $ressourceNode->setEmailTo($emailTo);
        $ressourceNode->save(); 

        $response = new AjaxResponse();
        return $response;
    }
    
    public function setStatut(array $form, FormStateInterface $form_state)
    {
        $notificationServices  = \Drupal::service('reservation.notification');
        $statuts = $form_state->getValue('statuts');

        foreach($statuts[1] as $key => $statut)
        {
            $node = $notificationServices->getNotificationByNidType($this->nid, $key); 
            $node->setStatut($statut);
            $node->save();
        } 
          
        $response = new AjaxResponse();
        return $response;
    }

    public function redirectPage(array $form, FormStateInterface $form_state){

        $select_ressource = $form_state->getValue('select_ressource');
                
        $response = new AjaxResponse();
        $url = Url::fromRoute('reservation.parametre.notification', 
                array('nid' => $select_ressource, 'type_email' => $this->type_email), 
                array("absolute" => TRUE))->toString();
        
        $response->addCommand(new RedirectCommand($url));
        
        return $response;
    }

    /**
    * {@inheritdoc}
    */
    public function validateForm(array &$form, FormStateInterface $form_state) {


    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }        
  
}
