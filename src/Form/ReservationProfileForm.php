<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DemandeCautionForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\ProfileForm;




class ReservationProfileForm extends ProfileForm {
    
    

    public function form(array $form, FormStateInterface $form_state)
    {
        $uid = \Drupal::routeMatch()->getParameter('user')->id();
        $ressourceUserServices  = \Drupal::service('reservation.ressource.user');
        $ressourceServices  = \Drupal::service('reservation.ressource');

        $form['reservation'] = array(
            '#type' => 'details',
            '#open' => True,
            '#weight' => 50,
            '#title' => 'Réservation',
        );

        $form['reservation']['ressource'] = array(
            '#type' => 'checkboxes',
            '#title' => 'Ressources actives',
            '#options' => $ressourceServices->getRessourceEnable(),
            '#default_value' => $ressourceUserServices->getNidByUser($uid),
        );

        return parent::form($form, $form_state);
    }

    
    public function save(array $form, FormStateInterface $form_state) {
      parent::save($form, $form_state);
        
        $uid = \Drupal::routeMatch()->getParameter('user')->id();      
        $ressourceUserServices  = \Drupal::service('reservation.ressource.user');
        foreach($form_state->getValue('ressource') as $key => $ressource)
        {
            $user = $ressourceUserServices->getUser($uid, $key);
            if($ressource && !$user)
            {
                $user = \Drupal\reservation\Entity\ReservationRessourceUser::create([
                        'user_id' => $uid,
                        'nid' => $key
                    ]);
                $user->save();
            }

            if(!$ressource && $user)
            {
                $user->delete();
            }
        }    
    }
    
}
