<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\ParametreNotificationFilterForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationRessourceNode;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class ParametreCommunForm extends FormBase {
        
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'parametre_commun';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {  
        // GET VALUES
        $reservationSettings = \Drupal::config('reservation.settings');
        
        $form['fieldset_token'] = array(
            '#type' => 'fieldset',
            '#title' => 'Token',
        );

        $tokenSettings = $reservationSettings->get('token');
        foreach($tokenSettings as $key => $value)
        {
            $form['fieldset_token'][$key] = [
                '#type' => 'number',
                '#title' => $key,
                '#default_value' => $value,
            ];
        }

        $form['fieldset_validation'] = array(
            '#type' => 'fieldset',
            '#title' => 'Messages de validation',
        );

        $messageSettings = $reservationSettings->get('message_validation');
        foreach($messageSettings as $key => $value)
        {
            $form['fieldset_validation'][$key] = [
                '#type' => 'textfield',
                '#title' => $key,
                '#default_value' => $value,
            ];
        }

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => 'Save',
        ];

        return $form;
    }
  
    
    /**
    * {@inheritdoc}
    */
    public function validateForm(array &$form, FormStateInterface $form_state) 
    {

        $time_limit = $form_state->getValue('time_limit'); 
        if($time_limit < 1)
        {
            $form_state->setErrorByName('time_limit','Le temps limite doit être supérieur à 0.');
        }
        
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {
        $time_limit = $form_state->getValue('time_limit');
        $message_temps_depasse = $form_state->getValue('message_temps_depasse');
        $message_incident = $form_state->getValue('message_incident');
        $message_annulee = $form_state->getValue('message_annulee');
        $message_confirmation_pre_reservation = $form_state->getValue('message_confirmation_pre_reservation');
        $message_confirmation_acceptee = $form_state->getValue('message_confirmation_acceptee');
        
        
        $config = \Drupal::service('config.factory')->getEditable('reservation.settings');
        $config->set('token', ['time_limit' => $time_limit]);
        $config->set('message_validation', [
            'message_temps_depasse' => $message_temps_depasse,
            'message_incident' => $message_incident,
            'message_annulee' => $message_annulee,
            'message_confirmation_pre_reservation' => $message_confirmation_pre_reservation,
            'message_confirmation_acceptee' => $message_confirmation_acceptee,
        ]);
        $config->save();
    }        
  
}
