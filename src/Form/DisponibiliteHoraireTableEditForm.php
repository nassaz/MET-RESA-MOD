<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteHoraireTableEditForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\reservation\Entity\ReservationHoraire;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;


/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireTableEditForm extends FormBase {

    protected $rdid;
    
    protected $rhid;
    
    protected $nid;
    
    protected $month;
    
    protected $year;
    
    public function __construct($rdid, $rhid, $nid, $month, $year)
    {
      $this->rdid = $rdid;
      $this->rhid = $rhid;
      $this->nid = $nid;
      $this->month = $month;
      $this->year = $year;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'disponibilite_horaire_table_Line_form_' . $this->rhid;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, ReservationHoraire $reservationHoraire = null, $creneau = 1)
    {
        $request = \Drupal::requestStack()->getCurrentRequest()->query->all();
        
        if($creneau == 1)
        {
            $header   = ['*', 'actif', 'Heure début', 'Heure fin', 'Jauge', '', 'Edition', 'Partage', 'Supprimer'];
        }
        else
        {
            $header = null;
        }
        
        $form['table_horaire'] = array(
            '#type' => 'table',
            '#tableselect' => FALSE,
            '#header' => $header,
        );           
        
        $heure_debut = new \DateTime($reservationHoraire->getHeureDebut());
        $heure_fin = new \DateTime($reservationHoraire->getHeureFin());


        $form['table_horaire'][$this->rhid]['creneau'] = array(
            '#type' => 'item',
            '#markup' => $creneau,
        );
        
        $form['table_horaire'][$this->rhid]['statut'] = array(
            '#type' => 'checkbox',
            '#default_value' => $reservationHoraire->getStatut(),
            '#size' => 2, 
        );
        
        $form['table_horaire'][$this->rhid]['heure_debut'] = array(
            '#type' => 'time',
            '#default_value' => $heure_debut->format('H:i'),
        );

        $form['table_horaire'][$this->rhid]['heure_fin'] = array(
            '#type' => 'time',
            '#default_value' => $heure_fin->format('H:i'),
        );

        $form['table_horaire'][$this->rhid]['jauge_statut'] = array(
            '#type' => 'checkbox',
            '#default_value' => $reservationHoraire->getJaugeStatut(),
            '#size' => 2, 
        );
        
        $form['table_horaire'][$this->rhid]['jauge_nombre'] = array(
            '#type' => 'number',
            '#value' => $reservationHoraire->getJaugeNombre(),
            '#disabled' => !$reservationHoraire->getJaugeStatut(),
            '#size' => 2
        );
        
        $form['table_horaire'][$this->rhid]['edit'] = [
            '#type' => 'button',
            '#name' => 'edit',
            '#value' => $this->t('edit'),
            '#ajax' => [
                'event' => 'click',
                'callback' => '::editLineAjax',
            ],
            '#suffix' => '<span class="valid-message_' . $this->rhid . '"></span>',
        ];
        
        
        $form['table_action'] = array(
            '#type' => 'table',
            '#tableselect' => FALSE,
        );          
        
        
        $form['table_horaire'][$this->rhid]['appliquer'] = [
            '#type' => 'link',
            '#title' => Markup::create('<i class="fa fa-share fa-2x fa-fw"></i>'),
            '#url' => Url::fromRoute('reservation.disponibilite.horaire.aplliquer', 
                    ['rhid' => $this->rhid, 'nid' => $this->nid, 'year' => $this->year, 'month' => $this->month]),
        ];
        
        $form['table_horaire'][$this->rhid]['supprimer'] = [
            '#type' => 'link',
            '#title' => Markup::create('<i class="fa fa-trash-alt fa-2x fa-fw"></i>'),
            '#url' => Url::fromRoute('reservation.disponibilite.horaire.supprimer', 
                    ['rhid' => $this->rhid, 'nid' => $this->nid, 'year' => $this->year, 'month' => $this->month]),
        ];
        
        
        return $form;
    }
    
    public function editLineAjax(array &$form, FormStateInterface $form_state) 
    {    
        $validate = True;
        $message = null;
        $response = new AjaxResponse();
        $css_valid = ['border' => '1px solid'];
        $css_error = ['border' => '2px solid red'];
        
        $heure_debut = $form_state->getValue(['table_horaire', $this->rhid, 'heure_debut']);
        $heure_fin = $form_state->getValue(['table_horaire', $this->rhid, 'heure_fin']);
        
        if ($heure_debut >= $heure_fin) {
            $message .= $this->t('<p>L\'heure de fin doit être supérieure à l\'heure de début.</p>');
            $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-heure-debut', $css_error));
            $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-heure-fin', $css_error));
            $validate = False;
        }
        
        $jauge_nombre = $form_state->getValue(['table_horaire', $this->rhid, 'jauge_nombre']);
        if ($jauge_nombre <= 0) {
            $message .= $this->t('<p>La jauge doit être supérieure à zéro.</p>');
            $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-jauge-nombre', $css_error));
            $validate = False;
        }
        
        if($validate)
        {
            $this->setValue($form_state, 'statut');
            $this->setHeure($form_state, 'heure_debut');
            $this->setHeure($form_state, 'heure_fin');
            $this->setValue($form_state, 'jauge_statut');
            $this->setValue($form_state, 'jauge_nombre');
            
            $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-heure-debut', $css_valid));
            $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-heure-fin', $css_valid));
            $response->addCommand(new CssCommand('#edit-table-horaire-' . $this->rhid . '-jauge-nombre', $css_valid));
            $message = $this->t('<p>Edition OK.</p>');
        }        
      
        $response->addCommand(new HtmlCommand('.valid-message_' . $this->rhid, $message));
        return $response;
    }
                
    public function setValue($form_state, $field)
    {
        $value = $form_state->getValue(['table_horaire', $this->rhid, $field]);
      
        $reservationHoraire = ReservationHoraire::load($this->rhid);
        $reservationHoraire->set($field, $value);
        $reservationHoraire->save();
    }
    
    public function setHeure($form_state, $field) {
        
        $reservationHoraire = ReservationHoraire::load($this->rhid);
        $date = new \DateTime($reservationHoraire->get($field)->value);
        $heure = new \DateTime($form_state->getValue(['table_horaire', $this->rhid, $field]));
        $value = \DateTime::createFromFormat("Y-m-d H:i", $date->format('Y-m-d ') . $heure->format('H:i'));
        $reservationHoraire->set($field, $value->format('Y-m-d\TH:i:s'));
        $reservationHoraire->save();
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }
}
