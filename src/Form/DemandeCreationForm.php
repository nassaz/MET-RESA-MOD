<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DemandeCreationForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Url;


/**
 * Class DemandeCreationForm.
 *
 * @ingroup reservation
 */
class DemandeCreationForm extends FormBase {
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'demande_creation_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $rdmid = null)
    {
        $demandeServices = \Drupal::service('reservation.demande');
        $reservationRessource = \Drupal::service('reservation.ressource');
        $demandeFormServices = \Drupal::service('reservation.demande.form');
        
        if($rdmid)
        {
            $demande = $demandeServices->load($rdmid);            
        }
        else
        {
            $demande = null;
        }
        
        $ressource = \Drupal::request()->query->get('ressource');            
      
        $form['#attached']['library'][] = 'reservation/reservation.calendar';

        if($demande)
        {
           
            $form['ressource'] = [
                '#type' => 'hidden',
                '#attributes' => array('id' => 'webform-reservation-ressource-id'),
                '#value' => $demande->getDate()->getReservationRessourceNode()->getNode()->id(),
            ];
         
        }
        else
        {
            $demande = \Drupal\reservation\Entity\ReservationDemande::create();
            $form['ressource'] = array(
                '#type' => 'select',
                '#id' => 'webform-reservation-ressource-id',
                '#title' => 'Ressource : ',
                '#options' => $reservationRessource->getRessourceEnable(),
                '#default_value' => $ressource,
                '#required' => true,
                '#ajax'         => [
                    'callback'  => [$this, 'redirectPage'],
                    'event'  => 'change',
                ],
            );
        }
        
        $form['calendar'] = [
            '#type' => 'hidden',
            '#attributes' => array('id' => 'reservation-calendar'),
            '#prefix' => '<div id="reservation-calendar-datepicker">',
            '#suffix' => '</div>',
        ];
        
        $form['horaire'] = array(
            '#type' => 'select',
            '#id' => 'reservation-horaire-select',
            '#title' => 'Horaire : ',
            '#options' => null,
            '#prefix' => '<div id="reservation-horaire--wrapper">',
            '#suffix' => '</div>',
            //'#attributes' => array('hidden' => True),
        );

        $form['demandeur'] = [
            '#type' => 'textfield',
            '#title' => 'Demandeur :',
            '#required' => true,
            '#default_value' => $demande->getDemandeur(),
        ];
             
        $form['email'] = [
            '#type' => 'textfield',
            '#title' => 'Email :',
            '#required' => true,
            '#default_value' => $demande->getEmail(),
        ];

        $form['telephone'] = [
            '#type' => 'textfield',
            '#title' => 'Téléphone :',
            '#default_value' => $demande->getTelephone(),
        ];

        $form['jauge'] = [
            '#type' => 'number',
            '#title' => 'Jauge :',
            '#required' => true,
            '#default_value' => 1,
            '#default_value' => $demande->getJauge(),          
        ];
        
        $form['statut'] = array(
            '#type' => 'select',
            '#title' => 'Statut',
            '#options' => $demandeFormServices->getStatutTab(),
            '#default_value' => $demande->getStatut(),  
            '#required' => true
          );
        
        $form['submit'] = [
          '#type' => 'submit',
          '#value' => 'Sauvegarder',
        ];
        
        $form['rdmid'] = [
          '#type' => 'hidden',
          '#value' => $rdmid,
        ];



        return $form;
    }

    public function redirectPage(array $form, FormStateInterface $form_state)
    {

         $ressource = $form_state->getValue('ressource');
   
         $response = new AjaxResponse();
         $url = Url::fromRoute('reservation.demande.create', 
                 ['ressource' => $ressource], 
                 array("absolute" => TRUE))->toString();

         $response->addCommand(new RedirectCommand($url));

         return $response;
     }
     
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {


    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {        
        $demandeServices = \Drupal::service('reservation.demande');
        $demandeFormServices = \Drupal::service('reservation.demande.form');
        
        $rdmid = $form_state->getValue('rdmid');
        $rdid = $form_state->getValue('calendar');
        $rhid = $form_state->getValue('horaire');
        $sid = null;
        $statut = $form_state->getValue('statut');
        $demandeur = $form_state->getValue('demandeur');
        $jauge = $form_state->getValue('jauge');
        $email = $form_state->getValue('email');
        $telephone = $form_state->getValue('telephone');
        
        if($rdmid)
        {
            $demandeServices->editDemande($rdmid, $rdid, $rhid, $sid, 
                            $statut, $demandeur, $jauge, $email, $telephone);            
        }
        else
        {
            $demande = $demandeServices->createDemande($rdid, $rhid, $sid, 
                            $statut, $demandeur, $jauge, $email, $telephone); 
            
            $mailServices  = \Drupal::service('reservation.mail');
            $mailServices->generateEmailById($demande->id(), $statut);
        }
 
        $form_state->setRedirectUrl(Url::fromRoute('reservation.demande.simple'));  
    }  
}
