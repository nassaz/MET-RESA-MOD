<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteUserForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteUserForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'disponibilite_user_form';
  }

  /**
   * {@inheritdoc}
   */
    public function buildForm(array $form, FormStateInterface $form_state, $choix = null) {

        $options = array(
            'user' => 'Mes ressources',
            'all' => 'Toutes les ressources', 
        );

        $form['choix'] = array(
            '#type' => 'radios',
            '#options' => $options,
            '#default_value' => $choix,
            '#ajax'         => [
                'callback'  => '::redirectPage',
                'wrapper'   => 'mode',
            ],
        );

        return $form;
    }

    public function redirectPage(array $form, FormStateInterface $form_state){

        $choix = $form_state->getValue('choix');

        $response = new AjaxResponse();
        $url = Url::fromRoute('reservation.disponibilite.index', 
                array('choix' => $choix), 
                array("absolute" => TRUE))->toString();
        $response->addCommand(new RedirectCommand($url));

        return $response;
    }


    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }

}
