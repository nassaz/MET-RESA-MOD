<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteHoraireTableDeleteForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationHoraire;
use Drupal\Core\Url;


/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireTableDeleteForm extends FormBase {

    public $nid = null;
    
    public $rhid = null;
    
    public $month = null;
    
    public $year = null;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'disponibilite_horaire_table_apply_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $nid = null, $rhid = null, $year = null, $month = null)
    {
        $this->nid = $nid;
        $this->rhid = $rhid;
        $this->month = $month;
        $this->year = $year;
        
        $reservationHoraire = ReservationHoraire::load($rhid);
   
        $label = 'Supprimer l\'horaire ' . $reservationHoraire->getHeureDebutFormat('H:i');
        $label .= ' - ' . $reservationHoraire->getHeureFinFormat('H:i');
        $label .= ' du ' . $reservationHoraire->getHeureDebutFormat('d/m/Y') . ' ?';
        
        
        $form['affichage'] = array(
            '#type' => 'item',
            '#markup' => $label
        );
             
        $form['supprimer'] = [
            '#type' => 'submit',
            '#name' => 'supprimer',            
            '#value' => 'Oui',
        ];
        
        $form['annuler'] = [
            '#type' => 'submit',
            '#name' => 'annuler',  
            '#value' => 'Non',
        ];
        
        
        return $form;
    }

    
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        $trigger = $form_state->getTriggeringElement();

        if($trigger['#name'] == 'supprimer')
        {
            $reservationHoraire = ReservationHoraire::load($this->rhid);
            $reservationHoraire->delete();
        }
        
        $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.horaire'),
                ['nid' => $this->nid, 'month' => $this->month, 'year' => $this->year]);   
        
    }

}
