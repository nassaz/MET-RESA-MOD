<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteModeForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteModeForm extends FormBase {


    protected $nid;

    public function __construct($nid)
    {
      $this->nid = $nid;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'disponibilite_mode_form_' . $this->nid;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, ReservationRessourceNode $ressourceNode = null) {

        $options = array(
            0 => 'Pré-Réservé',
            1 => 'Automatique', 
        );

        $form['mode'] = array(
            '#type' => 'radios',
            '#options' => $options,
            '#default_value' => $ressourceNode->getAutomatique(),
            '#ajax'         => [
              'callback'  => '::setAutomatique',
              'wrapper'   => 'mode',
            ],
        );

        $form['node'] = array(
          '#type' => 'hidden',
          '#value' => $ressourceNode->id(),
        );

        return $form;
    }

    public function setAutomatique(array $form, FormStateInterface $form_state){

        $form_state->setRedirectUrl(Url::fromRoute(\Drupal::routeMatch()->getRouteName()));
        $node = $form_state->getValue('node');
        $mode = $form_state->getValue('mode');
        

        $ressourcenode = ReservationRessourceNode::load($node);
        $ressourcenode->setAutomatique($mode);
        $ressourcenode->save();
        
        $response = new AjaxResponse();
        $response->addCommand(new CloseModalDialogCommand());
        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }

}
