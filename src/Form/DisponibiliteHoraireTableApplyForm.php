<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteHoraireTableApplyForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\reservation\Entity\ReservationDate;
use Drupal\reservation\Entity\ReservationHoraire;
use Drupal\Core\Url;


/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireTableApplyForm extends FormBase {

    public $nid = null;
    
    public $rhid = null;
    
    public $month = null;
    
    public $year = null;
    
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'disponibilite_horaire_table_apply_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $nid = null, $rhid = null, $year = null, $month = null)
    {
        $reservationDate = \Drupal::service('reservation.date');
        
        $this->nid = $nid;
        $this->rhid = $rhid;
        $this->month = $month;
        $this->year = $year;
                
        $mode = \Drupal::request()->query->get('mode');
        
        $reservationHoraire = ReservationHoraire::load($rhid);
   
        $label = 'Horaire à appliquer ' . $reservationHoraire->getHeureDebutFormat('H:i') . ' - ' . $reservationHoraire->getHeureFinFormat('H:i');
        
        $form['affichage'] = array(
            '#type' => 'item',
            '#markup' => $label
        );
                
        if($mode == null)
        {
            $mode = 'date';            
        }
        
        $options = array(
            'date' => 'Par Date',
            'mois' => 'Par Mois', 
        );
                
        $form['mode'] = array(
            '#type' => 'radios',
            '#title' => 'Mode : ',
            '#options' => $options,
            '#default_value' => $mode,
            '#ajax'         => [
                'callback'  => '::choixMode',
                'wrapper'   => 'mode',
            ],
        );
        
        $month_old = null;
        $dates = [];
        $months = [];
        
        
        $datePublies = $reservationDate->getDatePublie($nid, '2018', False, True);
        foreach($datePublies as $datePublie)
        {
            $dates[$datePublie->Id()] = $datePublie->getDateFormat('d/m/Y');
            if($datePublie->getDateFormat('m') != $month_old)
            {
                $months[$datePublie->getDateFormat('Y-m')] = $datePublie->getDateFormat('F');
                $month_old = $datePublie->getDateFormat('m');
            }            
        }
        
        if($mode == 'mois')
        {
            $form['month'] = array(
                '#type' => 'checkboxes',
                '#title' => 'Mois',    
                '#options' => $months,
            );      
        }
        else
        {
            $form['date'] = array(
                '#type' => 'checkboxes',
                '#title' => 'Dates',
                '#options' => $dates,
            );
        }
        
        $form['Valider'] = [
            '#type' => 'submit',
            '#name' => 'valider',            
            '#value' => 'Valider',
        ];
        
        $form['retour'] = [
            '#type' => 'submit',
            '#name' => 'retour',  
            '#value' => 'Retour',
        ];
        
        
        return $form;
    }

    
    
    
    public function choixMode(array $form, FormStateInterface $form_state){
        
        $mode = $form_state->getValue('mode');
        
        $response = new AjaxResponse();
        $url = Url::fromRoute('reservation.disponibilite.horaire.aplliquer', 
                ['mode' => $mode, 'nid' => $this->nid, 'rhid' => $this->rhid, 'year' => $this->year, 'month' => $this->month], 
                array("absolute" => TRUE))->toString();
        
        $response->addCommand(new RedirectCommand($url));

        return $response;
    }
    

    /**
     * {@inheritdoc}
     */
    public function setHoraireDate($form_state) 
    {
        $values = $form_state->getValues();        
        foreach($values['date'] as $key => $date)
        {
            if($date)
            {
                $reservationsDate = ReservationDate::load($key);
                $this->setRessourceHoraire($reservationsDate);
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function setHoraireMonth($form_state) 
    {
        $dateServices = \Drupal::service('reservation.date');
        $values = $form_state->getValues();        
        foreach($values['month'] as $month)
        {
            if($month)
            {
                $reservationsDates = $dateServices->getDatePublie($this->nid, $month, False, True);
                foreach($reservationsDates as  $reservationsDate)
                {
                    $this->setRessourceHoraire($reservationsDate);
                }                
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function setRessourceHoraire(ReservationDate $reservationsDate)
    {        
        $horaireServices  = \Drupal::service('reservation.horaire');

        $reservationsDate->setHoraire(True);
        $reservationsDate->save();
        
        $reservationsHoraire = ReservationHoraire::load($this->rhid);
        
        $exist = $horaireServices->notExisteHoraire($reservationsDate->Id(), $reservationsHoraire->getHeureDebut(), $reservationsHoraire->getHeureFin());
        
        if($exist)
        {
            $entite = ReservationHoraire::create([
                    'rdid' => $reservationsDate->Id(),
                    'statut' => $reservationsHoraire->getStatut(),
                    'heure_debut' => $reservationsHoraire->getHeureDebut(),
                    'heure_fin' => $reservationsHoraire->getHeureFin(),
                    'jauge_statut' => $reservationsHoraire->getJaugeStatut(),
                    'jauge_nombre' => $reservationsHoraire->getJaugeNombre(),
                ]
            );
            $entite->save();            
        }        
    }
    
    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) 
    {
        
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {
        $trigger = $form_state->getTriggeringElement();
        if($trigger['#name'] == 'valider')
        {
            $mode = $form_state->getValue('mode');
            if($mode == 'date')
            {
                $this->setHoraireDate($form_state);
            }
            else
            {
                $this->setHoraireMonth($form_state);
            }
        }
        
        $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.horaire',
                ['nid' => $this->nid, 'month' => $this->month, 'year' => $this->year]));
    }

}
