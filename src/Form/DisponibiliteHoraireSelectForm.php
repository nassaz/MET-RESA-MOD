<?php
/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteHoraireSelectForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireSelectForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'disponibilite_horaire_select_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $ressources = null, $nid = null, $month = null, $year = null)
    {     

        $dateServices = \Drupal::service('reservation.date');

        $form['table_filter'] = [
            '#type' => 'table',
            '#tableselect' => FALSE,            
        ];   

        $form['table_filter'][0]['select_ressource'] = [
            '#type' => 'select',
            '#title' => 'Ressource : ',
            '#options' => $ressources,
            '#default_value' => $nid,
            '#ajax'         => [
                'callback'  => '::redirectPage',
                'wrapper'   => 'mode',
            ],
        ];   

        $range_year = range(date('Y'), $dateServices->getYearMax());
        $form['table_filter'][0]['select_year'] = [
            '#type' => 'select',
            '#title' => 'Année : ',
            '#options' => array_combine($range_year, $range_year),
            '#default_value' => $year,
            '#ajax'         => [
                'callback'  => '::redirectPage',
                'wrapper'   => 'mode',
            ],
        ];   


        $form['select_month'] = [
          '#type' => 'hidden',
          '#value' => $month,
        ];



        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    public function redirectPage(array $form, FormStateInterface $form_state)
    {
        $nid = $form_state->getValue(['table_filter', '0', 'select_ressource']);
        $year = $form_state->getValue(['table_filter', '0', 'select_year']);
        $month = $form_state->getValue('select_month');

        $response = new AjaxResponse();
        $url = Url::fromRoute('reservation.disponibilite.horaire', ['nid' => $nid, 'month' => $month, 'year' => $year], 
                array("absolute" => TRUE))->toString();

        $response->addCommand(new RedirectCommand($url));

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {
        $nid = $form_state->getValue(['table_filter', '0', 'select_ressource']);
        $year = $form_state->getValue(['table_filter', '0', 'select_year']);
        $month = $form_state->getValue('select_month');

        $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.horaire', ['nid' => $nid, 'month' => $month, 'year' => $year]));  
    }

}
