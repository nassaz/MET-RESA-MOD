<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DemandeForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationRessourceUser;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class ParametreDroitForm extends FormBase {
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'parametre_droit_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {                   
        $ressourceSettings = \Drupal::service('reservation.ressource');
        $ressourceUserServices  = \Drupal::service('reservation.ressource.user');        
                 
        $ressource = \Drupal::request()->query->get('ressource');
        $ressourceEnable = $ressourceSettings->getRessourceEnable();
        if($ressource == null)
        {
            $ressource = current(array_keys($ressourceEnable));
        }
        
        $role = \Drupal::request()->query->get('role');
        $roleEnables = $ressourceUserServices->getRoles();
        if($role == null)
        {
            $role = current($roleEnables);
        }
        
        $userEnable = $ressourceUserServices->getUsers($role, $ressource);
        $users = $ressourceUserServices->getRessourceUserByNid($ressource);
        
        $form['ressource'] = [
            '#type' => 'select',
            '#title' => 'Ressource : ',
            '#options' => $ressourceEnable,
            '#default_value' => $ressource,
            '#ajax'         => [
                'callback'  => '::redirectPage',
                'wrapper'   => 'mode',
             ],
        ];   

        $form['table'] = [
            '#type' => 'table',
            '#tableselect' => FALSE,
        ];         
        
        $form['table'][1]['role'] = [
            '#type' => 'select',
            '#title' => 'Roles : ',
            '#options' => $roleEnables,
            '#default_value' => $role,
        ];   


        $form['table'][1]['filtre'] = [
            '#type' => 'submit',
            '#value' => 'Filtrer',
        ];   

        
        $form['table'][1]['user'] = [
            '#type' => 'select',
            '#title' => 'Users : ',
            '#options' => $userEnable,
        ];   
        
        $form['table'][1]['ajouter'] = [
            '#name' => 'ajouter',
            '#type' => 'submit',
            '#value' => 'Ajouter',
        ];   

        
        $output = [];   
        
        $header = [
            'name'      => [
                'data'      => 'Nom',
                'field'     => 'name',
                'specifier' => 'name',
            ],
        ];
        
        foreach($users as $user)
        {
            $output[$user->Id()] = 
            [
                'name' => $user->getUser()->get('name')->value,
            ];      
        }

        $form['table_user'] = [
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $output,
            '#empty' => 'Aucuns utilisateurs lié à cette ressource',
        ];
        
        $form['pager'] = [
           '#type' => 'pager'
        ]; 

        $form['retirer'] = [
            '#name' => 'retirer',
            '#type' => 'submit',
            '#value' => 'Retirer',
        ];   
        
        return $form;
    }

    
    public function redirectPage(array $form, FormStateInterface $form_state){

        $ressource = $form_state->getValue('ressource');
                
        $response = new AjaxResponse();
        $url = Url::fromRoute('reservation.parametre.droit', 
                array('ressource' => $ressource), 
                array("absolute" => TRUE))->toString();
        
        $response->addCommand(new RedirectCommand($url));
        
        return $response;
    }
    
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {        
        $ressource = $form_state->getValue('ressource');
        $role = $form_state->getValue(['table', '1', 'role']);
        $user = $form_state->getValue(['table', '1', 'user']);
        
        $trigger = $form_state->getTriggeringElement();
        if($trigger['#name'] == 'ajouter')
        {
            $ressourceUser = ReservationRessourceUser::create([
                'user_id' => $user,
                'nid' => $ressource
            ]);
            $ressourceUser->save();
        }
        
        if($trigger['#name'] == 'retirer')
        {
            $users = $form_state->getValue('table_user');
            foreach($users as $user)
            {
                if($user)
                {
                    $ressourceUser = ReservationRessourceUser::load($user);
                    $ressourceUser->delete();    
                }             
            }
        }
        
        $form_state->setRedirectUrl(Url::fromRoute('reservation.parametre.droit', 
        [
            'ressource' => $ressource, 
            'role' => $role,
            'user' => $user
        ]));   
        
    }
}