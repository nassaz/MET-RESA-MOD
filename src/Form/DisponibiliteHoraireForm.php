<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteHoraireForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\reservation\Entity\ReservationDate;


/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteHoraireForm extends FormBase {
    
    protected $rdid;
    
    protected $nid;
    
    protected $month;
    
    protected $year;
    
    public function __construct($rdid, $nid, $month, $year)
    {
      $this->rdid = $rdid;
      $this->nid = $nid;
      $this->month = $month;
      $this->year = $year;
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'disponibilite_horaire_form_' . $this->rdid;
    }

    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, ReservationDate $reservationDate = null)
    {
        $horaire = $reservationDate->getHoraire();
        
        $options = [
            '0' => 'Toute la journée',
            '1' => 'Créneaux horaires', 
        ];

        $form['table_date'] = array(
            '#type' => 'table',
            '#tableselect' => FALSE,
        );       

        $form['table_date'][$this->rdid]['horaire'] = array(
            '#type' => 'radios',
            '#title' => 'Mode de réservation',
            '#options' => $options,
            '#default_value' => $horaire,
            '#ajax' => [
                'callback' => [$this, 'setHoraire'],
                'event' => 'change',
                'wrapper' => 'edit-output-'.$this->rdid,
              ],
          );
        
        if($horaire === '0')
        {
            $form['table_date'][$this->rdid]['jauge_statut'] = array(
                '#type' => 'checkbox',
                '#title' => 'Jauge',
                '#size' => 2, 
                '#default_value' => $reservationDate->getJaugeStatut(),
                '#ajax'         => [
                    'event' => 'change',
                    'callback'  => '::setJaugeStatut',
                    'wrapper'   => 'jauge',
                ],
            ); 
            
            $form['table_date'][$this->rdid]['jauge_nombre'] = array(
                '#type' => 'number',
                '#size' => 2, 
                '#value' => $reservationDate->getJaugeNombre(),
                '#disabled' => !$reservationDate->getJaugeStatut(),
                '#ajax'         => [
                    'event' => 'change',
                    'callback'  => '::setJaugeNombre',
                    'wrapper'   => 'jauge',
                ],
            ); 
        }

        $form['table_date'][$this->rdid]['rappel'] = array(
          '#type' => 'checkbox',
          '#title' => 'Rappel',
          '#default_value' => $reservationDate->getRappel(),
          '#ajax'         => [
              'event' => 'change',
              'callback'  => '::setRappel',
              'wrapper'   => 'jauge',
          ],
        );

        $form['table_date'][$this->rdid]['rappel_jour'] = array(
            '#type' => 'number',
            '#title' => 'J-',
            '#size' => 2, 
            '#value' => $reservationDate->getRappelJour(),
            '#disabled' => !$reservationDate->getRappel(),
              '#ajax'         => [
                  'event' => 'change',
                  'callback'  => '::setRappelJour',
                  'wrapper'   => 'jauge',
              ],
        );

        $form['table_date'][$this->rdid]['enquete'] = array(
            '#type' => 'checkbox',
            '#title' => 'Enquête',
            '#default_value' => $reservationDate->getEnquete(),
              '#ajax'         => [
                  'event' => 'change',
                  'callback'  => '::setEnquete',
                  'wrapper'   => 'jauge',
              ],
        );

        $form['table_date'][$this->rdid]['enquete_jour'] = array(
            '#type' => 'number',
            '#title' => 'J+',
            '#size' => 2, 
            '#value' => $reservationDate->getEnqueteJour(),
            '#disabled' => !$reservationDate->getEnquete(),
              '#ajax'         => [
                  'event' => 'change',
                  'callback'  => '::setEnqueteJour',
                  'wrapper'   => 'jauge',
              ],
        );
        
        $form['table_date'][$this->rdid]['publie'] = array(
            '#type' => 'checkbox',
            '#title' => 'Publier',
            '#default_value' => $reservationDate->getPublie(),
              '#ajax'         => [
                  'event' => 'change',
                  'callback'  => '::setPublie',
                  'wrapper'   => 'jauge',
              ],
        );
        
        return $form;
    }

    public function setHoraire(array $form, FormStateInterface $form_state)
    {
        $horaire = $form_state->getValue(['table_date', $this->rdid, 'horaire']);
        
        $reservationDate = ReservationDate::load($this->rdid);
        $reservationDate->setHoraire($horaire);
        $reservationDate->save();
                
        $response = new AjaxResponse();
        $url = Url::fromRoute('reservation.disponibilite.horaire', ['nid' => $this->nid, 'month' => $this->month, 'year' => $this->year], array("absolute" => TRUE))->toString();
        $response->addCommand(new RedirectCommand($url));
        return $response;
    }

    public function setJaugeStatut(array $form, FormStateInterface $form_state)
    {
        return $this->setStatutNombre($form_state, 'jauge_statut', 'jauge-nombre');
    }
    
    public function setJaugeNombre(array $form, FormStateInterface $form_state)
    {
        return $this->setValue($form_state, 'jauge_nombre');
    }

    public function setRappel(array $form, FormStateInterface $form_state)
    {
        return $this->setStatutNombre($form_state, 'rappel', 'rappel-jour');
    }
    
    public function setRappelJour(array $form, FormStateInterface $form_state)
    {
        return $this->setValue($form_state, 'rappel_jour');
    }
    
    public function setEnquete(array $form, FormStateInterface $form_state)
    {
        return $this->setStatutNombre($form_state, 'enquete', 'enquete-jour');
    }
    
    public function setEnqueteJour(array $form, FormStateInterface $form_state)
    {
        return $this->setValue($form_state, 'enquete_jour');
    }
    
    public function setPublie(array $form, FormStateInterface $form_state)
    {
        return $this->setValue($form_state, 'publie');
    }
    
    public function setStatutNombre($form_state, $field_statut, $field_nombre)
    {
        $value = $form_state->getValue(['table_date', $this->rdid, $field_statut]);
      
        $reservationDate = ReservationDate::load($this->rdid);
        $reservationDate->set($field_statut, $value);
        $reservationDate->save();
           
        $response = new AjaxResponse();
        $id_field = '#edit-table-date-'.$this->rdid.'-'.$field_nombre;
        
        if($value)
        {
            $response->addCommand(new InvokeCommand($id_field, 'css', array('background-color', '#ffffff')));
            $response->addCommand(new InvokeCommand($id_field, 'attr', array('disabled',false)));
        }        
        else
        {
            $response->addCommand(new InvokeCommand($id_field, 'css', array('background-color', '#e5e5e5')));
            $response->addCommand(new InvokeCommand($id_field, 'attr', array('disabled',true)));
        }
        
        return $response;
    }  
    
    
    public function setValue($form_state, $field)
    {
        $value = $form_state->getValue(['table_date', $this->rdid, $field]);
      
        $reservationDate = ReservationDate::load($this->rdid);
        $reservationDate->set($field, $value);
        $reservationDate->save();
           
        $response = new AjaxResponse();
        
        return $response;
    }    
    

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

      $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.date', ['nid' => $this->nid, 'year' => $this->year]));
    }
}
 