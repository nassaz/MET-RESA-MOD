<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteDateDeleteForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationDate;
/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteDateDeleteForm extends FormBase {

  /**
   * 
   * @return string
   */
  public function getFormId() {
    return 'disponibilite_date_delete_form';
  }

/**
 * 
 * @param array $form
 * @param FormStateInterface $form_state
 * @param type $ressourceDates
 * @param type $parameter
 * @return string
 */
  public function buildForm(array $form, FormStateInterface $form_state, $ressourceDates = null, $parameter = [])
  {
    $start = \DateTime::createFromFormat("Y-m-d","2018-01-01");
    $interval_month = new \DateInterval("P1M");
    $period_month = new \DatePeriod($start,$interval_month,11);
        
    $hearders = array("Tout", "mois");
     
    $form['table_date'] = array(
        '#type' => 'table',
        '#header' => array_merge($hearders, range(1,31)),
        '#tableselect' => FALSE,
    );    
        
    foreach($period_month as $month) {
        $month_word[] = $month->format("M");
        $month_num[] = $month->format("m");
    }
        
    foreach ($month_num as $key => $month) {
        
        $start = \DateTime::createFromFormat("Ymd",$parameter["year"].$month.'01');
        $interval_day = new \DateInterval("P1D");
        
        $period_day = new \DatePeriod($start,$interval_day,$start->format("t")-1);
        

        $form['table_date'][$month]['ALL'] = array(
            '#type' => 'checkbox',
            '#default_value' => False
        );
        
        $form['table_date'][$month]['MONTH'] = array(
            '#type' => 'label',
            '#title' => $month_word[$key]
        );
        foreach($period_day as $day){
            
            $d = $day->format("d");
            $d_word = $day->format("D");
            
            $form['table_date'][$month][$d]['statut'] = array(
                '#type' => 'checkbox',
                '#default_value' => isset($ressourceDates[$month][$d]['statut']) ? True : False,
                '#title' => $d_word[0].$d_word[1],  
            );
            if(isset($ressourceDates[$month][$d]['rdid']) && $ressourceDates[$month][$d]['rdid'])
            {
                $form['table_date'][$month][$d]['rdid'] = array(
                    '#type' => 'hidden',
                    '#value' => $ressourceDates[$month][$d]['rdid'],
                );
            }            
        }
        
        $form['nid'] = array(
            '#type' => 'hidden',
            '#value' => $parameter["nid"],
        );

        $form['select_year'] = array(
            '#type' => 'hidden',
            '#value' => $parameter["year"],
          );

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => 'Change',
          '#name' => 'submit-date',
        ];

    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
      
      
    $form_state->setRedirect(Url::fromRoute('reservation.disponibilite.index', ['choix' => 'user']));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $nid = $form_state->getValue('nid');
    $year  =$form_state->getValue('select_year');
       
    // Boucle mois
    foreach($form["table_date"]["#value"] as $month => $form_month)
    { 
        // Boucle jour
        foreach($form_month as $day => $form_day)
        { 
            if($day !== "ALL")  
            {
              // cas d'une date déjà présente rdid non null
              if(isset($form_day['rdid']))
              {              
                $entite = ReservationDate::load($form_day['rdid']);
                $entite->statut->value = isset($form_day['statut']) ? True : False;
                $entite->save();
              }
              else
              {        
                  if($form_day['statut'])
                  {
                    $entite = ReservationDate::create([
                        'year' => $year,
                        'month' => $month,
                        'day' => $day,
                        'nid' => $nid,
                        'statut' => isset($form_day["statut"]) ? True : False,
                    ]);
                    $entite->save();
                  }
              }
            }
        }
    }
    
    $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.date', ['nid' => $nid, 'year' => $year]));

  }

}
