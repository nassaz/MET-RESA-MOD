<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DemandeCautionForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;


class DemandeCautionForm extends FormBase {
    
    /**
     *
     * @var type 
     */
    protected $token;
        
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'demande_caution_form';
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $token = null, $demande = null) {

        $this->token = $token;
        
        $date = new \DateTime();
        
        $form['message'] = [
          '#type' => 'item',
          '#markup' => '<p>Pour valider votre réservation, il vous faut déposer une caution.</p>'
            . '<p>Montant de : ' . $demande->getDate()->getReservationRessourceNode()->getCautionMontant() . ' €</p>'
            . '<p>Cliquez sur <i>Valider</i> pour continuer</p>',
        ];
        
        $fields = [
            'ORDERID'=> $demande->id(),
            'AMOUNT' => $demande->getDate()->getReservationRessourceNode()->getCautionMontant() * 100,
            'CN' => $demande->getDemandeur(),
            'EMAIL' => $demande->getEmail(),
            'ACCEPTURL' => $this->urlReturn('reservation.demande.caution.exec', 'accept', $this->token),
            'DECLINEURL' => $this->urlReturn('reservation.demande.caution.exec', 'decline', $this->token),
            'EXCEPTIONURL' => $this->urlReturn('reservation.demande.caution.exec', 'except', $this->token),
            'CANCELURL' => $this->urlReturn('reservation.demande.caution.exec', 'cancel', $this->token),
        ];
        $form = $this->parametreForm($form, $fields);
             
        $reservationSettings = \Drupal::config('reservation.settings');
        $rows = $reservationSettings->get('caution');
        $form = $this->parametreForm($form, $rows['input']);

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => 'Valider',
        ];

        $form['#action'] = $rows['url']['send'];

        return $form;
    }

    public function hiddenForm($form, $key, $row)
    {   
        $form[$key] = [
            '#type' => 'hidden',
            '#value' => $row,
        ];
        
        return $form;
    }
    
    public function parametreForm($form, $rows)
    {   
        foreach($rows as $key => $row)
        {
            $form = $this->hiddenForm($form, $key, $row);
        }
        
        return $form;
    }
    
    public function urlReturn($route, $etat, $token)
    {   
        $host = \Drupal::request()->getHost();
        return $host . Url::fromRoute($route, ['etat' => $etat, 'token' => $token])->toString();
    }
    
    /**
     * 
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }

}
