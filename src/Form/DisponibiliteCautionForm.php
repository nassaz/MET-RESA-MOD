<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteCautionForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;

/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class DisponibiliteCautionForm extends FormBase {
    
    protected $nid;
        
    public function __construct($nid)
    {
      $this->nid = $nid;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'disponibilite_caution_form_'.$this->nid;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, ReservationRessourceNode $ressourceNode = null) {

        $form['caution_statut'] = array(
            '#type' => 'checkbox',
            '#default_value' => $ressourceNode->getCautionStatut(),
            '#ajax'         => [
                'callback'  => '::setCaution',
                'wrapper'   => 'mode',
                'event' => 'change',
            ],
        );

        $form['caution_montant'] = array(
            '#type' => 'number',
            '#default_value' => $ressourceNode->getCautionMontant(),
            '#ajax'         => [
                'callback'  => '::setMontant',
                'wrapper'   => 'mode',
                'event' => 'change',
            ],
        );

        return $form;
    }

    public function setCaution(array $form, FormStateInterface $form_state){

        $form_state->setRedirectUrl(Url::fromRoute(\Drupal::routeMatch()->getRouteName()));
        $caution_statut = $form_state->getValue('caution_statut');
        
        $ressourcenode = ReservationRessourceNode::load($this->nid);
        $ressourcenode->setCautionStatut($caution_statut);
        $ressourcenode->save();
        
        $response = new AjaxResponse();
        $response->addCommand(new CloseModalDialogCommand());
        return $response;
    }
    
    public function setMontant(array $form, FormStateInterface $form_state){

        $form_state->setRedirectUrl(Url::fromRoute(\Drupal::routeMatch()->getRouteName()));
        $caution_montant = $form_state->getValue('caution_montant');
        
        $ressourcenode = ReservationRessourceNode::load($this->nid);
        $ressourcenode->setCautionMontant($caution_montant);
        $ressourcenode->save();
        
        $response = new AjaxResponse();
        $response->addCommand(new CloseModalDialogCommand());
        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }

}
