<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\ParametreNotificationForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\reservation\Entity\ReservationNotification;
/**
 * Class StateForm.
 *
 * @ingroup bat
 */
class ParametreNotificationForm extends FormBase {
    
    protected $rnid;
        
    public function __construct($rnid)
    {
      $this->rnid = $rnid;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'reservation_notification_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, ReservationNotification $notification = null)
    {  
        $reservationRessourceNode = \Drupal::service('reservation.ressource.node');

        $form['email']['email_from'] = array(
            '#type' => 'textfield',
            '#title' => 'Email From :',
            '#default_value' => $notification->getEmailFrom()
        );
        
        $form['email']['email_cc'] = array(
            '#type' => 'textfield',
            '#title' => 'Email C ou CC :',
            '#default_value' => $notification->getEmailCc()
        );

        $form['email']['email_objet'] = array(
            '#type' => 'textfield',
            '#title' => 'Objet de l\'Email  :',
            '#default_value' => $notification->getEmailObjet()
        );

        $form['email']['email_corps'] = array(
            '#type' => 'textarea',
            '#title' => 'Corps de l\'Email :',
            '#rows' => 10,
            '#cols' => 60,
            '#default_value' => $notification->getEmailCorps()
        );

        $form['submit'] = [
          '#type' => 'submit',
          '#value' => 'Save',
        ];


        $header = [
              'title' => 'Nom',
              'token' => 'token',
        ];

        $form['token'] = [
            '#type' => 'table',
            '#header' => $header,
            '#rows' => $reservationRessourceNode->getTokenTable($notification->getReservationRessourceNode()->getNode()->id()),
            '#empty' => 'Aucuns token liés à cette recherche',
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {


    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {
        $notification = ReservationNotification::load($this->rnid);
        $notification->setEmailFrom($form_state->getValue('email_from'));
        $notification->setEmailCc($form_state->getValue('email_cc'));
        $notification->setEmailObjet($form_state->getValue('email_objet'));
        $notification->setEmailCorps($form_state->getValue('email_corps'));
        $notification->save();
    }  
}
