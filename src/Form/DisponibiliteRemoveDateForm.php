<?php

/**
 * @file
 * Contains \Drupal\reservation\Form\DisponibiliteRemoveDateForm.
 */
namespace Drupal\reservation\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Defines a confirmation form for deleting mymodule data.
 */
class DisponibiliteRemoveDateForm extends ConfirmFormBase {
    
    /**
     * The ID of the item to delete.
     *
     * @var string
     */
    protected $nid;

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
      return 'disponibilite_remove_date_form';
    }

    /**
    * {@inheritdoc}
    */
    public function getQuestion() {
       //the question to display to the user.
       return 'Voulez vous annuler les dispos de la ressource ' . $this->nid . ' ?' ;
    }

    /**
    * {@inheritdoc}
    */
    public function getCancelUrl() {
       //this needs to be a valid route otherwise the cancel link won't appear
       return new Url('reservation.disponibilite.index', ['choix' => 'user']);
    }

    /**
    * {@inheritdoc}
    */
    public function getDescription() {
       //a brief desccription
       return 'Êtes-vous sûr ?';
    }

    /**
    * {@inheritdoc}
    */
    public function getConfirmText() {
       return 'Suppression !';
    }


    /**
    * {@inheritdoc}
    */
    public function getCancelText() {
       return 'Retour au menu';
    }

    /**
    * {@inheritdoc}
    *
    * @param int $id
    *   (optional) The ID of the item to be deleted.
    */
    public function buildForm(array $form, FormStateInterface $form_state, $nid = NULL) {
       $this->nid = $nid;
       return parent::buildForm($form, $form_state);
    }

    /**
    * {@inheritdoc}
    */
    public function submitForm(array &$form, FormStateInterface $form_state) 
    {
        $reservationDate = \Drupal::service('reservation.date');
        $reservationDate->setCancelDate($this->nid);

        $form_state->setRedirectUrl(Url::fromRoute('reservation.disponibilite.index', ['choix' => 'user']));
    }
}