<?php

namespace Drupal\reservation\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\reservation\Event\EntityEvent;
use Drupal\reservation\Entity\ReservationRessourceNode;
use Drupal\reservation\Entity\ReservationNotification;

/**
 * Logs the creation of a new node.
 */
class ReservationNodeSaveSubscriber implements EventSubscriberInterface {

    /**
     * Permet la création des entité ReservationRessourceNode et ReservationRessourceNotification, lors de la création d'un node.
     *
     * @param \Drupal\reservation\Event\NodeInsertDemoEvent $event
     */
    public function onNodeInsert(EntityEvent $event) {
        
        $reservationRessource = \Drupal::service('reservation.ressource');

        $entity = $event->getEntity();
        $nid = $entity->id();
        $type = $entity->getType();
        
        $node_exist = $reservationRessource->getTypeExist($type, '1');
        if($node_exist)
        {            
            $this->createRessourceNode($nid, $type);
            $this->createRessourceNotification($nid);
        }
    }

    /**
     * Permet la création des entité RessourceNode et RessourceNotification, lors de la création d'une entité ReservationRessource.
     * 
     * @param EntityEvent $event
     */
    public function onReservationRessourceInsert(EntityEvent $event) {
        
        $entity = $event->getEntity();
        $type = $entity->getType();
        
        $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties(['type' => $type]);        
        foreach($nodes as $node)
        {
          $nid = $node->id();
          $this->createRessourceNode($nid, $type);
          $this->createRessourceNotification($nid);
        }
    }
    
    /**
     * Permet la création d'une entité RessourceNode .
     * 
     * @param EntityEvent $event
     */
    private static function createRessourceNode($nid, $type) {

        $reservationRessource = \Drupal::service('reservation.ressource');

        $ressourceNode = ReservationRessourceNode::load($nid); 
        if(!$ressourceNode)
        {
            $ressource = $reservationRessource->getRessourceByType($type);
            $entite = ReservationRessourceNode::create([
                'nid' => $nid,
                'rrid' => $ressource->id(),
                'automatique' => False,
                'statut' => True,
                'caution_statut' => $ressource->getCautionStatut(),
                'caution_montant' => $ressource->getCautionMontant(),
            ]);
            $entite->save();
        }
    }

    /**
     * Permet la création d'une entité RessourceNotification .
     * 
     * @param type $nid
     */
    private static function createRessourceNotification($nid) {

        $reservationNotification = \Drupal::service('reservation.notification');  
        $reservationSettings = \Drupal::config('reservation.settings');
        $rows = $reservationSettings->get('notification');    
        foreach($rows as $key => $row)
        {
            $exist_entity = $reservationNotification->getNotificationByNidType($nid, $key);
            if(!$exist_entity)
            {
                $entite = ReservationNotification::create([
                    'nid' => $nid,
                    'type_email' => $key,
                    'statut' => $row['field']['statut'],
                    'email_from' => $row['field']['from'],
                    'email_to' => $row['field']['to'],
                    'email_cc' => $row['field']['cc'],
                    'email_objet' => $row['field']['objet'],
                    'email_corps' => $row['field']['corps'],
                ]);
                $entite->save();
            }
        }
    }

  
    

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() {
      $events[EntityEvent::NODE_SAVE][] = ['onNodeInsert', 2];
      $events[EntityEvent::RESERVATION_RESSOURCE_SAVE][] = ['onReservationRessourceInsert', 2];
      return $events;
    }
}

