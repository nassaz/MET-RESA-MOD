<?php

namespace Drupal\reservation\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\reservation\Event\EntityEvent;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\reservation\Entity\ReservationDemande;
use Drupal\reservation\Entity\ReservationRessourceNode;

/**
 * Logs the creation of a new node.
 */
class ReservationWebformSubmissionSubscriber implements EventSubscriberInterface {

    /**
     * Log the creation of a new node.
     *
     * @param \Drupal\reservation\Event\NodeInsertDemoEvent $event
     */
    public function onWebformReservation(EntityEvent $event) {
        
        $webform_submission = $event->getEntity();
        
        if($webform_submission->getElementData('webform_reservation'))
        {
            //$this->createDemande($webform_submission);
        }
    }

    public function createDemande(WebformSubmission $webform_submission) {
        
        $reservationDemande = \Drupal::service('reservation.demande');
        $caldendarServices = \Drupal::service('reservation.calendar');
        $submission = $webform_submission->getData();
        
        $ressourcenode = ReservationRessourceNode::load($submission["webform_reservation"]);
        
        if($ressourcenode->getAutomatique())
        {
            $statut = 'confirme';
        }
        else
        {
            $statut = 'attente';            
        }
        
        $demandeur = $submission["nom"] . ' ' . $submission["prenom"];
        
        
        $rdid = $submission["webform_reservation_calendar"];
        $rhid = $submission["webform_reservation_horaire"]['reservation-horaire-select'] ? $submission["webform_reservation_horaire"]['reservation-horaire-select'] : '0';
        $verification = $caldendarServices->verificationDisponibilite($rdid, $rhid);
                
        if($verification)
        {
            $jauge = isset($submission["accompagnateur"]) ? $submission["accompagnateur"] + 1 : 1;
            $telephone = isset($submission["telephone"]) ? $submission["telephone"] : '';
            $reservationDemande->createDemande($rdid, $rhid, $webform_submission->Id(), 
                            $statut, $demandeur, $jauge, $submission["email"], $telephone);
        }
    }

    
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents() {
      $events[EntityEvent::WEBFORM_SUBMISSION][] = ['onWebformReservation', 2];
      return $events;
    }
}

