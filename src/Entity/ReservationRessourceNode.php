<?php

namespace Drupal\reservation\Entity;

use Drupal\reservation\ReservationRessourceNodeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_ressource_node",
 *   label = @Translation("ressource node entity"),
 *   base_table = "reservation_ressource_node",
 *   entity_keys = {
 *     "id" = "nid"
 *   }
 * )
 * 
 */
class ReservationRessourceNode extends ContentEntityBase implements ReservationRessourceNodeInterface {

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime() {
      return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function getChangedTime() {
      return $this->get('changed')->value;
    }

    public static function getNodeById($nid) {

      $node = ReservationRessourceNode::load($nid);

      return $node;    
    }
    
    public static function queryNodeByRrid($rrid)
    {
        $query = \Drupal::entityQuery('reservation_ressource_node'); 
        $query->condition('rrid', $rrid);   
         
        return $query->execute();   
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCautionStatut() {
      return ($this->get('caution_statut')->value) ? True : False;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCautionStatut($caution_statut) {
      $this->set('caution_statut', $caution_statut);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCautionMontant() {
      return $this->get('caution_montant')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCautionMontant($caution_montant) {
      $this->set('caution_montant', $caution_montant);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAutomatique() {
      return $this->get('automatique')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function setAutomatique($automatique) {
      $this->set('automatique', $automatique);
      return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmailTo() {
      return $this->get('email_to')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getNode() {
      return $this->get('nid')->entity;
    }
        
    /**
     * {@inheritdoc}
     */
    public function setEmailTo($email_to) {
      $this->set('email_to', $email_to);
      return $this;
    }
    
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

      $fields['nid'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Node ID'))
        ->setDescription(t('Node lié à la réservation.'))
        ->setSetting('target_type', 'node')
        ->setSetting('handler', 'default')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'title',
          'weight' => 2,
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
          'weight' => 2,
        ])
        ->setReadOnly(FALSE);

      $fields['rrid'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Ressource ID'))
        ->setDescription(t('Ressource liée au node.'))
        ->setSetting('target_type', 'reservation_ressource')
        ->setSetting('handler', 'default')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'title',
          'weight' => 2,
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
          'weight' => 2,
        ])
        ->setReadOnly(FALSE);

      $fields['automatique'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Automatique'))
        ->setDescription(t('Reservation Automatique'))
        ->setDefaultValue(False)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 5,
        ])
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'weight' => 5,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['statut'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('statut'))
        ->setDescription(t('Reservation Node statut'))
        ->setDefaultValue(True)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 5,
        ])
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'weight' => 5,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

        $fields['caution_statut'] = BaseFieldDefinition::create('boolean')
          ->setLabel(t('Statut de la Caution '))
          ->setDefaultValue(True)
          ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'string',
            'weight' => 5,
          ])
          ->setDisplayOptions('form', [
            'type' => 'boolean_checkbox',
            'weight' => 5,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

        $fields['caution_montant'] = BaseFieldDefinition::create('integer')
          ->setLabel(t('Montant de la Caution '))
          ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'string',
            'weight' => 5,
          ])
          ->setDisplayOptions('form', [
            'type' => 'boolean_checkbox',
            'weight' => 5,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      // Standard field, used as unique if primary index.
      $fields['email_to'] = BaseFieldDefinition::create('email')
        ->setLabel(t('To Email'))
        ->setRequired(TRUE)
        ->setDefaultValue('[webform_submission:values:email]')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 21,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 21,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
      
      $fields['created'] = BaseFieldDefinition::create('created')
        ->setLabel(t('Created'))
        ->setDescription(t('The time that the entity was created.'));

      $fields['changed'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Changed'))
        ->setDescription(t('The time that the entity was last edited.'));

      return $fields;
    }
}
