<?php

namespace Drupal\reservation\Entity;

use Drupal\reservation\ReservationNotificationInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_notification",
 *   label = @Translation("ressource notification entity"),
 *   base_table = "reservation_notification",
 *   entity_keys = {
 *     "id" = "rnid",
 *     "label" = "type_email",
 *   }
 * )
 *
 * 
 */
class ReservationNotification extends ContentEntityBase implements ReservationNotificationInterface {

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public function getCreatedTime() {
      return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function getChangedTime() {
      return $this->get('changed')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getStatut() {
      return $this->get('statut')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getReservationRessourceNode() {
      return $this->get('nid')->entity;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEmailFrom() {
      return $this->get('email_from')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEmailTo() {
      return $this->get('email_to')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEmailCc() {
      return $this->get('email_cc')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEmailObjet() {
      return $this->get('email_objet')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEmailCorps() {
      return $this->get('email_corps')->value;
    }    
    
    /**
     * {@inheritdoc}
     */
    public function setEmailFrom($email_from) {
      $this->set('email_from', $email_from);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setEmailTo($email_to) {
      $this->set('email_to', $email_to);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setEmailCc($email_cc) {
      $this->set('email_cc', $email_cc);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setEmailObjet($email_objet) {
      $this->set('email_objet', $email_objet);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setEmailCorps($email_corps) {
      $this->set('email_corps', $email_corps);
      return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setStatut($statut) {
      $this->set('statut', $statut);
      return $this;
    }
    
    public static function getNotificationById($nid) {

      $notification = ReservationNotification::load($nid);

      return $notification;    
    }

    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

      $fields['rnid'] = BaseFieldDefinition::create('integer')
          ->setLabel(t('ID'))
          ->setDescription(t('The ID of the reservation notification entity.'))
          ->setReadOnly(TRUE);

      $fields['nid'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Notification ID'))
        ->setDescription(t('Notification lié à la réservation.'))
        ->setSetting('target_type', 'reservation_ressource_node')
        ->setSetting('handler', 'default')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'title',
          'weight' => 2,
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
          'weight' => 2,
        ])
        ->setReadOnly(FALSE);

      // Standard field, used as unique if primary index.
      $fields['type_email'] = BaseFieldDefinition::create('string')
        ->setLabel(t('type du message'))
        ->setRequired(TRUE)
        ->setSetting('max_length', 32)
        ->setDefaultValue('demande')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 3,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 3,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['statut'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Statut'))
        ->setDescription(t('Statut Notification'))
        ->setDefaultValue(False)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 4,
        ])
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'weight' => 4,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      // Standard field, used as unique if primary index.
      $fields['email_from'] = BaseFieldDefinition::create('email')
        ->setLabel(t('Email From'))
        ->setRequired(TRUE)
        ->setDescription(t(''))
        ->setDefaultValue('')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 1,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 1,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);


      // Standard field, used as unique if primary index.
      $fields['email_from'] = BaseFieldDefinition::create('email')
        ->setLabel(t('From Email'))
        ->setRequired(TRUE)
        ->setDescription(t(''))
        ->setDefaultValue('')
        #->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 20,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 20,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      // Standard field, used as unique if primary index.
      $fields['email_cc'] = BaseFieldDefinition::create('email')
        ->setLabel(t('CC Email'))
        ->setRequired(TRUE)
        ->setDescription(t(''))
        ->setDefaultValue('')
        #->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 22,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 22,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      // Standard field, used as unique if primary index.
      $fields['email_objet'] = BaseFieldDefinition::create('string')
        ->setLabel(t('Objet Email'))
        ->setRequired(TRUE)
        ->setDescription(t(''))
        ->setDefaultValue('')
        #->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 23,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 23,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      // Standard field, used as unique if primary index.
      $fields['email_corps'] = BaseFieldDefinition::create('string_long')
        ->setLabel(t('Corps du message'))
        ->setRequired(TRUE)
        ->setDescription(t(''))
        ->setDefaultValue('')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 30,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 30,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['created'] = BaseFieldDefinition::create('created')
        ->setLabel(t('Created'))
        ->setDescription(t('The time that the entity was created.'));

      $fields['changed'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Changed'))
        ->setDescription(t('The time that the entity was last edited.'));

      return $fields;
    }
}
