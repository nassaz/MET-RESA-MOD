<?php

namespace Drupal\reservation\Entity;

use Drupal\reservation\ReservationDemandeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_demande_token",
 *   label = @Translation("ressource demande token entity"),
 *   base_table = "reservation_demande_token",
 *   entity_keys = {
 *     "id" = "token",
 *   }
 * )
 *
 * 
 */
class ReservationDemandeToken extends ContentEntityBase implements ReservationDemandeInterface {

    use EntityChangedTrait;

    /**
    * {@inheritdoc}
    */
    public function getCreatedTime() {
      return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedFormat() {
        $date = new \DateTime();
        $date->setTimestamp($this->getCreatedTime());
        return $date;      
    }
    
    /**
     * {@inheritdoc}
     */
    public function getChangedTime() {
      return $this->get('changed')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getDemande() {
      return $this->get('rdmid')->entity;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setDemande($rdmid) {
        $this->set('rdmid', $rdmid);
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCreatedTime($created) {
        $this->set('created', $created);
        return $this;
    }
    
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) 
    {
        $fields['token'] = BaseFieldDefinition::create('string')
          ->setLabel(t('Token'))
          ->setDescription(t('Token reservation.'))
          ->setSettings(array(
            'default_value' => '',
            'max_length' => 50,
            'text_processing' => 0,
          ));

        $fields['rdmid'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Réservation Demande ID'))
            ->setSetting('target_type', 'reservation_demande')
            ->setSetting('handler', 'default')
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'title',
              'weight' => -3,
            ])
            ->setDisplayOptions('form', [
              'type' => 'options_select',
              'weight' => -4,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'))
            ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel(t('Changed'))
            ->setDescription(t('The time that the entity was last edited.'));

        return $fields;
    }
}
