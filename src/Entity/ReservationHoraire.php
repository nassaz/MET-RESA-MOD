<?php

namespace Drupal\reservation\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\reservation\ReservationHoraireInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_horaire",
 *   label = @Translation("reservationhoraire entity"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "reservation_horaire",
 *   entity_keys = {
 *     "id" = "rhid"
 *   }
 * )
 *
 */
class ReservationHoraire extends ContentEntityBase implements ReservationHoraireInterface {

    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     *
     * When a new entity instance is added, set the user_id entity reference to
     * the current user as the creator of the instance.
     */
    public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
      parent::preCreate($storage_controller, $values);
      $values += [
        'user_id' => \Drupal::currentUser()->id(),
      ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function getRdid() {
      return $this->get('rdid')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCreatedTime() {
      return $this->get('created')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getChangedTime() {
      return $this->get('changed')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function getHeureDebut() {
      return $this->get('heure_debut')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getHeureFin() {
      return $this->get('heure_fin')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getJaugeStatut() {
      return $this->get('jauge_statut')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getJaugeNombre() {
      return $this->get('jauge_nombre')->value;
    }
        
    /**
     * {@inheritdoc}
     */
    public function getHeureDebutFormat($format) {
        $date = new \DateTime($this->getHeureDebut());
        return $date->format($format);      
    }
    
    /**
     * {@inheritdoc}
     */
    public function getHeureFinFormat($format) {
        $date = new \DateTime($this->getHeureFin());
        return $date->format($format);      
    }
        
    /**
     * {@inheritdoc}
     */
    public function setJaugeStatut($jauge_statut) {
      $this->set('jauge_statut', $jauge_statut ? '1' : '0');
      return $this;
    }
        
    /**
     * {@inheritdoc}
     */
    public function getStatut() {
      return $this->get('statut')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setStatut($statut) {
      $this->set('statut', $statut ? '1' : '0');
      return $this;
    }
    
    /**
     * {@inheritdoc}
     *
     * Define the field properties here.
     *
     * Field name, type and size determine the table structure.
     *
     * In addition, we can define how the field and its content can be manipulated
     * in the GUI. The behaviour of the widgets used can be determined here.
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

      $fields['rhid'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('ID'))
        ->setDescription(t('The ID of the reservationhoraire entity.'))
        ->setReadOnly(TRUE);

      $fields['rdid'] = BaseFieldDefinition::create('entity_reference')
        ->setLabel(t('Node ID'))
        ->setDescription(t('Node lié à la réservation.'))
        ->setSetting('target_type', 'reservation_date')
        ->setSetting('handler', 'default')
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'title',
          'weight' => -3,
        ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
          'weight' => -4,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['statut'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('statut'))
        ->setDescription(t('The statut of the reservationressource entity.'))
        ->setDefaultValue(True)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 5,
        ])
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'weight' => 5,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['heure_debut'] = BaseFieldDefinition::create('datetime')
          ->setLabel(t('Heure de début'))
          ->setDescription(t('Heure de début d\'une réservation'))
          ->setSettings([
            'max_length' => 2,
            'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'string',
            'weight' => 10,
          ])
          ->setDisplayOptions('form', [
            'type' => 'string_textfield',
            'weight' => 10,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['heure_fin'] = BaseFieldDefinition::create('datetime')
          ->setLabel(t('Heure de fin'))
          ->setDescription(t('Heure de fin d\'une réreservation'))
          ->setSettings([
            'max_length' => 2,
            'text_processing' => 0,
          ])
          ->setDefaultValue('')
          ->setDisplayOptions('view', [
            'label' => 'above',
            'type' => 'string',
            'weight' => 10,
          ])
          ->setDisplayOptions('form', [
            'type' => 'string_textfield',
            'weight' => 10,
          ])
          ->setDisplayConfigurable('form', TRUE)
          ->setDisplayConfigurable('view', TRUE);

      $fields['jauge_statut'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Statut Jauge'))
        ->setDefaultValue(1)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 60,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 60,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);
      
      $fields['jauge_nombre'] = BaseFieldDefinition::create('integer')
        ->setLabel(t('Jauge'))
        ->setDefaultValue(1)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 60,
        ])
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => 60,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayConfigurable('view', TRUE);

      $fields['created'] = BaseFieldDefinition::create('created')
        ->setLabel(t('Created'))
        ->setDescription(t('The time that the entity was created.'));

      $fields['changed'] = BaseFieldDefinition::create('changed')
        ->setLabel(t('Changed'))
        ->setDescription(t('The time that the entity was last edited.'));

      return $fields;
    }

}
