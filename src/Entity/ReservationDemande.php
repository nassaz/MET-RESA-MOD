<?php

namespace Drupal\reservation\Entity;

use Drupal\reservation\ReservationDemandeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup reservation
 *
 * @ContentEntityType(
 *   id = "reservation_demande",
 *   label = @Translation("ressource demande entity"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "reservation_demande",
 *   entity_keys = {
 *     "id" = "rdmid",
 *   }
 * )
 *
 * 
 */
class ReservationDemande extends ContentEntityBase implements ReservationDemandeInterface {
        
    use EntityChangedTrait;
     
    /**
     * 
     * @return type
     */
    public function getDateCreneau() 
    {            
        $ressourceDate = $this->rdid->entity;
        $ressourceHoraire = $this->rhid->entity;

        if($ressourceHoraire)
        {
            $date = new \DateTime($ressourceHoraire->getHeureDebut());
            $date_reservation = $date->format('d/m/Y H:i');
        }
        elseif($ressourceDate)
        {
            $date = new \DateTime($ressourceDate->getDate());
            $date_reservation = $date->format('d/m/Y');
        }
        
        return $date_reservation;
    }
        
    /**
    * {@inheritdoc}
    */
    public function getCreatedTime() {
      return $this->get('created')->value;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedFormat() {
        $date = new \DateTime();
        $date->setTimestamp($this->getCreatedTime());
        return $date->format('d/m/Y H:i');      
    }
    
    /**
     * {@inheritdoc}
     */
    public function getChangedTime() {
      return $this->get('changed')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getDemandeur() {
      return $this->get('demandeur')->value ? $this->get('demandeur')->value : '';
    }
        
    /**
     * {@inheritdoc}
     */
    public function getWebform() {      
      return $this->get('sid')->entity;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getDate() {      
      return $this->get('rdid')->entity;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getHoraire() {      
      return $this->get('rhid')->entity;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail() {
      return $this->get('email')->value;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTelephone() {
      return $this->get('telephone')->value ? $this->get('telephone')->value : '';
    }

    /**
     * 
     * @return type
     */
    public function getStatut() {
      return $this->get('statut')->value;
    }
    
    public function getStatutFormat() {

        $reservationFormDemande = \Drupal::service('reservation.demande.form');
        $tab = $reservationFormDemande->getStatutTab();
        
        return $tab[$this->getStatut()];
    }
    
    /**
     * 
     * @param type $sid
     * @return $this
     */
    public function setRdid($rdid) {
        $this->set('rdid', $rdid);
        return $this;
    }
    
    /**
     * 
     * @param type $sid
     * @return $this
     */
    public function setRhid($rhid) {
        $this->set('rhid', $rhid);
        return $this;
    }
    
    /**
     * 
     * @param type $sid
     * @return $this
     */
    public function setSid($sid) {
        $this->set('sid', $sid);
        return $this;
    }
    
    /**
     * 
     * @param type $statut
     * @return $this
     */
    public function setStatut($statut) {
        $this->set('statut', $statut);
        return $this;
    }
    
    /**
     * 
     * @return type
     */
    public function getJauge() {
        return $this->get('jauge')->value;
    }    
    
    /**
     * 
     * @param type $jauge
     * @return $this
     */
    public function setJauge($jauge) {
        $this->set('jauge', $jauge);
        return $this;
    }
    
    /**
     * 
     * @param type $email
     * @return $this
     */
    public function setEmail($email) {
        $this->set('email', $email);
        return $this;
    }
    
    /**
     * 
     * @param type $telephone
     * @return $this
     */
    public function setTelephone($telephone) {
        $this->set('telephone', $telephone);
        return $this;
    }
    
    /**
     * 
     * @param type $nom
     * @param type $prenom
     * @return $this
     */
    public function setDemandeur($nom = null, $prenom = null) {
        if($prenom)
        {
            $demandeur = $nom . ' ' . $prenom;
            $this->set('demandeur', $demandeur);            
        }
        else
        {
            $this->set('demandeur', $nom);
        }
        return $this;
    }
    
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type) 
    {

        $fields['rdmid'] = BaseFieldDefinition::create('integer')
            ->setLabel(t('ID'))
            ->setDescription(t('The ID of the reservationdemande entity.'))
            ->setReadOnly(TRUE);

        $fields['rdid'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Réservation Date ID'))
            ->setSetting('target_type', 'reservation_date')
            ->setSetting('handler', 'default')
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'title',
              'weight' => -3,
            ])
            ->setDisplayOptions('form', [
              'type' => 'options_select',
              'weight' => -4,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['rhid'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Réservation Horaire ID'))
            ->setSetting('target_type', 'reservation_horaire')
            ->setSetting('handler', 'default')
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'title',
              'weight' => -3,
            ])
            ->setDisplayOptions('form', [
              'type' => 'options_select',
              'weight' => -4,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['sid'] = BaseFieldDefinition::create('entity_reference')
            ->setLabel(t('Webform Submission ID'))
            ->setSetting('target_type', 'webform_submission')
            ->setSetting('handler', 'default')
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'title',
              'weight' => -3,
            ])
            ->setDisplayOptions('form', [
              'type' => 'options_select',
              'weight' => -4,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['statut'] = BaseFieldDefinition::create('list_string')
            ->setLabel(t('statut'))
            ->setSettings([
              'allowed_values' => [
                'confirme' => 'Confirmée',
                'attente' => 'En attente',
                'caution' => 'En attente Caution',
                'refuse' => 'Refusée',
                'archive' => 'Archivée',
              ],
            ])
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 20,
            ])
            ->setDisplayOptions('form', [
              'type' => 'options_select',
              'weight' => 20,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['jauge'] = BaseFieldDefinition::create('integer')
            ->setLabel(t('Jauge'))
            ->setDefaultValue(1)
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 60,
            ])
            ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 60,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['demandeur'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Demandeur'))
            ->setRequired(TRUE)

            ->setDefaultValue('')
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 1,
            ])
            ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 1,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);


        $fields['email'] = BaseFieldDefinition::create('email')
            ->setLabel(t('Email'))
            ->setRequired(TRUE)
            ->setDefaultValue('')
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 1,
            ])
            ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 1,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['telephone'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Téléphone'))
            ->setRequired(TRUE)
            ->setDefaultValue('')
            ->setDisplayOptions('view', [
              'label' => 'above',
              'type' => 'string',
              'weight' => 1,
            ])
            ->setDisplayOptions('form', [
              'type' => 'string_textfield',
              'weight' => 1,
            ])
            ->setDisplayConfigurable('form', TRUE)
            ->setDisplayConfigurable('view', TRUE);

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'))
            ->setDescription(t('The time that the entity was created.'));

        $fields['changed'] = BaseFieldDefinition::create('changed')
            ->setLabel(t('Changed'))
            ->setDescription(t('The time that the entity was last edited.'));

        return $fields;
    }
}
